<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        view()->composer(
            'composer.menu', 'App\Http\ViewComposers\MenuComposer'
        );
        view()->composer(
            'composer.navigation', 'App\Http\ViewComposers\NavigationComposer'
        );
        view()->composer(
            'composer.footer', 'App\Http\ViewComposers\FooterComposer'
        );
        view()->composer(
            'composer.trend', 'App\Http\ViewComposers\TrendComposer'
        );
        view()->composer(
            'composer.search', 'App\Http\ViewComposers\SearchComposer'
        );
        view()->composer(
            'composer.hits', 'App\Http\ViewComposers\HitsComposer'
        );
    }
}
