<?php

namespace App\Http\Controllers;
use App\Http\Helpers\Helpers;
use App\Http\Models\Main;
use App\Http\Models\Hits;
use App\Http\Models\Content;
use Session;
use ImageModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
class HomeController extends Controller
{
    public function index()
    {
        $device = Helpers::mobiledetect();
        $cache_id = $device.'_home';
        if (Cache::has($cache_id)){
            return Cache::get($cache_id);
        } else {
            return Cache::remember($cache_id, 0, function () {
                $main = Main::get();
                $home_1= array();
                $ids = array();
                foreach ($main as $key=>$record){
                    $record['data'] = Content::main($record['content_id']);
                    if($record['section'] == 'home' && $record['area'] == '1' )
                    {
                        array_push($home_1,$record);
                    }
                    array_push($ids,$record['content_id']);
                }
                $data['home_1']                 = $home_1;
                $data['arab']                   = Content::section($ids,'5ddfae7aa2432129cf397577',5);
                $data['turkey']                 = Content::section($ids,'5ddfae8ba243214673044db3',7);
                $data['bollywood']              = Content::section($ids,'5ddfaea0a243212e8b11bea6',7);
                $data['global']                 = Content::section($ids,'5ddfaf26a243212b8875c341',5);
                $data['nestology']              = Content::section($ids,'5ddfaf46a243212b8875c342',7);
                $data['mix']                    = Content::section($ids,'5ddfafb6a243212b8875c343',7);
                $data['sport']                  = Content::section($ids,'5ddfb77ba243212b8875c345',7);
                $data['last']                   = Content::Last($ids,11);
                $data['video']                  = Content::video(5);
//                $hits = Hits::Hits(5);

                return view('home.index',compact('data'))->render();
            });
        }
    }

    public function redirect($url){
        header("Location: ".$url, true, 301);
        exit();
    }
    public function images($photo,$id)
    {
        $dimension = array('source','large','150x150','450x250','450x450','750x450');
        $y = date("Y");
        $m = date("m");
        foreach ($dimension as $record){
            $dir = 'images/'.$record;
            if( is_dir($dir) === false ){mkdir($dir);}
            if( is_dir($dir.'/'.$y) === false ){mkdir($dir.'/'.$y);}
            if( is_dir($dir.'/'.$y.'/'.$m) === false ){mkdir($dir.'/'.$y.'/'.$m);}
        }
        $dir = $y.'/'.$m.'/';


        $file = $photo;
        $file_width = ImageModel::make($file)->width();
        $file_height = ImageModel::make($file)->height();
        $file_mime = ImageModel::make($file)->mime();
        if ($file_mime == 'image/jpeg'){
            $file_extension = '.jpg';
        }elseif($file_mime == 'image/jpg') {
            $file_extension = '.jpg';
        }elseif($file_mime == 'image/png') {
            $file_extension = '.png';
        }else{
            $file_extension = '';
        }

        $file_name =Helpers::createSlug($id).'-'.time().'-'.$file_extension;
        $img = ImageModel::make($file);
        $img->save('images/source/'.$dir.$file_name);
        $ratio = $file_width / $file_height;
        foreach($dimension as $key_=>$val){
            if($key_ < 2){
                continue;
            }

            $di = explode("x", $val);
            if ($ratio < 1) {
                $targetWidth = $di['1'] * $ratio;
                if ($targetWidth < $di['0']){
                    $targetWidth = $di['0'];
                }
                ImageModel::make('images/source/'.$dir.$file_name)->resize(
                    $targetWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                }
                )->crop($di['0'], $di['1'], 0, 0)->save(
                    'images/' . $val . '/' . $file_name, 60
                );
            }else{
                $targetHeight = $di['0'] / $ratio;
                if ($targetHeight < $di['1']){
                    $targetHeight = $di['1'];
                }
                ImageModel::make('images/source/'.$dir.$file_name)->resize(null,$targetHeight, function ($constraint) {$constraint->aspectRatio();})->crop($di['0'], $di['1'], 0, 0)->save('images/'.$val.'/' .$dir.$file_name, 60);
            }
            if ($file_width >= 1000){
                $targetWidth = '1000';
            }else{
                $targetWidth = $file_width;
            }
            ImageModel::make('images/source/' . $dir.$file_name)->resize(
                $targetWidth, null, function ($constraint) {
                $constraint->aspectRatio();
            }
            )->save('images/large/'.$dir.$file_name, 60);
        }
        return $dir.$file_name;

    }
    public function cache($id){
        if($id != 'website'){
            if(Session::get('backend') === 'on'){
                echo '<div id="view" style="position: fixed;width: 100%;z-index: 9999;bottom: 0px;float: right;direction: rtl;height: 60px;border:none;"><iframe src="https://admin.mydasht.com/out/'.$id.'" ></iframe></div>';
            }
        }

    }
    public function out(){
        Session::set('backend', 'on');
    }

    public function backend_out(){
        Session::put('backend', 'on');
//        \Session\Store::set('u2f.registerData', $req);
    }


    public function hits(){
        $data['hits'] = Hits::hits(7);
        foreach($data['hits'] as $key=>$record){
            return $record['contentData'];
        }

    }
}
