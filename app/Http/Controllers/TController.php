<?php

namespace App\Http\Controllers;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use App\Http\Models\Main;
use App\Http\Models\Content;
use App\Http\Models\Section;
use url;

class TController extends Controller
{
    public function index($channel,$id,$section,$title)
    {
        $content = Content::where('_id',$id)->where('case','published')->first();
        $content->hits = $content->hits+1;
        $content->save();
        $content_array = $content->content;
        foreach ($content_array as $key=>$record){
            if(is_array($record))
            {
                if($record['type'] == 'album'){
                    $content_array[$key]['album'] = Content::where('mydasht_id',$record['content'])->get();
                }elseif($record['type'] == 'video'){
                    $content_array[$key]['video'] = Content::where('mydasht_id',$record['content'])->first();
                }
            }
        }


        ////////// hits /////////
        $hits =  Content::
        where('case','published')
//            ->whereDate('created_at', Carbon::today())
            ->orderBy('hits','DESC')
            ->limit(4)
            ->get();
        foreach ($hits as $key=>$record){
            $record['section_name'] = Section::whereIn('mydasht_id',$record->section)->first();
        }
        ////////// hits /////////






        return view('t.index',compact('content','content_array','hits'));
    }


}
