<?php

namespace App\Http\Controllers;
use App\Http\Helpers\Helpers;
use Illuminate\Http\Request;
use App\Http\Models\Content;
use App\Http\Models\Section;
use App\Http\Models\Hits;
use App\Http\Models\Keyword;
use App\Http\Models\Images;
use App\Http\Models\Cv;

use Cache;
use url;
use Session;
use Carbon\Carbon;


class Single_newsController extends Controller
{
    public function index($y,$m,$d,$id,$title)
    {
        $device = Helpers::mobiledetect();
        $cache_id = $device.$id;
        $this->hits($id,$id);
        if (Cache::has($cache_id)){
            return Cache::get($cache_id);
        } else {
            return Cache::remember($cache_id, 2, function () use($y,$m,$d,$id,$title) {
                $content = Content::where('news_id',(int)$id)->where('case','published')->first();

                if(empty($content)){
                    return $this->redirect('/page/error/404');
                };


                $ids = array();
                array_push($ids,$content['_id']);


                $next = array();


                if(isset($content['related'])){
                    $content['related_data'] = Content::whereIn('_id', $content['related'])->take(3)->get();
                }
                if(!empty($content['album'])){
                    $content['album_data'] = Images::where('album', $content['album'])->get();
                }
                if(isset($content['tags'])){
                    $content['tags'] = array(implode(',',$content['tags']));
//                    $content['tags'] = explode('"',$content['tags']);
//                    return $content['tags'];
                    $content['tags_data'] = Keyword::whereIn('keyword_id', $content['tags'])->get();
//                    return $content['tags_data'];
                }



                $section = Section::find($content['section']);
                $ids = array();
//                $hits = Content::Last($ids,8);
                $latestarticles = Content::Section($ids,$content['section'],5);
//                ;
                // get previous user id
                $previousID = Content::where('case', 'published')->where('news_id', '<', $content->news_id)->max('news_id');
                $prearticle = Content::select('title','url')->where('news_id', $previousID)->first();

                // get next user id
                $nextID = Content::where('case', 'published')->where('news_id', '>', $content->news_id)->where('case', 'published')->min('news_id');
                $nextarticle = Content::select('title','url')->where('news_id', $nextID)->first();
                $hits = Hits::Hits(6);
                $views = Hits::where('news_id', $content->news_id)->first();
                return view('single.index',compact('content','section','hits','views','latestarticles','next','prearticle','nextarticle'))->render();
            });
        }
    }
    public function album($_id,$images_id)
    {
        return 'loading';
        $content = Content::find($_id);
        $images = Images::where('album',$content->_id)->orderBy('sort', 'desc')->get();
        return view('single.album',compact('content','images','images_id'));

    }
    public function redirect($url){
        header("Location: ".$url, true, 301);
        exit();
    }


    public function hits($id){
        $hits = Hits::where('news_id',(int)$id)->first();
        if(!empty($hits)){
            if($hits->today != Carbon::today()->toDateTimeString()){
                $today = Carbon::today()->toDateTimeString();
                $hits_today = 1;
            }else{
                $today = $hits->today;
                $hits_today = $hits->hits_today+1;
            }
            $hits->hits = $hits->hits+1;
            $hits->today = $today;
            $hits->hits_today = $hits_today;
            $hits->save();
        }else{
            $hits = new Hits();
            $hits->news_id = (int)$id;
            $hits->hits = $hits->hits+1;
            $hits->hits_today = $hits->hits_today+1;
            $hits->today = Carbon::today()->toDateTimeString();
            $hits->save();
        }
    }


//    public function hits($id,$path){
//        $hits = Hits::where('news_id',$id)->first();
//        if(!empty($hits)){
//            if($hits->today != Carbon::today()->toDateTimeString()){
//                $today = Carbon::today()->toDateTimeString();
//                $hits_today = 1;
//            }else{
//                $today = $hits->today;
//                $hits_today = $hits->hits_today+1;
//            }
//            $hits->hits = $hits->hits+1;
//            $hits->today = $today;
//            $hits->path = $path;
//            $hits->hits_today = $hits_today;
//            $hits->save();
//        }else{
//            $hits = new Hits();
//            $hits->news_id = (int)$id;
//            $hits->hits = $hits->hits+1;
//            $hits->path = $path;
//            $hits->hits_today = $hits->hits_today+1;
//            $hits->today = Carbon::today()->toDateTimeString();
//            $hits->save();
//        }
//    }

    public function scrollarticles($id){
        $content = Content::select('_id', 'news_id', 'title', 'section', 'url', 'content', 'written', 'main_img', 'img_description', 'publication_date')->with('sectionData:_id,name,url','hitsData:_id,news_id,hits')->where('news_id', '!=', $id)->orderBy('publication_date', 'desc')->take(5)->get();
        return $content;
    }


    public function cv($id,$title)
    {
        $device = Helpers::mobiledetect();
        $cache_id = $device.$id;
//        return $cache_id;
//        $this->hits($id,$id);
        if (Cache::has($cache_id)){
            return Cache::get($cache_id);
        } else {
            return Cache::remember($cache_id, 2, function () use($id,$title) {
                $content = Cv::where('ID', $id)->first();
//                return $cv;
                if(empty($content)){
                    return $this->redirect('/page/error/404');
                };


//                $ids = array();
//                array_push($ids,$content['_id']);
//
//
//                $next = array();


//                if(isset($content['related'])){
//                    $content['related_data'] = Content::whereIn('_id', $content['related'])->take(3)->get();
//                }
//                if(!empty($content['album'])){
//                    $content['album_data'] = Images::where('album', $content['album'])->get();
//                }
//                if(isset($content['tags'])){
//                    $content['tags'] = array(implode(',',$content['tags']));
//                    $content['tags_data'] = Keyword::whereIn('keyword_id', $content['tags'])->get();
//                }



//                $section = Section::find($content['section']);
//                $ids = array();
//                $hits = Content::Last($ids,8);
//                $latestarticles = Content::Section($ids,$content['section'],5);
//                ;
                // get previous user id
//                $previousID = Content::where('case', 'published')->where('news_id', '<', $content->news_id)->max('news_id');
//                $prearticle = Content::select('title','url')->where('news_id', $previousID)->first();

                // get next user id
//                $nextID = Content::where('case', 'published')->where('news_id', '>', $content->news_id)->where('case', 'published')->min('news_id');
//                $nextarticle = Content::select('title','url')->where('news_id', $nextID)->first();
//                $hits = Hits::Hits(6);
//                $views = Hits::where('news_id', $content->news_id)->first();
                return view('single.cv',compact('content', 'content'))->render();
            });
        }
    }



    public function amp($y,$m,$d,$id,$title)
    {
        $device = Helpers::mobiledetect();
        $cache_id = $device.$id;
        $this->hits($id,$id);
        if (Cache::has($cache_id)){
            return Cache::get($cache_id);
        } else {
            return Cache::remember($cache_id, 2, function () use($y,$m,$d,$id,$title) {
                $content = Content::where('news_id',(int)$id)->where('case','published')->first();

                if(empty($content)){
                    return $this->redirect('/page/error/404');
                };


                $ids = array();
                array_push($ids,$content['_id']);


                $next = array();


                if(isset($content['related'])){
                    $content['related_data'] = Content::whereIn('_id', $content['related'])->take(3)->get();
                }
                if(!empty($content['album'])){
                    $content['album_data'] = Images::where('album', $content['album'])->get();
                }
                if(isset($content['tags'])){
                    $content['tags'] = array(implode(',',$content['tags']));
//                    $content['tags'] = explode('"',$content['tags']);
//                    return $content['tags'];
                    $content['tags_data'] = Keyword::whereIn('keyword_id', $content['tags'])->get();
//                    return $content['tags_data'];
                }



                $section = Section::find($content['section']);
                $ids = array();
//                $hits = Content::Last($ids,8);
                $latestarticles = Content::Section($ids,$content['section'],5);
//                ;
                // get previous user id
                $previousID = Content::where('case', 'published')->where('news_id', '<', $content->news_id)->max('news_id');
                $prearticle = Content::select('title','url')->where('news_id', $previousID)->first();

                // get next user id
                $nextID = Content::where('case', 'published')->where('news_id', '>', $content->news_id)->where('case', 'published')->min('news_id');
                $nextarticle = Content::select('title','url')->where('news_id', $nextID)->first();
                $hits = Hits::Hits(6);
                $views = Hits::where('news_id', $content->news_id)->first();
                return view('single.amp',compact('content','section','hits','views','latestarticles','next','prearticle','nextarticle'))->render();
            });
        }
    }



}
