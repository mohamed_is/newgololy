<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Image;


class ImagesController extends Controller
{

    public function index($filename,$dimension)
    {
        $img = Image::make('images/source/'.$filename);
        $width = $img->width();
        $height = $img->height();
        $ratio = $width / $height;

        foreach($dimension as $val){
            $di = explode("x", $val);

            if ($ratio < 1) {
                $targetWidth = $di['1'] * $ratio;
                if ($targetWidth < $di['0']){
                    $targetWidth = $di['0'];
                }
                Image::make('images/source/' . $filename)->resize(
                    $targetWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                }
                )->crop($di['0'], $di['1'], 0, 0)->save(
                    'images/' . $val . '/' . $filename, 60
                );
            }else{
                $targetHeight = $di['0'] / $ratio;
                if ($targetHeight < $di['1']){
                    $targetHeight = $di['1'];
                }
                Image::make('images/source/' . $filename)->resize(
                     null,$targetHeight, function ($constraint) {
                    $constraint->aspectRatio();
                }
                )->crop($di['0'], $di['1'], 0, 0)->save(
                    'images/' . $val . '/' . $filename, 60
                );
            }
            if ($width >= 1000){
                $targetWidth = '1000';
            }else{
                $targetWidth = $width;
            }
            Image::make('images/source/' . $filename)->resize(
                $targetWidth, null, function ($constraint) {
                $constraint->aspectRatio();
            }
            )->save('images/large/'.$filename, 60);
        }
    }











    public function insert(Request $request){
    //   if(!empty(Helpers::check_permission('section-create','0'))) { return view('errors/permissions');}
        $publisher = Publisher::find(Session::get('Publisher'));
        if($request->input('images_type') == 'album'){
        $content = new Content();
        $content->publisher_id = Session::get('Publisher');
        $content->title = htmlentities($request->input('title'));
        $content->case = '';
        $content->search = Helpers::search(htmlentities($request->input('title')));
        $content->user_id = auth::user()->_id;
        $content->type = 'album';
        $content->save();
        app('App\Http\Controllers\TrackingController')->create($content);
    }

        $files_path = $publisher->images_url;
        $files_source = $files_path.'source/';
        $publisher_url = $publisher->url;
        $publisher_images = Publisher_images::where('publisher_id',Session::get('Publisher'))->get();
        foreach($publisher_images as $val){
            $dimension[] = $val->width.'x'.$val->width;
        }

    $output = [];
    foreach($request->file('file') as $file) {
        $file_size = $file->getClientSize();
        list($width, $height) = getimagesize($file);
        $file_extension = $file->getClientOriginalExtension();

        $add = new Images;
        $add->user_id = auth::user()->_id;
        $add->publisher_id = Session::get('Publisher');
        $add->name = $request->input('title');
        $add->master = $request->input('master');
        $add->images_type = $request->input('images_type');
        if($request->input('images_type') == 'album'){
            $add->album_id = $content->_id;
        };
        $add->width = $width;
        $add->height = $height;
        $add->file_size = $file_size;
        $add->file_extension = $file_extension;
        $add->keyword = Helpers::search($request->input('name'));
        $add->save();
        $file_name =$add->_id.'.'.$file_extension;



        if($width > $height){
            ImageModel::make($file)->resize(null, 300,function ($constraint) {$constraint->aspectRatio();})->crop(300, 300, 0, 0)->save('files/thumbnail/'.$file_name);
        }else{
            ImageModel::make($file)->resize(300, null,function ($constraint) {$constraint->aspectRatio();})->crop(300, 300, 0, 0)->save('files/thumbnail/'.$file_name);
        }
        $files[] = array(
            'url' => $files_source.$file_name,
            'thumbnailUrl' => '/files/thumbnail/'.$file_name,
            'name' => $file_name,
            'type' => 'image/png',
            'size' => $add->file_size,
            'deleteUrl' => $files_path.$file_name,
            'deleteType' => 'DELETE'
        );


        if ( ! is_array( $file ) ) {
            $output[] = [
                'contents' => base64_encode(file_get_contents($file)),
                'filename' => $file_name,
                'dimension' => $dimension
            ];
            continue;
        }
    }
        $client = new Client();
        $response = $client->request('POST', $publisher_url.'api/content/photo/add', [
            'headers' => [
                'authorization' => 'Client-ID ' . 'app_id',
                'content-type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => [
                'multipart' => $output
            ]
        ]);
        if($response->getStatusCode() == "200"){


            return response()->json(array('files' => $files), 200);
        }else{
            return 'error';
        }


//        if($width > $height){
//            ImageModel::make($files_source.'/'.$file_name)->resize(null, 300,function ($constraint) {$constraint->aspectRatio();})->crop(300, 300, 0, 0)->save($files_path.'/thumbnail/'.$file_name);
//        }else{
//            ImageModel::make($files_source.'/'.$file_name)->resize(300, null,function ($constraint) {$constraint->aspectRatio();})->crop(300, 300, 0, 0)->save($files_path.'/thumbnail/'.$file_name);
//        }
//        foreach($publisher_images as $val){
//            if($width > $height){
//                ImageModel::make($files_source.'/'.$file_name)->resize(null, $val->height,function ($constraint) {$constraint->aspectRatio();})->crop($val->width, $val->height, 0, 0)->save($files_path.'/'.$val->width.'x'.$val->height.'/'.$file_name);
//            }else{
//                ImageModel::make($files_source.'/'.$file_name)->resize($val->width, null,function ($constraint) {$constraint->aspectRatio();})->crop($val->width, $val->height, 0, 0)->save($files_path.'/'.$val->width.'x'.$val->height.'/'.$file_name);
//            }
//        }
//
//
//

}

}
