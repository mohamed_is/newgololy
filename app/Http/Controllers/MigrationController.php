<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helpers;
use App\Http\Models\Content;
use App\Http\Models\Tags;
use App\Http\Models\Images;
use App\Http\Models\Keyword_link;
use App\Http\Models\Keyword;
use App\Http\Models\Cv;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Image;
use Carbon\Carbon;
use Exception;

class MigrationController extends Controller
{

    public function __construct()
    {
    }


    public function index()
    {
        $api_url = Content::where('news_id', 'exists', true)->orderBy('news_id', 'desc')->first();
        if(empty($api_url)){
            $news_id_api = 0;
        }else{
            $news_id_api = $api_url['news_id'];
        }
        $url = 'https://www.gololy.com/migration/'.$news_id_api;
//        $url = 'https://www.gololy.com/migration/243201';
        $client = new Client();
        try {
            $response = $client->request('GET', $url);
        } catch (GuzzleException $exception) {
            $responseBody = $exception->getResponse()->getBody();
            return $responseBody;
        }
        $back = json_decode($response->getBody(), true);


        $rows = $back['rows'];

//        foreach ($rows as $key=>$re){
//
//            try {
//                $new_url = $this->test_url($re['NewsURL']);
//                $rows[$key]['NewsURL'] = $new_url;
//                echo $rows[$key]['NewsURL'] ;
//echo '<BR>';
//            } catch (Exception $e) {
//               // echo 'Caught exception: ',  $e->getMessage(), "<BR>";
//                continue;
//            }
//        }
//return $rows;

        foreach ($rows as $key=>$records) {
            $duple = Content::where('news_id',$records['ID'])->first();
            if(!empty($duple)){
                continue;
            }

            ///start keyword
             $tags = explode(",",$records['tagID']);

            /// start section
            $sectionDB = DB::table('section')->get();
            $secID = explode(",",$records['secID']);
            foreach ($sectionDB as $rec){
                if (in_array($rec['old_id'], $secID))
                {
                    $section = (string)$rec['_id'];
                }
            }
            if(!isset($section)){
                $section = '5ddfafb6a243212b8875c343';
            }
            /// end section
            ///start related
            $related = array();
            ///end related

            ///start Content

            $content = $this->content_array($records['post_content']);
            foreach ($content as $key => $record) {
                $content[$key] = htmlentities($record);
            }
            if(!isset($content)){
                $content = array();
            }
            ///end Content
            $type = 'article';
            $case = 'published';

            ///start video
            if(!empty($records['post_video'])){
                $video = $records['post_video'];
                $type = 'video';
            }else{
                $video = null;
            }
            ///end video


            try {
                $url = $this->test_url($records['NewsURL']);


            /// start album
            if(!empty($records['post_images'])){
                if(count($records['post_images']) > 1){
                    $album = new Content();
//                    $album['news_id'] = (int)$records['ID'];
                    $album['user_id'] = '5b4243ae0ec4ef4844005f82';
                    $album['title'] = htmlentities($records['Title']);
                    $album['description'] = '';
                    $album['section'] = $section;
                    $album['content'] = $related;
                    $album['written'] = htmlentities($records['writer']);
                    $album['main_img'] = htmlentities($records['Image']);
                    $album['img_description'] = htmlentities($records['Title']);
                    $album['album'] = null;
                    $album['video'] = null;
                    $album['search'] = Helpers::search(htmlentities($records['Title']));
                    $album['tags'] = $tags;
                    $album['related'] = $related;
                    $album['type'] = "album";
                    $album['case'] = "unpublished";
                    $album->save();
                    foreach ($records['post_images'] as $key=>$imgs){
                        $img = new Images();
                        $img['user_id'] = '5b4243ae0ec4ef4844005f82';
                        $img['album'] = (string)$album['_id'];
                        $img['name'] = htmlentities($imgs['alttext']);
                        $img['sort'] = (int)$key;
                        $img['images_url'] = str_replace('../','',$imgs['path']).'/'.$imgs['filename'];
                        $img['keyword'] = Helpers::search(htmlentities($imgs['alttext']));
                        $img->save();
                    }

                    $album = $album['_id'];
                }else{
                    $album = null;
                }
            }else{
                $album = null;
            }

            /// end album

            $data = new Content();
            $data['news_id'] = (int)$records['ID'];
            $data['user_id'] = '5b4243ae0ec4ef4844005f82';
            $data['title'] = htmlentities($records['Title']);
            $data['url_text'] = htmlentities($records['Title']);
            $data['url'] = $url;
            $data['description'] = htmlentities($records['Abstract']);
            $data['section'] = $section;
            $data['content'] = $content;
            $data['written'] = htmlentities($records['writer']);
            $data['main_img'] = htmlentities($records['Image']);
            $data['img_description'] = htmlentities($records['Title']);
            $data['album'] = null;
            $data['video'] = $video;
            $data['search'] = Helpers::search(htmlentities($records['Title']));
            $data['tags'] = $tags;
            $data['related'] = $related;
            $data['type'] = $type;
            $data['case'] = $case;
            $data['options'] = array('inside_img');
            $data['seo'] = array('ads');
            $data['publication_date'] = Carbon::parse($records['publishTime'])->toDateTimeString();
            $data['updated_at'] = Carbon::parse($records['publishTime'])->toDateTimeString();
            $data['created_at'] = Carbon::parse($records['publishTime'])->toDateTimeString();
            $data->save();

            } catch (Exception $e) {
                continue;
            }
//
        }
        echo 'done';
        echo '<meta http-equiv="refresh" content="3">';

    }


    public function cv()
    {
        $api_url = Cv::where('cv_id', 'exists', true)->orderBy('cv_id', 'desc')->first();
        if(empty($api_url)){
            $cv_id_api = 0;
        }else{
            $cv_id_api = $api_url['cv_id'];
        }
        $url = 'https://www.gololy.com/migration/cv/'.$cv_id_api;
//        $url = 'https://www.gololy.com/migration/243201';
        $client = new Client();
        try {
            $response = $client->request('GET', $url);
        } catch (GuzzleException $exception) {
            $responseBody = $exception->getResponse()->getBody();
            return $responseBody;
        }
        $back = json_decode($response->getBody(), true);


        $rows = $back['rows'];
//        return $rows;
//        exit;

        foreach ($rows as $key=>$records) {
            $duple = Cv::where('cv_id',$records['ID'])->first();
            if(!empty($duple)){
                continue;
            }

            ///start Content

            $content = $this->content_array($records['post_content']);
            foreach ($content as $key => $record) {
                $content[$key] = htmlentities($record);
            }
            if(!isset($content)){
                $content = array();
            }
            ///end Content
            $type = 'cv';
            $case = 'published';



            try {
//                $url = $this->test_url($records['NewsURL']);
//
//
//
//                $data = new Content();
//                $data['news_id'] = (int)$records['ID'];
//                $data['user_id'] = '5b4243ae0ec4ef4844005f82';
//                $data['title'] = htmlentities($records['Title']);
//                $data['url_text'] = htmlentities($records['Title']);
//                $data['url'] = $url;
//                $data['description'] = htmlentities($records['Abstract']);
//                $data['section'] = $section;
//                $data['content'] = $content;
//                $data['written'] = htmlentities($records['writer']);
//                $data['main_img'] = htmlentities($records['Image']);
//                $data['img_description'] = htmlentities($records['Title']);
//                $data['album'] = null;
//                $data['video'] = $video;
//                $data['search'] = Helpers::search(htmlentities($records['Title']));
//                $data['tags'] = $tags;
//                $data['related'] = $related;
//                $data['type'] = $type;
//                $data['case'] = $case;
//                $data['options'] = array('inside_img');
//                $data['seo'] = array('ads');
//                $data['publication_date'] = Carbon::parse($records['publishTime'])->toDateTimeString();
//                $data['updated_at'] = Carbon::parse($records['publishTime'])->toDateTimeString();
//                $data['created_at'] = Carbon::parse($records['publishTime'])->toDateTimeString();
//                $data->save();

            } catch (Exception $e) {
                continue;
            }
//
        }
        echo 'done';
//        echo '<meta http-equiv="refresh" content="3">';

    }




    public function content_array($content)
    {
        $content = str_replace('<div>', "\n", $content);
        $content = str_replace('<br>', "\n", $content);
        $content = str_replace('<br>', "\n", $content);
        $content = str_replace('<br />', "\n", $content);
        $content = str_replace('<br/>', "\n", $content);
        $content = str_replace('show.aspx?id=', "", $content);
        $content = str_replace('ca-pub-9259888425604903', "", $content);
        $content = str_replace('(adsbygoogle = window.adsbygoogle || []).push({});', "", $content);
        $content = str_replace('</div>', "", $content);
        $content = str_replace('\r', "", $content);
        $content = strip_tags($content, '<img><a><b><i><blockquote><u><script><iframe><h2><h3><h4><h5><album><font>');
        $content = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $content);
        $content = explode("\n", $content);
        foreach($content as $key => $value)
        {
            if (strpos($value, '<album') !== false) {
                $array = str_replace('<album id="', '', $value);
                $new_key = substr($array, 0, strpos($array, '">'));
                $new_array = array('type'=>'album','content'=>$new_key);
                //  array_push($content[$key], $new_array);
                $element[] = $new_array;
            }else{
                $element[] = $value;

            }
        }
        $content_end =  array_filter($element);
        return $content_end;}
    public function album()
    {
        $content = DB::table('content')->where('migration_album','<>',1)->take(10000)->orderBy('_id', 'ASC')->get();

        foreach ($content as $key=>$record){

            $NewsGallery = DB::table('NewsGallery')->where('NewsID',(string)$record['news_id'])->get();
//            if(count($NewsGallery)> 0){
//                return $NewsGallery;
//            }
            foreach ($NewsGallery as $k=>$rec){
                if($k === 0){
                    $this->images_id($record['_id']);
                }
                $img_id = str_split($rec['ID']);
                if(strlen($rec['ID']) == '3'){
                    $main_img = '/upload/photo/Gallery/0/0/'.$img_id['0'].$img_id['1'].$img_id['2'].'.jpg';
                }elseif(strlen($rec['ID']) == '4'){
                    if($img_id['1'] === '0'){
                        $img_id['1'] ='';
                    }
                    if($img_id['2'] === '0'){
                        $img_id['2'] ='';
                    }
                    $main_img = '/upload/photo/Gallery/0/'.$img_id['0'].'/'.$img_id['1'].$img_id['2'].$img_id['3'].'.jpg';
                }elseif(strlen($rec['ID']) == '5'){
                    if($img_id['3'] === '0'){
                        $img_id['3'] ='';
                    }
                    if($img_id['2'] === '0'){
                        $img_id['2'] ='';
                    }
                    $main_img = '/upload/photo/Gallery/'.$img_id['0'].'/'.$img_id['1'].'/'.$img_id['2'].$img_id['3'].$img_id['4'].'.jpg';
                }elseif(strlen($rec['ID']) == '6'){
                    if($img_id['3'] === '0'){
                        $img_id['3'] ='';
                    }
                    if($img_id['4'] === '0'){
                        $img_id['4'] ='';
                    }
                    $main_img = '/upload/photo/Gallery/'.$img_id['0'].$img_id['1'].'/'.$img_id['2'].'/'.$img_id['3'].$img_id['4'].$img_id['5'].'.jpg';
                }else{
                    $main_img = '';
                }




                $data['user_id'] = '5b4243ae0ec4ef4844005f82';
                $data['album'] = (string)$record['_id'];
                $data['name'] = $record['title'];
                $data['sort'] = (int)$rec['OrderIndex'];
                $data['images_url'] = $main_img;
                $data['keyword'] = $record['search'];
                DB::table('images')->insert($data);

            }
            if(count($NewsGallery) > 0){
                DB::table('content')->where('_id', $record['_id'])->update(['migration_album' => 1,'type' => 'album']);
            }else{
                DB::table('content')->where('_id', $record['_id'])->update(['migration_album' => 1]);
            }
        }
        return 'done';
    }
    public function images(){
        $content = Content::where('case','published')->where('img_new','<>',1)->take(10)->orderBy('news_id','desc')->get();
        foreach ($content as $record){
            if(isset($record->img_new)){
                return '';
            }else{
                $this->images_id($record['_id']);
            }
        }
        echo 'done';
        echo '<meta http-equiv="refresh" content="3">';
    }
    public function keyword()
    {
        $content = DB::table('wp_terms')
            ->where('migration','<>',1)
            ->take(5000)->orderBy('ID', 'asc')->get();
        foreach ($content as $record){
            $keyword = new Keyword();
            $keyword['keyword_id'] = (int)$record['ID'];
            $keyword['keyword'] = $record['keyword'];
            $keyword->save();
            DB::table('wp_terms')->where('_id', $record['_id'])->update(['migration' => 1]);
        }
        echo 'done';
        echo '<meta http-equiv="refresh" content="3">';    }
    public function copy_title()
    {
        $title = DB::table('title0')->get();
        foreach ($title as $key=>$record){
            DB::table('title')->insert($record);;
        }
        return '0';
    }
    public function copy_body()
    {
        $title = DB::table('900000')->get();
        foreach ($title as $key=>$record){
            $new = DB::table('body')->insert($record);;
        }
        return '900000';
    }
    public function images_id($aid){

        $record = Content::find($aid);
        if(isset($content->img_new)){
            return '';
        }
        $main_imgggg = $record['img_description'];

        $id = $record['news_id'];
        $_id = $record['_id'];

        $file = 'https://www.gololy.com'.$record['main_img'];



        $file_headers = @get_headers($file);
            if($file_headers[0] == 'HTTP/1.1 404 Not Found') {


            $content = Content::find($_id);
            $content->img_new = 1;
            $content->main_img = 'error';
            $content->save();
            return '';
        }

        $dimension = array('source','large','150x150','450x250','320x145','800x500','520x600');
        $y = date("Y");
        $m = date("m");
        foreach ($dimension as $record){
            $dir = 'images/'.$record;
            if( is_dir($dir) === false ){mkdir($dir);}
            if( is_dir($dir.'/'.$y) === false ){mkdir($dir.'/'.$y);}
            if( is_dir($dir.'/'.$y.'/'.$m) === false ){mkdir($dir.'/'.$y.'/'.$m);}
        }
        $dir = $y.'/'.$m.'/';


        $file_width = Image::make($file)->width();
        $file_height = Image::make($file)->height();
        $file_mime = Image::make($file)->mime();
        if ($file_mime == 'image/jpeg'){
            $file_extension = '.jpg';
        }elseif($file_mime == 'image/jpg') {
            $file_extension = '.jpg';
        }elseif($file_mime == 'image/png') {
            $file_extension = '.png';
        }else{
            $file_extension = '';
        }

        $file_name =Helpers::createSlug($id).'-'.time().'-'.$file_extension;
        $img = Image::make($file);
        $img->save('images/source/'.$dir.$file_name);
        $ratio = $file_width / $file_height;
        foreach($dimension as $key_=>$val){
            if($key_ < 2){
                continue;
            }

            $di = explode("x", $val);
            if ($ratio < 1) {
                $targetWidth = $di['1'] * $ratio;
                if ($targetWidth < $di['0']){
                    $targetWidth = $di['0'];
                }
                Image::make('images/source/'.$dir.$file_name)->resize(
                    $targetWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                }
                )->crop($di['0'], $di['1'], 0, 0)->save(
                    'images/' . $val . '/' . $file_name, 60
                );
            }else{
                $targetHeight = $di['0'] / $ratio;
                if ($targetHeight < $di['1']){
                    $targetHeight = $di['1'];
                }
                Image::make('images/source/'.$dir.$file_name)->resize(null,$targetHeight, function ($constraint) {$constraint->aspectRatio();})->crop($di['0'], $di['1'], 0, 0)->save('images/'.$val.'/' .$dir.$file_name, 60);
            }
            if ($file_width >= 1000){
                $targetWidth = '1000';
            }else{
                $targetWidth = $file_width;
            }
            Image::make('images/source/' . $dir.$file_name)->resize(
                $targetWidth, null, function ($constraint) {
                $constraint->aspectRatio();
            }
            )->save('images/large/'.$dir.$file_name, 60);
        }

//        $img = new Images();
//        $img->user_id = '5b4243ae0ec4ef4844005f82';
//        $img->name = $main_imgggg;
//        $img->sort = 1;
//        $img->images_url = $dir.$file_name;
//        $img->keyword = $main_imgggg;
//        $img->save();

        $content = Content::find($_id);
        $content->img_new = 1;
        $content->main_img = $dir.$file_name;
        $content->save();
    }


    public function id($id)
    {
        $title = DB::table('title')->where('ID',$id)->where('migration','<>',1)->first();

        ///start keyword
        $keywordDB = DB::table('NewsKeywords')->where('NewsID',$title['ID'])->get();
        $tags = array();
        foreach ($keywordDB as $record){
            $tagsrecord = DB::table('keyword')->where('ID',$record['KeywordID'])->first();
            $iod = (string)$tagsrecord['_id'];
            array_push($tags,$iod);
        }

        ///end keyword


        /////start main_img
        $img_id = str_split($title['ID']);
        if(strlen($title['ID']) == '3'){
            $main_img = '/upload/photo/news/0/0/600x338o/'.$img_id['0'].$img_id['1'].$img_id['2'].'.jpg';
        }elseif(strlen($title['ID']) == '4'){
            if($img_id['1'] === '0'){
                $img_id['1'] ='';
            }
            if($img_id['2'] === '0'){
                $img_id['2'] ='';
            }
            $main_img = '/upload/photo/news/0/'.$img_id['0'].'/600x338o/'.$img_id['1'].$img_id['2'].$img_id['3'].'.jpg';
        }elseif(strlen($title['ID']) == '5'){
            if($img_id['3'] === '0'){
                $img_id['3'] ='';
            }
            if($img_id['2'] === '0'){
                $img_id['2'] ='';
            }
            $main_img = '/upload/photo/news/'.$img_id['0'].'/'.$img_id['1'].'/600x338o/'.$img_id['2'].$img_id['3'].$img_id['4'].'.jpg';
        }elseif(strlen($title['ID']) == '6'){
            if($img_id['3'] === '0'){
                $img_id['3'] ='';
            }
            if($img_id['4'] === '0'){
                $img_id['4'] ='';
            }
            $main_img = '/upload/photo/news/'.$img_id['0'].$img_id['1'].'/'.$img_id['2'].'/600x338o/'.$img_id['3'].$img_id['4'].$img_id['5'].'.jpg';
        }
        /////start main_img

        /// start section
        $sectionDB = DB::table('section')->where('ID',$title['CatID'])->first();
        if(count($sectionDB) > 0){
            $section = (string)$sectionDB['_id'];
        }else{
            $section = '-1';
        }
        /// end section



        ///start related
        $relatedDB = DB::table('RelatedNews')->where('NewsID',$title['ID'])->get();
        $related = array();
        foreach ($relatedDB as $record){
            $relatedrecord = DB::table('content')->where('NewsID',$record['RelatedID'])->first();
            $ido = (string)$relatedrecord['_id'];
            array_push($related,$ido);
        }
        ///end related







        $body = DB::table('body')->where('NewsID',$title['ID'])->orderBy('ID', 'ASC')->get();
        if(count($body) > 0){
            $content = '';
            foreach ($body as $record){
                ////start photos
                if($record['MediaExt'] !== null){
                    $img_id = str_split($record['ID']);
                    if(strlen($record['ID']) == '3'){
                        $img = '/upload/photo/Parags/0/0/425x239o/'.$img_id['0'].$img_id['1'].$img_id['2'].'.'.$record['MediaExt'];
                    }elseif(strlen($record['ID']) == '4'){
                        $img = '/upload/photo/Parags/0/'.$img_id['0'].'/425x239o/'.$img_id['1'].$img_id['2'].$img_id['3'].'.'.$record['MediaExt'];
                    }elseif(strlen($record['ID']) == '5'){
                        $img = '/upload/photo/Parags/'.$img_id['0'].'/'.$img_id['1'].'/425x239o/'.$img_id['2'].$img_id['3'].$img_id['4'].'.'.$record['MediaExt'];
                    }elseif(strlen($record['ID']) == '6'){
                        $img = '/upload/photo/Parags/'.$img_id['0'].$img_id['1'].'/'.$img_id['2'].'/425x239o/'.$img_id['3'].$img_id['4'].$img_id['5'].'.'.$record['MediaExt'];
                    }
                    $content.= '<img src="'.$img.'">';
                }
                /////end photos
                $content.= $record['Text'];
            }
            $content = $this->content_array($content);
            foreach ($content as $key=>$record){
                $content[$key]= htmlentities($record);
            }
            $type = 'article';


        }else{
            $gallery = DB::table('NewsGallery')->where('NewsID',$title['ID'])->orderBy('ID', 'ASC')->get();
            $type = 'album';
        }

        ///start video
        $video = DB::table('NewsPropsData')->where('NewsID',$title['ID'])->first();
        if(isset($video)){

            if(count($video) >0){
                $vid = $this->content_array(htmlentities('<iframe width="100%" height="315" src="'.$video['CharValue'].'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'));
                $content = array_merge($content,$vid);
            }
        }
        ///end video



        $URLName = $title['URLName'];
        if(empty($URLName)){
            $url = $title['ID'];
        }else{
            $url = $title['ID'].'/'.htmlentities(Helpers::createSlug($URLName));;
        }


        if($title['MidStage'] === '6'){
            $case = 'published';
        }else{
            $case = 'delete';
        }

        if($title['CatID'] === '27'){
            $type = 'opinion';
        }

        $data = new Content();
        $data['news_id'] = (int)$title['ID'];
        $data['user_id'] = '5b4243ae0ec4ef4844005f82';
        $data['title'] = htmlentities($title['Name']);
        $data['url_text'] = htmlentities($URLName);
        $data['url'] = $url;
        $data['description'] = htmlentities($title['Clip']);
        $data['section'] = $section;
        $data['content'] = $content;
        $data['written'] = htmlentities($title['WriterSource']);
        $data['main_img'] = $main_img;
        $data['img_description'] = htmlentities($title['PhotoTxt']);
        $data['hits'] = (int)htmlentities($title['Views']);
        $data['hits_today'] = 0;
        $data['album'] = null;
        $data['video'] = null;
        $data['search'] = Helpers::search(htmlentities($title['Name']).','.htmlentities($URLName));
        $data['tags'] = $tags;
        $data['related'] = $related;
        $data['type'] = $type;
        $data['case'] = $case;
        $data['options'] = array('inside_img');
        $data['seo'] = array('ads');
        $data['publication_date'] = Carbon::parse($title['AddDate'])->toDateTimeString();
        $data['updated_at'] = Carbon::parse($title['LastUpdate'])->toDateTimeString();
        $data['created_at'] = Carbon::parse($title['PublishDate'])->toDateTimeString();
        $data->save();



        DB::table('title')
            ->where('_id', $title['_id'])
            ->update(['migration' => 1]);

    }

    public function duplicate(){

        $content = DB::table('content')->where('section','5b4243ae0ec4ef4844005f82')->take(1000)->orderBy('_id', 'desc')->get();
        foreach ($content as $key=>$record){
            $content = Content::find($record['_id']);
            $content->section = '5e1e4f77ea3e7c7ae03274ea';
            $content->save();
        }

//
//        $content = Content::take(5)->orderBy('_id', 'DESC')->get();
//
//        foreach ($content as $record){
//            $xxx['record'] = $record;
//            $duplicate = Content::where('news_id',$record->news_id)->where('_id','<>',$record->_id)->orderBy('_id', 'DESC')->get();
//            foreach ($duplicate as $ddddd){
//                Content::destroy($ddddd['_id']);
//            }
//            $xxx['duplicate'] = $duplicate;
//
//        }
    }

    public function record(){

        $content = DB::table('content_')->where('migration_record','<>',1)->take(50000)->orderBy('news_id', 'asc')->get();

        foreach ($content as $key=>$record){
            DB::table('content')->insert($record);
            DB::table('content_')->where('_id', $record['_id'])->update(['migration_record' => 1]);

        }


return ' <meta http-equiv="refresh" content="1">';

    }



    public function related($id){
//        $content = Content::where('related_new','<>',1)->take(20)->orderBy('publication_date','desc')->get();
        $content = Content::where('news_id','<',560467)->take(20)->orderBy('publication_date','desc')->get();
        foreach ($content as $records){
            $related = array();
            $relatedrecord = DB::table('RelatedNews')->where('NewsID',$records['news_id'])->get();
            foreach ($relatedrecord as $record){
                $related_content = Content::where('news_id',$record['RelatedID'])->first();
                array_push($related,$related_content['_id']);
            }
//            $cont = Content::find($records['_id']);
//            $cont->related = $related;
          //  $cont->save();
//            return $related;
        }
exit();

    }

    public function section(){
        $title = DB::table('title')->where('migration_section', '<>', 1)->take(10000)->orderBy('ID', 'dsec')->get();
            foreach ($title as $records) {
                $sectionDB = DB::table('section')->where('section_ID', $records['CatID'])->first();
                if (isset($sectionDB)) {
                    if (count($sectionDB) > 0) {
                        $section = (string)$sectionDB['_id'];
                    } else {
                        $section = '-1';
                    }
                } else {
                    $section = '-1';
                }
                $content = Content::where('news_id',(int)$records['ID'])->first();
                $content->section = $section;
                $content->save();
                DB::table('title')->where('_id', $records['_id'])->update(['migration_section' => 1]);
            }
        }
    public function k($id){
        $content = DB::table('content')->where('news_id','<',(int)$id)->where('tags',null)->take(100)->orderBy('news_id', 'desc')->get();
        return $content;
        foreach ($content as $records){
            $related = array();
            $relatedrecord = DB::table('RelatedNews')->where('NewsID',$records['news_id'])->get();
            foreach ($relatedrecord as $record){
                $related_content = Content::where('news_id',$record['RelatedID'])->first();
                array_push($related,$related_content['_id']);
            }
//            $cont = Content::find($records['_id']);
//            $cont->related = $related;
            //  $cont->save();
//            return $related;
        }

//
//        $content = Content::take(5)->orderBy('_id', 'DESC')->get();
//
//        foreach ($content as $record){
//            $xxx['record'] = $record;
//            $duplicate = Content::where('news_id',$record->news_id)->where('_id','<>',$record->_id)->orderBy('_id', 'DESC')->get();
//            foreach ($duplicate as $ddddd){
//                Content::destroy($ddddd['_id']);
//            }
//            $xxx['duplicate'] = $duplicate;
//
//        }
    }

    public function content_idxxxx(){
        $NewsKeywords = DB::table('NewsKeywords')->where('migration', 'exists', false)->take(50000)->orderBy('_id', 'asc')->get();
        foreach ($NewsKeywords as $key=>$record){
            $content = Content::select('_id','news_id')->where('news_id',(int)$record['NewsID'])->first();
            $keyword_li = new keyword_link();
            $keyword_li->content_id = $content['_id'];
            $keyword_li->keyword_id = (int)$record['KeywordID'];
            $keyword_li->save();
            DB::table('NewsKeywords')->where('_id','=',$record['_id'])->update(['migration' => 1]);
        }
        echo '<meta http-equiv="refresh" content="3">';
    }
    public function content_id(){
        $NewsKeywords = keyword_link::where('migrationx', 'exists', false)->take(1000)->orderBy('_id', 'desc')->get();
        foreach ($NewsKeywords as $key=>$record){
            $record['keyword_'] = keyword_link::where('content_id',$record['content_id'])->where('keyword_id', (int)$record['keyword_id'])->orderBy('_id', 'desc')->get();
            if(count($record['keyword_']) > 1){
               return $record;
            }
           DB::table('keyword_link')->where('_id', $record['_id'])->update(['migrationx' => 1]);
        }

        echo '<meta http-equiv="refresh" content="3">';
    }

    public function last_update(){
        $content = Content::where('migrationx_last_update', 'exists', false)->take(5000)->orderBy('news_id','desc')->get();
        foreach ($content as $record){
            $rec = Content::find($record['_id']);
            $rec['migrationx_last_update'] = 1;
//            $rec['last_update_date'] = $record['updated_at'];
            $rec['last_update_date'] = $record['publication_date'];
            $rec->save();
        }
        echo '<meta http-equiv="refresh" content="3">';

    }

    public function count(){
//        $content = DB::table('keyword_link')
        $content = Content::where('migrationx_last_update', 'exists', false)
            ->count();

        echo $content;
        echo '<meta http-equiv="refresh" content="3">';

    }


    public function test(){

        try {
            echo $this->inverse(ddd) . "<BR>";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "<BR>";
        }
        echo "<hr>";

//        try {
//            echo $this->inverse(0) . "<BR>";
//        } catch (Exception $e) {
//            echo 'Caught exception: ',  $e->getMessage(), "<BR>";
//        }

// Continue execution
    }

public function inverse($x) {
        if (!$x) {
            throw new Exception('Division by zero.');
        }
        return 1/$x;
    }


    public function test_url($x) {
        if (!$x) {
            throw new Exception('Division by zero.');
        }
        return str_replace('https://www.gololy.com/','',urldecode($x));
    }



}
