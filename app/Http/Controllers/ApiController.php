<?php

namespace App\Http\Controllers;
use App\Http\Models\Content;
use App\Http\Models\Section;
use App\Http\Models\Section_link;
use App\Http\Models\Images;
use App\Http\Models\Tracking;
use App\Http\Models\Main;
use App\Http\Models\Opinion;
use App\Http\Models\Ads;
use App\Http\Models\Hits;
use App\Http\Models\Option;
use App\Http\Models\Keyword;
use App\Http\Models\Keyword_link;
use App\Http\Helpers\Helpers;
use Carbon\Carbon;

use Illuminate\Http\Request;
use ImageModel;
use Cache;
class ApiController extends Controller
{
    public function option(Request $request)
    {
        $option = Option::get();
        if(count($option) > 0){
            foreach ($option as $opt){
                Option::destroy($opt->_id);
            };
        }
        Option::create($request['data']);
    }
    public function content($type,$case,$section,$start_date,$end_date,$key)
    {

        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $start_date =  new Carbon((new Carbon($start_date.' 00:00:01'))->format('d-m-yy\TH:i:s.uP'));
        $end_date =  new Carbon((new Carbon($end_date.' 23:59:59'))->format('d-m-yy\TH:i:s.uP'));

        if($section === 'section'){
            $content['content'] = Content::
            select('title','url','section','news_id','type','case','user_id','update_user_id','last_update_date','publication_date','written','main_img','img_description')
                ->where('type',$type)
                ->where('case',$case)
                ->where('last_update_date','>', $start_date)
                ->where('last_update_date','<', $end_date)
                ->orderBy('last_update_date','DESC')
                ->get();
            foreach ($content['content'] as $key=>$record){
                $record['hits'] = Hits::select('hits')->where('news_id',(int)$record['news_id'])->first();
            }

        }else{
            $content['content'] = Content::
            select('title','url','section','news_id','type','case','user_id','update_user_id','last_update_date','publication_date','written','main_img','img_description')
                ->where('type',$type)
                ->where('case',$case)
                ->where('section',$section)
                ->where('last_update_date','>', $start_date)
                ->where('last_update_date','<', $end_date)
                ->orderBy('last_update_date','DESC')
                ->get();
            foreach ($content['content'] as $key=>$record){
                $record['hits'] = Hits::select('hits')->where('news_id',(int)$record['news_id'])->first();
            }
        }
        $content['section'] = Section::where('case','on')->orderBy('sort','ASC')->get();
        return $content;
    }
    public function tracking($id,$key)
    {

        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content['content'] = Tracking::
        where('content_id',$id)
            ->orderBy('_id','DESC')
            ->get();
        $content['section'] = Section::where('case','on')->orderBy('sort','ASC')->get();
        return $content;
    }
    public function home($id,$key)
    {

        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $home = array();
        $content_today = array();
        $content_today_hits = 0;
        $content_yesterday = array();
        $content_yesterday_hits = 0;
        $content_week = array();
        $content_week_hits = 0;


        $content = Content::
        select('title','url','news_id','last_update_date','publication_date')
            ->where('user_id',$id)
            ->where('case','published')
            ->where('last_update_date','>', Carbon::now()->subDays(7))
            ->orderBy('last_update_date','DESC')
            ->get();
        foreach ($content as $key=>$record){
            $content[$key]['hits'] = Hits::select('hits')->where('news_id',(int)$record['news_id'])->first();
            if ($record->last_update_date->isToday()){
                array_push($content_today,$record);
                $content_today_hits = $content_today_hits + $content[$key]['hits']['hits'];
            }
            if ($record->last_update_date->isYesterday()){
                array_push($content_yesterday,$record);
                $content_yesterday_hits = $content_yesterday_hits + $content[$key]['hits']['hits'];
            }
            array_push($content_week,$record);
            $content_week_hits = $content_week_hits + $content[$key]['hits']['hits'];
        }
        $home['content_today'] = $content_today;
        $home['content_today_hits'] = $content_today_hits;
        $home['content_today_count'] = count($content_today);
        $home['content_yesterday'] = $content_yesterday;
        $home['content_yesterday_hits'] = $content_yesterday_hits;
        $home['content_yesterday_count'] = count($content_yesterday);
        $home['content_week'] = $content_week;
        $home['content_week_hits'] = $content_week_hits;
        $home['content_week_count'] = count($content_week);
        return $home;
    }


    public function content_ajax($type,$case,$section,$date,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        if($date === '1'){
            $date = Carbon::today();
        }else{
            $date = str_replace('+',' ',$date);
            $date =  new Carbon($date);
        }
        if($section === 'section'){
            $content['content'] = Content::
            select('title','description','url','created_at','section','news_id','main_img','img_description','type','case','user_id','update_user_id','last_update_date','publication_date')
                ->where('type',$type)
                ->where('case',$case)
                ->where('last_update_date','>', $date)
                ->orderBy('last_update_date','DESC')
                ->take(20)->get();
            foreach ($content['content'] as $key=>$record){
                $record['hits'] = Hits::select('hits')->where('news_id',(int)$record['news_id'])->first();
            }
        }else{
            $content['content'] = Content::
            select('title','description','url','created_at','section','news_id','main_img','img_description','type','case','user_id','update_user_id','last_update_date','publication_date')
                ->where('type',$type)
                ->where('case',$case)
                ->where('section',$section)
                ->where('last_update_date','>', $date)
                ->orderBy('last_update_date','DESC')
                ->take(20)->get();
            foreach ($content['content'] as $key=>$record){
                $record['hits'] = Hits::select('hits')->where('news_id',(int)$record['news_id'])->first();
            }
        }
        $content['section'] = Section::where('case','on')->orderBy('sort','ASC')->get();
        return $content;
    }
    public function content_ajax_api($type,$case,$section,$date,$first_id,$last_id,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content['time'] = $date;

        $date = str_replace('+',' ',$date);
        $date =  new Carbon($date);


        $content['unpublished'] = Content::select('case','last_update_date')->where('case','unpublished')->where('last_update_date','>', $date)->orderBy('last_update_date','DESC')->count();
        $content['published'] = Content::select('case','last_update_date')->where('case','published')->where('last_update_date','>', $date)->orderBy('last_update_date','DESC')->count();
        $content['ready'] = Content::select('case','last_update_date')->where('case','ready')->where('last_update_date','>', $date)->orderBy('last_update_date','DESC')->count();
        $content['review'] = Content::select('case','last_update_date')->where('case','review')->where('last_update_date','>', $date)->orderBy('last_update_date','DESC')->count();



        return $content;
    }
    public function content_ajax_in($user_id,$content_id,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){return '404';}
        $content = Content::select('user_in_date','user_in')->find($content_id);
        if(isset($content['user_in'])){
            if($content['user_in'] == $user_id){

            }elseif($content['user_in'] !== $user_id && $content['user_in_date'] > Carbon::now()->subSeconds(45)){
                return $content['user_in'];
            }
        }
        $content->user_in_date = Carbon::now();
        $content->user_in = $user_id;
        $content->save();
        return 'done';
        //Carbon::now()->subMinutes(2);

    }
    public function content_copy(Request $request)
    {
        $output = $request['content'];
//        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
//        if($output['web_key'] != $web_key){
//            return '404';
//        }
        $ids = explode(',', $output['ids']);
        foreach ($ids as $key=>$record){
            $content = Content::find($record);
            if($output['current_case'] === $content['case']){
                $content->case = $output['case'];
                $content->update_user_id = $output['user_id'];
                if($output['case'] == 'published'){
                    $content->publication_date = Carbon::now();
                }
                $content->save();
                $this->create_tracking($content);
            }
        }
        return 'done';
    }
    public function content_create($key)
    {
//        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
//        if($web_key != $key){
//            return '404';
//        }
        $content['main'] = Main::get();
        foreach ($content['main'] as  $record){
            $record['content'] = Content::select('title')->find($record['content_id']);
        }
        $content['section'] = Section::where('case','on')->orderBy('sort', 'ASC')->get();
        $content['opinion'] = Opinion::where('case','on')->orderBy('sort','ASC')->get();
//        $content['files'] = Content::where('type','files')->where('case','published')->orderBy('id','DESC')->take(5)->get();
        return $content;
    }
    public function content_create_save(Request $request)
    {
        $back_massage = array();
        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $title = Content::where('title',$output['title'])->first();
        if(!empty($title)){
            array_push($back_massage, "title");
        }
        $url_text = Content::where('url_text',$output['url_text'])->first();
        if(!empty($url_text)){
            array_push($back_massage, "url_text");
        }
        if(!empty($back_massage)){
            return $back_massage;
        }




        $news_id = Content::select('news_id')->orderBy('news_id', 'desc')->first();
        $news_id = (int)$news_id->news_id+1;
        $section_url = Section::find($output['section'][0]);
//        $url = $section_url['url'].'/'.$news_id.'/'.$output['url'];
        $url = $output['url'].'-'.$news_id;

        $keyword = explode(',', $output['tags']);
        $tags = array();
        foreach ($keyword as $key=>$record){
            $tag_id = $this->keyword_create($record);
            array_push($tags, $tag_id);
        }

        $content = new Content();
        $content->user_id = $output['user_id'];
        $content->title = $output['title'];
        $content->url_text = $output['url_text'];
        $content->url = $url;
        $content->description = $output['description'];
        $content->content = $output['content'];
        $content->written = $output['written'];
        $content->section = $output['section'][0];
        if($output['case'] == 'published'){
            $content->publication_date = Carbon::now();
        }
        $content->last_update_date = Carbon::now();
        $content->tags = $tags;
        $content->tag_text = $output['tags'];
        $content->related = $output['related'];
        $content->main_img = $output['main_img'];
        $content->img_description = $output['img_description'];
        $content->album = $output['album'];
        $content->video = $output['video'];
        $content->search = $output['search'];
        $content->opinion = $output['opinion'];
        $content->type = $output['type'];
        $content->case = $output['case'];
        $content->options = $output['options'];
        $content->seo = $output['seo'];
        $content->hits = 0;
        $content->news_id = $news_id;
        $content->save();
//        $this->section_link($output['section'],$content->_id,$content->type,$content->case,'new');
        $this->create_tracking($content);

        if(!empty($tags)){
            foreach ($tags as $key=>$record){
                $keyword_link = new Keyword_link();
                $keyword_link->content_id =  $content->_id;
                $keyword_link->keyword_id =  (int)$record;
                $keyword_link->save();
            }
        }

        array_push($back_massage, "done");
        return $back_massage;

    }
    public function content_create_save_old(Request $request)
    {

        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $back_massage = array();
        $title = Content::where('title',$output['title'])->first();
        if(!empty($title)){
            $output['case'] = 'unpublished';
            array_push($back_massage, "title");
        }
        $url_text = Content::where('url_text',$output['url_text'])->first();
        if(!empty($url_text)){
            $output['case'] = 'unpublished';
            array_push($back_massage, "url_text");
        }
        $news_id = Content::select('news_id')->orderBy('news_id', 'desc')->first();
        $news_id = (int)$news_id->news_id+1;
        $section_url = Section::find($output['section'][0]);
//        $url = $section_url['url'].'/'.$news_id.'/'.$output['url'];
        $url = $output['url'].'-'.$news_id;



        $keyword = explode(',', $output['tags']);
        $tags = array();
        foreach ($keyword as $key=>$record){
            $tag_id = $this->keyword_create($record);
            array_push($tags, $tag_id);
        }

        $content = new Content();
        $content->user_id = $output['user_id'];
        $content->title = $output['title'];
        $content->url_text = $output['url_text'];
        $content->url = $url;
        $content->description = $output['description'];
        $content->content = $output['content'];
        $content->written = $output['written'];
        $content->section = $output['section'][0];
        if($output['case'] == 'published'){
            $content->publication_date = Carbon::now();
        }
        $content->last_update_date = Carbon::now();

        $content->tags = $tags;
        $content->tag_text = $output['tags'];
        $content->related = $output['related'];
        $content->main_img = $output['main_img'];
        $content->img_description = $output['img_description'];
        $content->album = $output['album'];
        $content->video = $output['video'];
        $content->search = $output['search'];
        $content->opinion = $output['opinion'];
        $content->type = $output['type'];
        $content->case = $output['case'];
        $content->options = $output['options'];
        $content->seo = $output['seo'];
        $content->hits = 0;
        $content->news_id = $news_id;
        $content->save();
        $this->section_link($output['section'],$content->_id,$content->type,$content->case,'new');
        $this->create_tracking($content);

        if(!empty($tags)){
            foreach ($tags as $key=>$record){
                $keyword_link = new Keyword_link();
                $keyword_link->content_id =  $content->_id;
                $keyword_link->keyword_id =  (int)$record;
                $keyword_link->save();
            }
        }
        if(empty($back_massage)){
            array_push($back_massage, "done");
        }
        $back['id'] = $content['_id'];
        $back['massage'] = $back_massage;
        return $back;

    }
    public function content_update($id,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';

        if($web_key != $key){
            return '404';
        }
        $content['content'] = Content::find($id);
        $content['main'] = Main::get();
        foreach ($content['main'] as  $record){
            $record['content'] = Content::select('title')->find($record['content_id']);
        }
        if(!empty($content['content']['album'])){
            $content['content']['album_images'] = Content::find($content['content']['album']);
            $content['content']['album_images']['images'] = Images::where('album',$content['content']['album'])->orderBy('sort', 'ASC')->get();
        }
        $content['related'] = Content::select('_id','title')->whereIn('_id', $content['content']['related'])->get();
        $content['section_link'] = Section_link::select('_id','name','section_id')->where('content_id',$id)->get();
        $content['section'] = Section::select('_id','name')->where('case','on')->orderBy('sort', 'ASC')->get();
        $content['opinion'] = Opinion::where('case','on')->orderBy('sort','ASC')->get();
        $content['tags'] = Keyword_link::select('keyword_id')->where('content_id',$id)->orderBy('sort','ASC')->get();

        foreach ($content['tags'] as $rec){
            $tag_keyword =  keyword::select('keyword')->where('keyword_id',$rec->keyword_id)->first();
            if(!empty($tag_keyword)){
                $rec['keyword'] =  $tag_keyword->keyword;
            }
        }
//        $content['files'] = Content::where('type','files')->where('case','published')->orderBy('id','DESC')->take(10)->get();
        return $content;

    }
    public function content_update_save(Request $request)
    {





        $back_massage = array();
        $output = $request['content'];

//        if($output['user_id'] === '5b4243ae0ec4ef4844005f82'){
//            return $request->all();
//        }


        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }

        $title = Content::where('title',$output['title'])->where('_id','<>',$output['id'])->first();
        if(!empty($title)){
            array_push($back_massage, "title");
        }
        $url_text = Content::where('url_text',$output['url_text'])->where('_id','<>',$output['id'])->first();
        if(!empty($url_text)){
            array_push($back_massage, "url_text");
        }
        if(!empty($back_massage)){
            return $back_massage;
        }




        $keyword = explode(',', $output['tags']);
        $tags = array();
        foreach ($keyword as $key=>$record){
            $tag_id = $this->keyword_create($record);
            array_push($tags, $tag_id);
        }


        $content = Content::find($output['id']);
//        $section_url = Section::find($output['section'][0]);
//        $url = $section_url['url'].'/'.$content['news_id'].'/'.$output['url'];
        $url = $output['url'].'-'.$content['news_id'];

        if($output['case'] == 'published'){
            if(!isset($content->publication_date)){
                $content->publication_date = Carbon::now();
            }
        }
        $content->update_user_id = $output['user_id'];
        $content->title = $output['title'];
        $content->url_text = $output['url_text'];
        $content->url = $url;
        $content->description = $output['description'];
        $content->content = $output['content'];
        $content->written = $output['written'];
        $content->section = $output['section'][0];
        $content->tags = $tags;
        $content->tag_text = $output['tags'];
        $content->last_update_date = Carbon::now();
        $content->related = $output['related'];
        $content->main_img = $output['main_img'];
        $content->img_description = $output['img_description'];
        $content->album = $output['album'];
        $content->video = $output['video'];
        $content->search = $output['search'];
        $content->opinion = $output['opinion'];
        $content->type = $output['type'];
        $content->case = $output['case'];
        $content->options = $output['options'];
        $content->seo = $output['seo'];
        $content->save();
        if($output['main'] !== '0'){
            $this->main_save_content($output['user_id'],$output['id'],$output['main']);
        }
        $this->create_tracking($content);
        Keyword_link::where('content_id', $content->_id)->delete();
        if(!empty($tags)){
            foreach ($tags as $key=>$record){
                $keyword_link = new Keyword_link();
                $keyword_link->content_id =  $content->_id;
                $keyword_link->keyword_id =  (int)$record;
                $keyword_link->save();
            }
        }
        Cache::forget('Desktop_'.$content->news_id);
        Cache::forget('Mobile_'.$content->news_id);

        if(empty($back_massage)){
            array_push($back_massage, "done");
        }
        array_push($back_massage, "done");
        return $back_massage;
    }
    public function content_update_save_old(Request $request)
    {
        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $back_massage = array();

        $title = Content::where('title',$output['title'])->where('_id','<>',$output['id'])->first();
        if(!empty($title)){
            $output['case'] = 'unpublished';
            array_push($back_massage, "title");
        }
        $url_text = Content::where('url_text',$output['url_text'])->where('_id','<>',$output['id'])->first();
        if(!empty($url_text)){
            $output['case'] = 'unpublished';
            array_push($back_massage, "url_text");
        }

        $keyword = explode(',', $output['tags']);
        $tags = array();
        foreach ($keyword as $key=>$record){
            $tag_id = $this->keyword_create($record);
            array_push($tags, $tag_id);
        }


        $content = Content::find($output['id']);
        $section_url = Section::find($output['section'][0]);
        $url = $section_url['url'].'/'.$content['news_id'].'/'.$output['url'];

        if($output['case'] == 'published'){
            if(!isset($content->publication_date)){
                $content->publication_date = Carbon::now();
            }
        }
        $content->update_user_id = $output['user_id'];
        $content->title = $output['title'];
        $content->url_text = $output['url_text'];
        $content->url = $url;
        $content->description = $output['description'];
        $content->content = $output['content'];
        $content->written = $output['written'];
        $content->section = $output['section'][0];
        $content->tags = $tags;
        $content->tag_text = $output['tags'];
        $content->last_update_date = Carbon::now();
        $content->related = $output['related'];
        $content->main_img = $output['main_img'];
        $content->img_description = $output['img_description'];
        $content->album = $output['album'];
        $content->video = $output['video'];
        $content->search = $output['search'];
        $content->opinion = $output['opinion'];
        $content->type = $output['type'];
        $content->case = $output['case'];
        $content->options = $output['options'];
        $content->seo = $output['seo'];
        $content->save();
        if($output['main'] !== '0'){
            $this->main_save_content($output['user_id'],$output['id'],$output['main']);
        }
        $this->create_tracking($content);
        Keyword_link::where('content_id', $content->_id)->delete();
        if(!empty($tags)){
            foreach ($tags as $key=>$record){
                $keyword_link = new Keyword_link();
                $keyword_link->content_id =  $content->_id;
                $keyword_link->keyword_id =  (int)$record;
                $keyword_link->save();
            }
        }

        if(empty($back_massage)){
            array_push($back_massage, "done");
        }
        $back['id'] = $content['_id'];
        $back['massage'] = $back_massage;
        return $back;
    }
    public function content_out($id,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';

        if($web_key != $key){
            return '404';
        }
        $content = Content::find($id);
        $content['hits'] = Hits::select('hits')->where('news_id',(int)$content['news_id'])->first();
        return $content;

    }
    public function create_tracking($tracking)
    {
        $content = new Tracking();
        $content->content_id = $tracking->_id;
        $content->user_id = $tracking->user_id;

        $content->title = $tracking->title;
        $content->description = $tracking->description;
        $content->content = $tracking->content;
        $content->tags = $tracking->tags;
        $content->search = $tracking->search;
        $content->case = $tracking->case;
        $content->related = $tracking->related;
        $content->type = $tracking->type;
        $content->written = $tracking->written;
        $content->main_img = $tracking->main_img;
        $content->img_description = $tracking->img_description;
        $content->album = $tracking->album;
        $content->video = $tracking->video;
        $content->options = $tracking->options;
        $content->seo = $tracking->seo;
        $content->save();
    }


    public function section_link($section_id,$content_id,$type,$case,$update){
        if($update === 'update'){
            $record = Section_link::where('content_id',$content_id)->get();
            foreach ($record as $rec){
                Section_link::destroy($rec->_id);
            }
        }
        foreach ($section_id as $record){
            $section = Section::find($record);
            $section_link = new Section_link();
            $section_link->content_id = $content_id;
            $section_link->type = $type;
            $section_link->case = $case;
            $section_link->section_id = $record;
            $section_link->section_name = $section['name'];
            $section_link->save();
        }
    }
    public function content_related($keyword,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        if($keyword === 'empty'){
            $related = Content::where('case', 'published')
                ->orderBy('news_id', 'desc')
                ->take(20)->get();
        }else{
            $newkeyword = str_replace('+',' ',$keyword);
            $newkeyword = Helpers::search($newkeyword);
            $allkeyword = explode('+',$keyword);
            $related = Content::where('title', 'like', '%'.$newkeyword.'%')

                ->where('case', 'published')
                ->orderBy('news_id', 'desc')
                ->take(10)->get();
        }
        return $related;
    }
    public function related(Request $request)
    {
        $output = $request['content'];
        $keyword = explode(',', $output['tags']);
        $content = array();
        foreach ($keyword as $key=>$record){
            $Keyword = Keyword::where('keyword',$record)->first();
            $Keyword_link = Keyword_link::where('keyword_id',$Keyword['keyword_id'])->orderBy('updated_at','DESC')->take(10)->get();
            $r = array();
            foreach ($Keyword_link as $rec){
                $tag = Content::select('title','publication_date','url')->where('_id',$rec['content_id'])->where('case','published')->first();
                if(isset($tag)){
                    array_push($r, $tag);
                }
            }
            $content[$record] = $r;
        }
        return $content;
    }
    public function search($keyword,$date,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $keyword = str_replace('+',' ',$keyword);
        $date = str_replace('+',' ',$date);
        $date =  new Carbon($date);
        $content['content'] = Content::
        select('title','description','url','created_at','section','news_id','main_img','img_description','type','case','user_id','update_user_id','last_update_date','publication_date')
            ->where('title', 'LIKE', '%'.$keyword.'%')
            ->orWhere('news_id',(int)$keyword)
            ->orWhere('search', 'LIKE', '%'.Helpers::search($keyword).'%')
            ->where('last_update_date','<', $date)
            ->orderBy('news_id','DESC')
            ->take(10)->get();
        $content['section'] = Section::where('case','on')->orderBy('sort','DESC')->get();
        return $content;
    }

    public function images($key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content = Images::orderBy('created_at','DESC')->paginate(20);
        return $content;
    }
    public function images_search($keyword,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        if($keyword === 'empty'){
            $content = Images::where('case', 'published')
                ->orderBy('created_at', 'desc')
                ->paginate(21);
        }else{
            $keyword = str_replace('+',' ',$keyword);
            $keyword = Helpers::search($keyword);
            $content = Images::where('keyword', 'LIKE', '%'.$keyword.'%')
                ->orderBy('created_at', 'desc')
                ->paginate(21);
        }
        return $content;
    }
    public function images_save(Request $request)
    {
//        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
//        if($web_key != $key){
//            return '404';
//        }


        $dimension = array('source','large','150x150','450x250','450x450','750x450');
        $y = date("Y");
        $m = date("m");
        foreach ($dimension as $record){
            $dir = 'images/'.$record;
            if( is_dir($dir) === false ){mkdir($dir);}
            if( is_dir($dir.'/'.$y) === false ){mkdir($dir.'/'.$y);}
            if( is_dir($dir.'/'.$y.'/'.$m) === false ){mkdir($dir.'/'.$y.'/'.$m);}
        }
        $dir = $y.'/'.$m.'/';
        $output = $request['output'];



        if(is_array($output)){
            if(count($output) > 1){
                $news_id = Content::select('news_id')->orderBy('news_id', 'desc')->first();
                $news_id = (int)$news_id->news_id+1;
                $album = new Content();
                $album->title = htmlentities($output[0]['name']);
                $album->news_id = $news_id;
                $album->description = '';
                $album->album = '';
                $album->video = '';
                $album->files = '';
                $album->tags = '';
                $album->main_img = '';
                $album->main_img_description = $output[0]['name'];
                $album->related = [];
                $album->section = '';
                $album->written = '';
                $album->content = [];
                $album->case = 'unpublished';
                $album->search = Helpers::search(htmlentities($output[0]['name']));
                $album->user_id = $output[0]['user_id'];
                $album->type = 'album';
                $album->save();
            }
            foreach ($output as $key=> $record){

                $file = base64_decode($record['images']);
                $file_width = ImageModel::make($file)->width();
                $file_height = ImageModel::make($file)->height();
                $file_mime = ImageModel::make($file)->mime();
                if ($file_mime == 'image/jpeg'){
                    $file_extension = '.jpg';
                }elseif($file_mime == 'image/jpg') {
                    $file_extension = '.jpg';
                }elseif($file_mime == 'image/png') {
                    $file_extension = '.png';
                }else{
                    $file_extension = '';
                }
                if($file_extension == '.gif' || $file_extension == ''){
                    continue;
                }
                $file_name =Helpers::createSlug($record['name']).'-'.time().'-'.$key.$file_extension;
                $img = ImageModel::make($file);
                if($record['watermark'] == 'on'){
                    $img->insert('images/watermark.png', 'center');
                }
                $img->save('images/source/'.$dir.$file_name);
                if($key === 0){
                    if(count($output) > 1) {
                        $album->main_img = $dir.$file_name;
                        $album->save();
                    }
                }
                $ratio = $file_width / $file_height;
                foreach($dimension as $key_=>$val){
                    if($key_ < 2){
                        continue;
                    }
                    $di = explode("x", $val);
                    if ($file_width < $file_height) {

                        $targetWidth = $di['1'] * $ratio;

                        if ($targetWidth < $di['0']){
                            $targetWidth = $di['0'];
                        }
                        ImageModel::make('images/source/'.$dir.$file_name)
                            ->resize($targetWidth, null, function ($constraint) {$constraint->aspectRatio();})
                            ->crop($di['0'], $di['1'], 0, 0)
                            ->save('images/'.$val.'/' .$dir.$file_name, 60);
                    }else{
                        $targetHeight = $di['0'] / $ratio;

                        if ($targetHeight < $di['1']){
                            $targetHeight = $di['1'];
                        }
                        ImageModel::make('images/source/'.$dir.$file_name)
                            ->resize(null,$targetHeight, function ($constraint) {$constraint->aspectRatio();})
                            ->crop($di['0'], $di['1'], 0, 0)
                            ->save('images/'.$val.'/' .$dir.$file_name, 60);
                    }
                    if ($file_width >= 1000){
                        $targetWidth = '1000';
                    }else{
                        $targetWidth = $file_width;
                    }
                    ImageModel::make('images/source/' . $dir.$file_name)
                        ->resize($targetWidth, null, function ($constraint) { $constraint->aspectRatio();})
                        ->save('images/large/'.$dir.$file_name, 60);
                }


                $photo = new Images();
                $photo->user_id = $record['user_id'];
                if(isset($album)){
                    $photo->album = $album->_id;
                }
                $photo->name = $record['name'];
                $photo->sort = (int)$key+1;
                $photo->images_url = $dir.$file_name;
                $photo->keyword = Helpers::search($record['name']);
                $photo->width = (int)$file_width;
                $photo->height = (int)$file_height;
                $photo->save();





            }

        }
    }
    public function images_crop(Request $request)
    {


//        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
//        if($web_key != $key){
//            return '404';
//        }




        $output = $request['output'];



        if(is_array($output)){

            foreach ($output as $key=> $record){
                $file = base64_decode($record['image']);
                $file_width = ImageModel::make($file)->width();
                $file_height = ImageModel::make($file)->height();
                $file_mime = ImageModel::make($file)->mime();

                if ($file_mime == 'image/jpeg'){
                    $file_extension = '.jpg';
                }elseif($file_mime == 'image/jpg') {
                    $file_extension = '.jpg';
                }elseif($file_mime == 'image/png') {
                    $file_extension = '.png';
                }else{
                    $file_extension = '';
                }

                if($file_extension == '.gif' || $file_extension == ''){
                    continue;
                }


                $file_name =$record['id'];
                $dir = 'images/'.$record['dimensions'].'/'.$record['y'].'/'.$record['m'].'/';
                $img = ImageModel::make($file)->save($dir.$file_name);

            }
        }
    }
    public function album_get($key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content = Content::where('type','album')->orderBy('created_at','DESC')->paginate(20);
        foreach ($content as $key=>$record){
            $record['images'] = Images::where('album',$record->_id)->get();
        }

        return $content;
    }

    public function keyword_create($keyword)
    {
        $tags = Keyword::where('keyword',$keyword)->first();
        if(empty($tags)){
            $keyword_id = Keyword::select('keyword_id')->orderBy('keyword_id', 'desc')->first();
            if(empty($keyword_id)){
                $keyword_id = 1;
            }else{
                $keyword_id = (int)$keyword_id->keyword_id+1;
            }
            $newkeyword = new Keyword();
            $newkeyword->keyword_id = $keyword_id;
            $newkeyword->keyword = $keyword;
            $newkeyword->save();
            return $newkeyword['keyword_id'];
        }else{
            return $tags['keyword_id'];
        }
    }



    public function main($area,$section,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content['main'] = Main::where('area',$area)->where('section',$section)->get();
        foreach ($content['main'] as  $record){
            $record['content'] = Content::find($record['content_id']);
        }

        $content['content'] = Content::where('case','published')->orderBy('created_at','DESC')->paginate(20);
        $content['section'] = Section::where('case','on')->orderBy('sort','DESC')->get();

        return $content;

    }
    public function main_save(Request $request)
    {
        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $ids = $output['ids'];
        $section = $output['section'];
        $area = $output['area'];
        $ids = explode(',',$ids);

        Main::where('area',$area)->where('section',$section)->delete();
        foreach ($ids as $record){
            $main = new Main();
            $main->user_id = $output['user_id'];
            $main->section = $section;
            $main->area = $area;
            $main->content_id = $record;
            $main->save();
        }
        return 'done';
    }
    public function main_related($keyword,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        if($keyword === 'empty'){
            $related = Content::where('case', 'published')->orderBy('news_id', 'desc')->take(20)->get();
        }else{
            $newkeyword = str_replace('+',' ',$keyword);
            $newkeyword = Helpers::search($newkeyword);
            $allkeyword = explode('+',$keyword);
            $related = Content::where('title', 'like', '%'.$newkeyword.'%')->where('case', 'published')->orderBy('news_id', 'desc')->take(10)->get();
        }
        return $related;
    }
    public function main_save_content($user_id,$content_id,$main_id)
    {
//        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
//        if($output['web_key'] != $web_key){
//            return '404';
//        }


        $main = Main::find($main_id);
        $main->user_id = $user_id;
        $main->content_id = $content_id;
        $main->save();
        return 'done';
    }

    public function section($key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content['section'] = Section::where('case','on')->orderBy('sort','ASC')->get();
        return $content;

    }
    public function section_create_save(Request $request)
    {
        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $name = Section::where('name', 'LIKE', '%'.$output['name'].'%')->first();
        if(!empty($name)){
            return 'name';
        }
        $url = 'category/'.$output['url'];
        $url_text = Section::where('url_text', 'LIKE', '%'.$output['url_text'].'%')->first();
        if(!empty($url_text)){
            return 'url_text';
        }
        $url_ = Section::where('url', 'LIKE', '%'.$url.'%')->first();
        if(!empty($url_)){
            return 'url';
        }

        $content = new Section();
        $content->user_id = $output['user_id'];
        $content->name = $output['name'];
        $content->url_text = $output['url_text'];
        $content->url = $url;
        $content->description = $output['description'];
        $content->section_tree = $output['section_tree'];
        $content->case = 'on';
        $content->sort = 0;
        $content->save();
        return 'done';
    }
    public function section_update($id,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content['section_id'] = Section::find($id);
        $content['section'] = Section::where('_id','<>',$id)->where('case','on')->orderBy('sort', 'desc')->get();
        return $content;
    }
    public function section_update_save(Request $request)
    {
        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $name = Section::where('name', 'LIKE', '%'.$output['name'].'%')->where('_id','<>',$output['id'])->first();
        if(!empty($name)){
            return 'name';
        }
        $content = Section::find($output['id']);
        $content->user_id = $output['user_id'];
        $content->name = $output['name'];
        $content->description = $output['description'];
        $content->section_tree = $output['section_tree'];
        $content->save();
        return 'done';
    }
    public function section_sort_save(Request $request)
    {
        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $ids = explode(',',$output['ids']);
        foreach ($ids as $key=>$record){
            $sort = Section::find($record);
            $sort->sort = ($key+1);
            $sort->save();
        }
        return 'done';
    }
    public function section_delete(Request $request)
    {
        $output = $request['content'];

        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $sort = Section::find($output['id']);
        $sort->case = 'off';
        $sort->save();
        return 'done';
    }


    public function opinion($key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content['opinion'] = Opinion::where('case','on')->orderBy('sort','ASC')->get();
        return $content;

    }
    public function opinion_create_save(Request $request)
    {
        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $name = Opinion::where('name', 'LIKE', '%'.$output['name'].'%')->first();
        if(!empty($name)){
            return 'name';
        }
        $content = new Opinion();
        $content->user_id = $output['user_id'];
        $content->name = $output['name'];
        $content->facebook = $output['facebook'];
        $content->twitter = $output['twitter'];
        $content->email = $output['email'];
        $content->description = $output['description'];
        $content->photo = $output['photo'];
        $content->case = 'on';
        $content->save();
        return 'done';
    }
    public function opinion_update($id,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content['opinion_id'] = Opinion::find($id);
        return $content;
    }
    public function opinion_update_save(Request $request)
    {
        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $name = Opinion::where('name', 'LIKE', '%'.$output['name'].'%')->where('_id','<>',$output['id'])->first();
        if(!empty($name)){
            return 'name';
        }
        $content = Opinion::find($output['id']);
        $content->user_id = $output['user_id'];
        $content->name = $output['name'];
        $content->facebook = $output['facebook'];
        $content->twitter = $output['twitter'];
        $content->email = $output['email'];
        $content->description = $output['description'];
        $content->photo = $output['photo'];

        $content->save();
        return 'done';
    }
    public function opinion_delete(Request $request)
    {
        $output = $request['content'];

        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $sort = Opinion::find($output['id']);
        $sort->case = 'off';
        $sort->save();
        return 'done';
    }



    public function ads($key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content['ads'] = Ads::where('case','on')->orWhere('case','disabled')->orderBy('sort','ASC')->get();
        return $content;

    }
    public function ads_create_save(Request $request)
    {
        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $name = Ads::where('name', 'LIKE', '%'.$output['name'].'%')->first();
        if(!empty($name)){
            return 'name';
        }
        $content = new Ads();
        $content->user_id = $output['user_id'];
        $content->name = $output['name'];
        $content->link = $output['link'];
        $content->title = $output['title'];
        $content->_blank = $output['_blank'];
        $content->nofollow = $output['nofollow'];
        $content->desktop_img = $output['desktop_img'];
        $content->desktop_code = $output['desktop_code'];
        $content->phone_img = $output['phone_img'];
        $content->phone_code = $output['phone_code'];
        $content->case = $output['case'];
        $content->save();


        if($content->_blank === 'yse'){
            $_blank = '_blank';
        }else{
            $_blank = '';
        }
        if($content->nofollow === 'yse'){
            $nofollow = 'nofollow';
        }else{
            $nofollow = '';
        }

        $myfile = fopen("ad_file/".$content->_id.".php", "w") or die("Unable to open file!");
        if($content->case === 'on') {
            $txt1 = "<?php $"."device = (new App\Http\Helpers\Helpers)->mobiledetect();\n";
            $txt2 = "if("."$"."device === 'Mobile'){?>\n";
            if(empty($content->phone_code)){
                $txt3 = '<a href="'.$content->link.'" target="'.$_blank.'" rel="'.$nofollow.'"><img src="'.$content->phone_img.'" title="'.$content->title.'"/></a>';
                $txt4 = "\n";
            }else{
                $txt3 = html_entity_decode($output['phone_code']);
                $txt4 = "\n";
            }
            $txt5 = "<?php }else{?>\n";
            if(empty($content->desktop_code)){
                $txt6 = '<a href='.$content->link.' target="'.$_blank.'" rel="'.$nofollow.'"><img src="'.$content->desktop_img.'" title="'.$content->title.'"/></a>';
                $txt7 = "\n";
            }else{
                $txt6 = html_entity_decode($output['desktop_code']);
                $txt7 = "\n";
            }
            $txt8 = "<?php }?>\n";
            fwrite($myfile, $txt1);
            fwrite($myfile, $txt2);
            fwrite($myfile, $txt3);
            fwrite($myfile, $txt4);
            fwrite($myfile, $txt5);
            fwrite($myfile, $txt6);
            fwrite($myfile, $txt7);
            fwrite($myfile, $txt8);
        }else{
            $txt1 = "";
            fwrite($myfile, $txt1);
        }
        fclose($myfile);
        return 'done';
    }
    public function ads_update($id,$key)
    {
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($web_key != $key){
            return '404';
        }
        $content['ads_id'] = Ads::find($id);
        return $content;
    }
    public function ads_update_save(Request $request)
    {
        $output = $request['content'];
        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $name = Ads::where('name', 'LIKE', '%'.$output['name'].'%')->where('_id','<>',$output['id'])->first();
        if(!empty($name)){
            return 'name';
        }
        $content = Ads::find($output['id']);
        $content->user_id = $output['user_id'];
        $content->name = $output['name'];
        $content->link = $output['link'];
        $content->title = $output['title'];
        $content->_blank = $output['_blank'];
        $content->nofollow = $output['nofollow'];
        $content->desktop_img = $output['desktop_img'];
        $content->desktop_code = $output['desktop_code'];
        $content->phone_img = $output['phone_img'];
        $content->phone_code = $output['phone_code'];
        $content->case = $output['case'];
        $content->save();

        if($content->_blank == 'yes'){
            $_blank = '_blank';
        }else{
            $_blank = '';
        }
        if($content->nofollow == 'yes'){
            $nofollow = 'nofollow';
        }else{
            $nofollow = '';
        }
        if (file_exists("ad_file/".$content->_id.".php")) {
            unlink("ad_file/" . $content->_id . ".php");
        }
        $myfile = fopen("ad_file/".$content->_id.".php", "w") or die("Unable to open file!");
        if($content->case === 'on') {

            $txt1 = "<?php $"."device = (new App\Http\Helpers\Helpers)->mobiledetect();\n";
            $txt2 = "if("."$"."device === 'Mobile'){?>\n";
            if(empty($content->phone_code)){
                $txt3 = '<a href="'.$content->link.'" target="'.$_blank.'" rel="'.$nofollow.'"><img src="'.$content->phone_img.'" title="'.$content->title.'"/></a>';
                $txt4 = "\n";
            }else{
                $txt3 = html_entity_decode($output['phone_code']);
                $txt4 = "\n";
            }
            $txt5 = "<?php }else{?>\n";
            if(empty($content->desktop_code)){
                $txt6 = '<a href='.$content->link.' target="'.$_blank.'" rel="'.$nofollow.'"><img src="'.$content->desktop_img.'" title="'.$content->title.'"/></a>';
                $txt7 = "\n";
            }else{
                $txt6 = html_entity_decode($output['desktop_code']);
                $txt7 = "\n";
            }
            $txt8 = "<?php }?>\n";

            fwrite($myfile, $txt1);
            fwrite($myfile, $txt2);
            fwrite($myfile, $txt3);
            fwrite($myfile, $txt4);
            fwrite($myfile, $txt5);
            fwrite($myfile, $txt6);
            fwrite($myfile, $txt7);
            fwrite($myfile, $txt8);
        }else{
            $txt1 = "";
            fwrite($myfile, $txt1);
        }
        fclose($myfile);

        return 'done';
    }
    public function ads_delete(Request $request)
    {
        $output = $request['content'];

        $web_key = 'f0cGY5RrMkgh9WDrIkAx6EJKYhMF7vcWez44whkfBDxdaBqtQpTcaHVPapC81iKD3xxxHmMcobBqqFcCj0oNYL8HH1PQzmexcE0TXm0SiuzmehkAzcUdwC7Lj1KyPkckb7FCbZVQPKpGPm5nh5GYSYd';
        if($output['web_key'] != $web_key){
            return '404';
        }
        $sort = Ads::find($output['id']);
        $sort->case = 'off';
        $sort->save();
        return 'done';
    }
}
