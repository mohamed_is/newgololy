<?php

namespace App\Http\Controllers;
use App\Http\Helpers\Helpers;
use App\http\Models\Hits;
use Illuminate\Http\Request;
use App\Http\Models\Section;
use App\Http\Models\Keyword_link;
use App\Http\Models\Content;
use Carbon\Carbon;
use Cache;

class SectionController extends Controller
{

    public function category(Request $request)
    {
        if(isset($_GET['page'])) {
            $page = $_GET['page'];
        }else{
            $page = '0';
        }
        $cash = 'news_'.$page;
        if (Cache::has($cash)){
            return Cache::get($cash);
        } else {
            return Cache::remember($cash, 1, function () use($page) {
                if ($page === '0') {
                    $content = Content::
                    select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')
                        ->with('sectionData:name,url')
                        ->where('case', 'published')
                        ->orderBy('publication_date', 'desc')
                        ->take(15)->get();
//                    $next_url = '/news?page='.$content[15]['publication_date'];
                }else{
                    $content = Content::
                    select('title', 'url', 'publication_date', 'section','main_img', 'img_description','type','news_id')
                        ->where('case', 'published')
                        ->where('publication_date', '<',Carbon::parse($page))
                        ->orderBy('publication_date', 'desc')
                        ->take(15)->get();
//                    $next_url = 'news?page='.$content[15]['publication_date'];
                }
//                $views = Hits::where('news_id', $content->news_id)->first();
                $tags = 'اخر الاخبار';
                $current_url = '/news';
                return view('section.index',compact('content','tags','current_url'))->render();
            });
        }
    }

    public function loadmore(Request $request)
    {
        $limit = $request->limit;
        $offset = $request->offset;
        if($request->page == 'news'){
                        $content = Content::
                        select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')
                            ->with('hitsData:news_id,hits')
                            ->with('sectionData:name,url')
                            ->where('case', 'published')
                            ->orderBy('publication_date', 'desc')
                            ->skip((int)$offset)->take((int)$limit)->get();
            return $content;
        }elseif ($request->page == 'section'){
            $url = 'category/'.$request->sec_name;
            $section = Section::where('url', $url)->first();
            $content = Content::
            select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')
                ->with('hitsData:news_id,hits')
                ->where('case', 'published')
                ->where('section', $section->_id)
                ->orderBy('publication_date', 'desc')
                ->skip((int)$offset)->take((int)$limit)->get();
                return $content;
        }elseif ($request->page == 'search'){
            $keyword =  $request->sec_name;
            $content = Content::select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')
                ->with('sectionData:_id,name,url')
                ->with('sectionData:name,url')
                ->with('hitsData:news_id,hits')
                ->where('search', 'LIKE', '%'.Helpers::search($keyword).'%')
                ->orderBy('publication_date', 'desc')
                ->skip((int)$offset)->take((int)$limit)->get();
            return $content;
        }
    }


    public function section(Request $request,$title)
    {


        if(isset($_GET['page'])) {
            $page = $_GET['page'];
        }else{
            $page = '0';
        }
        $cash = $title.'_'.$page;
        $url = 'category/'.$title;
        if (Cache::has($cash)){
            return Cache::get($cash);
        } else {
            return Cache::remember($cash, 1, function () use($url,$page,$title) {
                    $section = Section::where('url', $url)->first();
                if ($page === '0') {
//                    return $section->name;
                    $content = Content::
                    select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')
                        ->with('hitsData:news_id,hits')
                        ->where('case', 'published')
                        ->where('section', $section->_id)
                        ->orderBy('publication_date', 'desc')
                        ->take(15)->get();
//                    $next_url = '/'.$section['url'].'?page='.$content[15]['publication_date'];
                }else{
                    $content = Content::
                    select('title', 'url', 'publication_date', 'section','main_img', 'img_description','type','news_id')
                        ->with('hitsData:news_id,hits')
                        ->where('case', 'published')
                        ->where('section', $section->_id)
                        ->where('publication_date', '<',Carbon::parse($page))
                        ->orderBy('publication_date', 'desc')
                        ->take(15)->get();
//                    $next_url = '/'.$section['url'].'?page='.$content[15]['publication_date'];
                }
//                foreach ($content as $record){
//                    foreach ($record['hitsData'] as $view){
//                        return $record;
//                    }
//                }
//                return $content;

//                $views = Hits::where('news_id', $content->news_id)->first();
                $tags = $section->name;
                $current_url = $section['url'];
                return view('section.index',compact('content','tags','current_url'))->render();
            });
        }
    }


    public function section_old(Request $request,$id,$title)
    {


        if(isset($_GET['page'])) {
            $page = $_GET['page'];
        }else{
            $page = '0';
        }
        $cash = $title.'_'.$page;
        $url = 'section/'.$id.'/'.$title;
        if (Cache::has($cash)){
            return Cache::get($cash);
        } else {
            return Cache::remember($cash, 1, function () use($url,$page,$title,$id) {
                $section = Section::where('section_ID', $id)->first();
                if(empty($section)){
                    $section = Section::where('section_ID','-1')->first();
                }
                if ($page === '0') {
                    $content = Content::
                    select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')
                        ->where('case', 'published')
                        ->where('section', $section->_id)
                        ->orderBy('news_id', 'desc')
                        ->take(16)->get();

                    $next_url = '/'.$section['url'].'?page='.$content[15]['publication_date'];
                }else{
                    $content = Content::
                    select('title', 'url', 'publication_date', 'section','main_img', 'img_description','type','news_id')
                        ->where('case', 'published')
                        ->where('section', $section->_id)
                        ->where('publication_date', '<',Carbon::parse($page))
                        ->orderBy('news_id', 'desc')
                        ->take(16)->get();
                    $next_url = '/'.$section['url'].'?page='.$content[15]['publication_date'];
                }
                $tags = $section->name;
                $current_url = $section['url'];
                return view('section.index',compact('content','tags','next_url','current_url'))->render();
            });
        }
    }
    public function rss_category()
    {

        $content = Content::
        where('case','published')
            ->where('publication_date','<',Carbon::now()->addMinutes('-10'))
            ->orderBy('news_id', 'desc')
            ->limit(50)
            ->get();
        $info['page_title'] = 'أهل مصر';
        $info['page_description'] = 'أهل مصر صحافة كل يوم احدث الاخبار لحظة بلحظة';
        $info['page_link'] = env('APP_URL');
        return response()->view('section.rss',compact('content','info'))->header('Content-Type', 'text/xml');

    }
    public function rss_section($title)
    {
        $url = 'news/'.$title;
        if (Cache::has($url)){
            return Cache::get($url);
        } else {
            return Cache::remember($url, 0, function () use($url) {
                $section = Section::where('url',$url)->first();
                $tags = $section->name;
                $content = Content::
                where('case','published')
                    ->where('section',$section->_id)
                    ->orderBy('news_id', 'desc')
                    ->limit(50)
                    ->get();
                $info['page_title'] = $tags;
                $info['page_description'] = 'أهل مصر صحافة كل يوم احدث الاخبار لحظة بلحظة';
                $info['page_link'] = env('APP_URL').'/'.$url;
                return response()->view('section.rss',compact('content','info'))->header('Content-Type', 'text/xml');
            });
        }
    }
    public function rss_google_news($title)
    {
        $url = 'news/'.$title;
        if (Cache::has($url)){
            return Cache::get($url);
        } else {
            return Cache::remember($url, 0, function () use($url,$title) {
                $section = Section::where('url',$url)->first();
                $tags = $section->name;
                $content = Content::
                where('case','published')
                    ->where('section',$section->_id)
                    ->orderBy('news_id', 'desc')
                    ->limit(50)
                    ->get();
                $info['page_title'] = $tags;
                $info['page_description'] = 'أهل مصر صحافة كل يوم احدث الاخبار لحظة بلحظة';
                $info['page_link'] = env('APP_URL').'/rss/google_news/'.$title;
                return response()->view('section.google_news',compact('content','info'))->header('Content-Type', 'text/xml');
            });
        }
    }

    public function rss_section_old($id)
    {
        $url = 'rss/'.$id;
        if (Cache::has($url)){
            return Cache::get($url);
        } else {
            return Cache::remember($url, 0, function () use($url,$id) {
                $section = Section::where('section_ID',$id)->first();
                if(empty($section)){
                    $section = Section::where('section_ID','-1')->first();
                }
                $tags = $section->name;
                $content = Content::
                where('case','published')
                    ->where('section',$section->_id)
                    ->orderBy('news_id', 'desc')
                    ->limit(50)
                    ->get();
                $info['page_title'] = $tags;
                $info['page_description'] = 'أهل مصر صحافة كل يوم احدث الاخبار لحظة بلحظة';
                $info['page_link'] = env('APP_URL').'/'.$url;
                return response()->view('section.rss',compact('content','info'))->header('Content-Type', 'text/xml');
            });
        }
    }
    public function sitemaps()
    {
        $content = Content::
        where('case','published')
            ->orderBy('news_id', 'desc')
            ->first();
        $con = Content::where('case','published')->count();
        $count = $con/999;
        if(strpos($count, '.') !== false){
            $count = substr($count, 0, strpos($count, "."));
            $count = $count+1;
        }
//        return $count;
        return response()->view('section.sitemaps',compact('content','count'))->header('Content-Type', 'text/xml');
    }
    public function sitemap_news(Request $request)
    {
        $content = Content::
        where('case','published')
            ->where('publication_date','>',Carbon::now()->addDays('-2'))
            ->orderBy('news_id', 'desc')
            ->get();
        $info['page_title'] = 'أهل مصر';
        $info['page_link'] = env('APP_URL');
        return response()->view('section.sitemap',compact('content','info'))->header('Content-Type', 'text/xml');
    }
    public function tags($id,$tags)
    {
        if(isset($_GET['page'])) {
            $page = $_GET['page'];
        }else{
            $page = '0';
        }
        $cash = $id.'_'.$page;
        $url = '/tags/'.$id.'/'.$tags;
        if (Cache::has($cash)){
            return Cache::get($cash);
        } else {
            return Cache::remember($cash, 0, function () use($url,$page,$tags,$id) {
                if ($page === '0') {
                    $tag = Keyword_link::where('keyword_id',(int)$id)->orderBy('_id', 'desc')->take(16)->get();
                    foreach ($tag as $record){
                        $record['content'] = Content::
                        select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')
                            ->where('_id',$record['content_id'])
                            ->where('case', 'published')
                            ->first();
                        $next_url = $url.'?page='.$record['_id'];
                    }
                }else{
                    $tag = Keyword_link::where('keyword_id',(int)$id)->where('_id', '<',$page)->orderBy('_id', 'desc')->take(16)->get();
                    foreach ($tag as $record){
                        $record['content'] = Content::
                        select('title', 'url', 'publication_date', 'section','main_img','img_description','type','news_id')
                            ->where('_id',$record->content_id)
                            ->where('case', 'published')
                             ->first();
                        $next_url = $url.'?page='.$record['_id'];
                    }
                }
                if(empty($next_url)){
                    $next_url = '';
                }
                $tags = str_replace("-"," ",htmlentities($tags));;
                $current_url = $url;
                return view('section.tags',compact('tag','tags','next_url','current_url'))->render();
            });
        }
    }
    public function tagsssss($id,$tags)
    {
        $tags = str_replace("-"," ",htmlentities($tags));
        if (Cache::has($id)){
            return Cache::get($id);
        } else {
            return Cache::remember($id, 0, function () use($id,$tags) {
                $tag = Keyword_link::where('keyword_id',(int)$id)->orderBy('created_at', 'desc')->take(16)->get();
                foreach ($tag as $record){
                    $record['content'] = Content::
                    select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')
                        ->where('_id',$record->contact_id)
                        ->where('case', 'published')
                        ->orderBy('news_id', 'desc')
                        ->first();
                }

                return view('section.tags',compact('tag','tags'))->render();
            });
        }


    }
    public function search(Request $request)
    {

//        $keyword =  $request['keyword'];
        $keyword =  $_GET['keyword'];
        if(empty($keyword)){
            return redirect('https://www.gololy.com/');
        }
        if(isset($_GET['page'])) {
            $page = $_GET['page'];
        }else{
            $page = '0';
        }
        $cash = 'search_'.$keyword;

        if (Cache::has($cash)){
            return Cache::get($cash);
        } else {
            return Cache::remember($cash, 1, function () use($page,$keyword) {
                if ($page === '0') {
//                    $content = Content::
//                    select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')
//                        ->where('search', 'LIKE', '%'.Helpers::search($keyword).'%')
//                        ->where('case', 'published')
//                        ->orderBy('publication_date', 'desc')
//                        ->take(16)->get();


                    $content = Content::select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')->with('sectionData:_id,name,url')->where('search', 'LIKE', '%'.Helpers::search($keyword).'%')->orderBy('publication_date', 'desc')->take(15)->get();
//                    foreach ($content as $con){
//                        return $con['sectionData']['name'];
//                    }
//                    return $content;

                }else{
//                    $content = Content::
//                    select('title', 'url', 'publication_date', 'section','main_img', 'img_description','type','news_id')
//                        ->where('search', 'LIKE', '%'.Helpers::search($keyword).'%')
//                        ->where('case', 'published')
//                        ->where('publication_date', '<',Carbon::parse($page))
//                        ->orderBy('publication_date', 'desc')
//                        ->take(16)->get();
                    $content = Content::select('title', 'url', 'publication_date', 'section','main_img', 'img_description', 'type','news_id')->with('sectionData:_id,name,url')->where('search', 'LIKE', '%'.Helpers::search($keyword).'%')->orderBy('publication_date', 'desc')->take(15)->get();

                }
//                $next_url = '/search?keyword='.$keyword.'&page='.$content[15]['publication_date'];

                $tags = $keyword;
                $current_url = '/search?keyword='.$keyword;
                return view('section.search',compact('content','tags','current_url'))->render();
            });
        }










    }
    public function rss_feed_facebook(Request $request)
    {
        $content = Content::
        where('case','published')
            ->where('instant','on')
            ->orderBy('news_id', 'desc')
            ->limit(10)
            ->get();
        foreach ($content as $record){

            $record['content_array'] = $record->content;
            foreach ($record->content as $key=>$rec){
                if(is_array($rec))
                {
                    if($rec['type'] == 'album'){
                        // return $key;
//                        $xxx = Content::where('mydasht_id',$rec['content'])->first();
//                        return $xxx;

                    }elseif($record['type'] == 'video'){
                        $rec[$key]['video'] = Content::where('mydasht_id',$rec['content'])->first();
                    }
                }
            }

            $record['section_name'] = Section::whereIn('mydasht_id',$record->section)->first();
        }
        // return $content;

        return view('section.rss_facebook',compact('content'));
    }
    public function images($id)
    {
        $img_id = str_split($id);
        if (strlen($id) == '3') {
            $file = 'https://www.ahlmasrnews.com/upload/photo/news/0/0/'.$img_id['0'].$img_id['1'].$img_id['2'].'.jpg';
        } elseif (strlen($id) == '4') {
            if ($img_id['1'] === '0') {
                $img_id['1'] = '';
            }
            if ($img_id['2'] === '0') {
                $img_id['2'] = '';
            }
            $file = 'https://www.ahlmasrnews.com/upload/photo/news/0/'.$img_id['0'].'/'.$img_id['1'].$img_id['2'].$img_id['3'].'.jpg';
        } elseif (strlen($id) == '5') {
            if ($img_id['2'] === '0') {
                $img_id['2'] = '';
            }
            if ($img_id['3'] === '0') {
                $img_id['3'] = '';
            }
            $file = 'https://www.ahlmasrnews.com/upload/photo/news/'.$img_id['0'].'/'.$img_id['1'].'/'.$img_id['2'].$img_id['3'].$img_id['4'].'.jpg';
        } elseif (strlen($id) == '6') {
            if ($img_id['3'] === '0') {
                $img_id['3'] = '';
            }
            if ($img_id['4'] === '0') {
                $img_id['4'] = '';
            }
            $file = 'https://www.ahlmasrnews.com/upload/photo/news/'.$img_id['0'].$img_id['1'].'/'.$img_id['2'].'/'.$img_id['3'].$img_id['4'].$img_id['5'].'.jpg';
        }




        $dimension = array('source','large','150x150','450x250','450x450','750x450');
        $y = date("Y");
        $m = date("m");
        foreach ($dimension as $record){
            $dir = 'images/'.$record;
            if( is_dir($dir) === false ){mkdir($dir);}
            if( is_dir($dir.'/'.$y) === false ){mkdir($dir.'/'.$y);}
            if( is_dir($dir.'/'.$y.'/'.$m) === false ){mkdir($dir.'/'.$y.'/'.$m);}
        }
        $dir = $y.'/'.$m.'/';


        $file_width = ImageModel::make($file)->width();
        $file_height = ImageModel::make($file)->height();
        $file_mime = ImageModel::make($file)->mime();
        if ($file_mime == 'image/jpeg'){
            $file_extension = '.jpg';
        }elseif($file_mime == 'image/jpg') {
            $file_extension = '.jpg';
        }elseif($file_mime == 'image/png') {
            $file_extension = '.png';
        }else{
            $file_extension = '';
        }

        $file_name =Helpers::createSlug($id).'-'.time().'-'.$file_extension;
        $img = ImageModel::make($file);
        $img->save('images/source/'.$dir.$file_name);
        $ratio = $file_width / $file_height;
        foreach($dimension as $key_=>$val){
            if($key_ < 2){
                continue;
            }

            $di = explode("x", $val);
            if ($ratio < 1) {
                $targetWidth = $di['1'] * $ratio;
                if ($targetWidth < $di['0']){
                    $targetWidth = $di['0'];
                }
                ImageModel::make('images/source/'.$dir.$file_name)->resize(
                    $targetWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                }
                )->crop($di['0'], $di['1'], 0, 0)->save(
                    'images/' . $val . '/' . $file_name, 60
                );
            }else{
                $targetHeight = $di['0'] / $ratio;
                if ($targetHeight < $di['1']){
                    $targetHeight = $di['1'];
                }
                ImageModel::make('images/source/'.$dir.$file_name)->resize(null,$targetHeight, function ($constraint) {$constraint->aspectRatio();})->crop($di['0'], $di['1'], 0, 0)->save('images/'.$val.'/' .$dir.$file_name, 60);
            }
            if ($file_width >= 1000){
                $targetWidth = '1000';
            }else{
                $targetWidth = $file_width;
            }
            ImageModel::make('images/source/' . $dir.$file_name)->resize(
                $targetWidth, null, function ($constraint) {
                $constraint->aspectRatio();
            }
            )->save('images/large/'.$dir.$file_name, 60);
        }
        return $dir.$file_name;

    }




}
