<?php namespace App\http\Models;

use Moloquent;

class Section extends Moloquent{
    protected $collection = 'section';
    protected $fillable=["name","keyword","description","lang","case","sort","section_type","section","section_tree","img"];
}