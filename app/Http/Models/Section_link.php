<?php namespace App\http\Models;

use Moloquent;

class Section_link extends Moloquent{
    protected $collection = 'section_link';
    protected $fillable=["name","section","content_id"];
}