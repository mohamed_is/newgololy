<?php namespace App\http\Models;

use Moloquent;

class Ads extends Moloquent{
    protected $collection = 'ads';
    protected $fillable=["user_id","name"];

}