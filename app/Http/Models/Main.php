<?php namespace App\Http\Models;

use Moloquent;

class Main extends Moloquent{
    protected $collection = 'main';
    protected $fillable=["publisher_id","user_id","type","area","content_id"];


    public function master(){
        return $this
            ->with('sub')
            ->get();
    }
    public function sub(){
        return $this->hasMany('App\Http\Models\Content');
    }

}