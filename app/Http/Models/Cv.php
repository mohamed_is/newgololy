<?php namespace App\http\Models;

use Moloquent;


class Cv extends Moloquent{
    protected $collection = 'wp_cv';
    protected $fillable=["ID","post_title","post_name","content","img_outside_1","post_date","post_modified"];
}
