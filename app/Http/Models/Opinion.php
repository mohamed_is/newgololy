<?php namespace App\http\Models;

use Moloquent;

class Opinion extends Moloquent{
    protected $collection = 'opinion';
    protected $fillable=["user_id","name"];

}