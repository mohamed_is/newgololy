<?php namespace App\http\Models;

use Moloquent;

class Option extends Moloquent{
    protected $collection = 'option';
    protected $fillable=["user_id","name","home1","home2","home3","home4","home5"];

}