<?php namespace App\http\Models;

use Moloquent;

class Images extends Moloquent{
    protected $collection = 'images';
    protected $fillable=["name","keyword","description","lang","case","sort"];
}