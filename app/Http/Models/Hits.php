<?php namespace App\http\Models;

use Moloquent;
use Carbon\Carbon;

class Hits extends Moloquent{
    protected $collection = 'hits';
    protected $dates = ['today'];
    public function scopeHits($query,$limit){
        return $query
            ->where('news_id','>',0)
            ->where('today','=', Carbon::today())
            ->orderBy('hits_today','desc')
            ->limit($limit)
            ->with('contentData')
            ->groupBy('news_id')
            ->get();
    }
    public function contentData(){
        return $this->hasOne('App\Http\Models\Content','news_id','news_id');
    }

    public function scopeSection($section){
        return $this->hasOne('App\Http\Models\section','_id', $section);
    }

}
