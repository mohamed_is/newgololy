<?php namespace App\http\Models;

use Moloquent;

class Tags extends Moloquent{
    protected $collection = 'tags';
}