<?php namespace App\Http\Models;

use Moloquent;
use DateTime;

class Content extends Moloquent{
    protected $collection = 'content';

    protected $fillable=["user_id","title","description","code",
                         "content","section","publication_date","lang","written","files",
                         "tags","related","case","search","main_img","main_img_description","album","video"];
//    protected $dates = ['publication_date','updated_at','publication_date'];
        protected $dates = ['publication_date','last_update_date','user_in_date'];
    public function scopeMain($query,$id){
        return $query
            ->where('_id',$id)
            ->with('sectionData')
            ->first();
    }
    public function scopeHits($query,$limit){
        return $query
//            ->select('title','url','publication_date','section','main_img','img_description','type')
            ->where('case','published')
            ->where('publication_date', '>', new DateTime('-1 day'))
            ->orderBy('hits','desc')
            ->limit($limit)
            ->with('sectionData')
            ->get();
    }
    public function scopeVideo($query,$limit){
        return $query
//            ->select('title','url','publication_date','section','main_img','img_description','type')
            ->where('case','published')
            ->where('type','video')
            ->orderBy('publication_date','desc')
            ->limit($limit)
            ->with('sectionData')
            ->get();
    }
    public function scopeLast($query,$ids,$limit){
        return $query
//            ->select('title','url','publication_date','section','main_img','img_description','type')
            ->where('case','published')
            ->whereNotIn('_id',$ids)
            ->orderBy('publication_date','desc')
            ->limit($limit)
            ->with('sectionData')
            ->get();
    }
    public function scopeSection($query,$ids,$section,$limit){
        return $query
//            ->select('title','url','publication_date','section','main_img','img_description','type')
            ->where('case','published')
            ->where('section',$section)
            ->whereNotIn('_id',$ids)
            ->orderBy('publication_date','desc')
            ->limit($limit)
            ->get();
    }
    public function sectionData(){
        return $this->hasMany('App\Http\Models\Section','_id','section');
    }

    public function hitsData(){
        return $this->hasOne('App\Http\Models\Hits','news_id','news_id');
    }


}
