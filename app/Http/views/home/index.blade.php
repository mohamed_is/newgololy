@section('title', 'Gololy  جولولى')
@section('description', 'جولولى')
@section('keywords', 'جولولى')
@section('robots', 'index, follow')
@section('ads', 'on')
@section('googlebot', 'index, follow')
@section('url', env('APP_URL'))
@section('image', env('APP_URL'))



@extends('layouts.app')
@section('content')
    <script type="application/ld+json">
    {
    "@context": "https://schema.org",
    "@type": "WebSite",
    "url": "{{env('APP_URL')}}",
    "potentialAction": {
        "@type": "SearchAction",
        "target": "{{env('APP_URL')}}/search/{search_term_string}",
        "query-input": "required name=search_term_string"
        }
    }
    {"@context": "https://schema.org",
    "@type": "WebSite",
    "name": "{{env('APP_NAME')}}",
    "url": "{{env('APP_URL')}}",
    "sameAs": ["https://www.facebook.com/###/","https://twitter.com/###","https://www.youtube.com/channel/###"]
    }
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "{{env('APP_URL')}}",
        "logo": "{{env('APP_URL')}}/assets/images/logo.png"
    }
</script>

    <div class="container" id="app-box" data-src="home">
        <div class="ads">
            <?php include(public_path().'/ad_file/5e1b2ae229cc0f069520b273.php')?>
{{--            <img src="/assets/img/ad/768x90.png" alt=""/>--}}
        </div>

        <!-- Main Section -->
        <div class="layout-wrap">
            <!-- Right side -->
            <div class="section-wrapper section-wrapper--reverse">
                @if(count($data['home_1']) > 0)
                <section class="right-side">
                    <div class="main-title">
                        <div class="main-title__box">
                            <h1>تريند</h1>
                        </div>
                    </div>
                    @foreach($data['arab'] as $key=>$record)
                    <div class="news-card news-card--card"><a class="news-card__link" href="/{{$record['url']}}"></a>
                        <div class="news-card__img">
                            <img src="{{env('APP_URL')}}/images/320x145{{$record['main_img']}}"/>
                            @if(!empty($record['video']))
                            <span class="icon news-card__video">
                                <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                  <g>
                                    <g>
                                            <!-- video -->
                                            <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                    </g>
                                  </g>
                                </svg>
                            </span>
                            @elseif(!empty($record['album']))
                            <!-- album -->
                                <span class="icon news-card__video">
                                <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                  <g>
                                    <g>
                                            <!-- album -->
                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                    </g>
                                  </g>
                                </svg>
                            </span>
                            @endif

                        </div>
                        <div class="news-card__caption">
                            <div class="news-card__trend"><span>{{$key+1}}</span></div>
                            <div class="news-card__details">
                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date"> | {{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                </div>
                                <h4 class="news-card__title">{{ html_entity_decode($record['title'])}}</h4>
                            </div>
                        </div>
                    </div>
                        @endforeach
                </section>
                @endif
                <section class="main-section">
                    @foreach($data['home_1'] as $key=>$record)
                        @if($key == 0)
                    <div class="news-card news-card--over news-card--large"><a class="news-card__link" href="{{$record['data']['url']}}"></a>
                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/520x600/{{$record['data']['main_img']}}"/>
                        </div>
                        <div class="news-card__caption">
                            <div class="news-card__details">
                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record['data']->sectionData[0]['name']}}</a><span class="or">|</span><span class="date"> | {{ (new App\Http\Helpers\Helpers)->datetime($record['data']['publication_date'])}}</span>
                                </div>
                                <h4 class="news-card__title">{{html_entity_decode($record['data']['title'])}}</h4>
                            </div>
                        </div>
                    </div>
                        @endif
                        @if($key > 0)
                    <div class="news-card news-card--over "><a class="news-card__link" href="{{$record['data']['url']}}"></a>
                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['data']['main_img']}}"/>
                        </div>
                        <div class="news-card__caption">
                            <div class="news-card__details">
                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record['data']->sectionData[0]['name']}}</a><span class="or">|</span><span class="date"> | {{ (new App\Http\Helpers\Helpers)->datetime($record['data']['publication_date'])}}</span>
                                </div>
                                <h4 class="news-card__title">{{html_entity_decode($record['data']['title'])}}</h4>
                            </div>
                        </div>
                    </div>
                        @endif
                        @endforeach
                </section>
            </div>
            <!-- End Right Side -->
            <!-- Left Side -->
            <aside class="left-side">
                <div class="ads ads--square">
                    <?php include(public_path().'/ad_file/5e1b2db629cc0f264e1fcf72.php')?>
                </div>
                <div class="main-title">
                    <div class="main-title__box">
                        <h1>آخر الأخبار</h1>
                    </div>
                </div>
                @foreach($data['last'] as $key=>$record)
                <div class="news-card news-card--card news-card--switch"><a class="news-card__link" href="/{{$record['url']}}"></a>
                    <div class="news-card__img"><img src="{{env('APP_URL')}}/images/150x150{{$record['main_img']}}"/>
                    </div>
                    <div class="news-card__caption">
                        <div class="news-card__details">
                            <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                            </div>
                            <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                        </div>
                    </div>
                </div>
                @endforeach
            </aside>
            <!-- End left Side -->
        </div>
        <!-- End Main Section -->



        <!-- Video Section -->
        <section class="dark-section">
            @if(count($data['video']) > 0)
                <div class="main-title">
                    <div class="main-title__box">
                        <h1>شاهد </h1>
                    </div>
                </div>
                <div class="section-wrapper">
                    <div class="main-section">
                        @foreach($data['video'] as $key=>$record)
                            @if($key == 0)
                                <div class="news-card news-card--over news-card--large"><a class="news-card__link" href="{{$record['url']}}"></a>
                                    <div class="news-card__img"><img src="{{env('APP_URL')}}/images/800x500/{{$record['main_img']}}"/><span class="icon news-card__video">
                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                      <g>
                        <g>
                          <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                        </g>
                      </g>
                    </svg></span>
                                    </div>
                                    <div class="news-card__caption">
                                        <div class="news-card__details">
                                            <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                            </div>
                                            <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <aside class="left-side">
                        @foreach($data['video'] as $key=>$record)
                            @if($key > 0)
                                <div class="news-card news-card--card news-card--switch dark-card"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                    <div class="news-card__img"><img src="{{env('APP_URL')}}/images/150x150/{{$record['main_img']}}"/><span class="icon news-card__video">
                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                      <g>
                        <g>
                          <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                        </g>
                      </g>
                    </svg></span>
                                    </div>
                                    <div class="news-card__caption">
                                        <div class="news-card__details">
                                            <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                            </div>
                                            <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </aside>
                </div>
            @endif
        </section>
        <!-- End Video Section -->


        <!-- Arab -->
        <section class="mt20">
            @if(count($data['arab']) > 0)
                <div class="main-title">
                    <div class="main-title__box">
                        <h1>عرب</h1>
                    </div>
                </div>
                <div class="layout-wrap">
                <div class="section-wrapper section-wrapper--reverse">
                    <section class="right-side">
                        @foreach($data['arab'] as $key=>$record)
                            @if($key == 1 || $key == 2)

                            <div class="news-card news-card--card"><a class="news-card__link" href="/{{$record['url']}}"></a>
                            <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['main_img']}}"/>
                                @if(!empty($record['video']))
                                    <span class="icon news-card__video">
                                <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                  <g>
                                    <g>
                                            <!-- video -->
                                            <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                    </g>
                                  </g>
                                </svg>
                            </span>
                                @elseif(!empty($record['album']))
                                <!-- album -->
                                    <span class="icon news-card__video">
                                <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                  <g>
                                    <g>
                                            <!-- album -->
                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                    </g>
                                  </g>
                                </svg>
                            </span>
                                @endif
                            </div>
                            <div class="news-card__caption">
                                <div class="news-card__details">
                                    <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                    </div>
                                    <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                </div>
                            </div>
                        </div>
                            @endif
                            @endforeach

                    </section>
                    <section class="main-section">
                        @foreach($data['arab'] as $key=>$record)
                            @php if($key > 0) continue; @endphp
                        <div class="news-card news-card--over news-card--large"><a class="news-card__link" href="{{$record['url']}}"></a>
                            <div class="news-card__img"><img src="{{env('APP_URL')}}/images/520x600/{{$record['main_img']}}"/>
                            </div>
                            <div class="news-card__caption">
                                <div class="news-card__details">
                                    <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                    </div>
                                    <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                </div>
                            </div>
                        </div>
                            @endforeach
                    </section>
                </div>
                <aside class="left-side">

                    @foreach($data['arab'] as $key=>$record)
                        @php if($key < 3) continue; @endphp
                        <div class="news-card news-card--card news-card--switch"><a class="news-card__link" href="/{{$record['url']}}"></a>
                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/150x150/{{$record['main_img']}}"/>
                        </div>
                        <div class="news-card__caption">
                            <div class="news-card__details">
                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                </div>
                                <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                            </div>
                        </div>
                    </div>
                    @endforeach
                        <div class="ads ads--square">
                            <?php include(public_path().'/ad_file/5e1b2dc229cc0f069a3b3872.php')?>
                        </div>

                </aside>
            </div>
            @endif
        </section>
        <!-- End Arab -->


        <!-- World -->
        <section class="mt20">
            @if(count($data['global']) > 0)
                <div class="main-title">
                    <div class="main-title__box">
                        <h1>عالمي</h1>
                    </div>
                </div>
                <div class="layout-wrap">
                    <div class="section-wrapper section-wrapper--reverse">
                        <section class="main-section ml15">
                            @foreach($data['global'] as $key=>$record)
                                @php if($key > 0) continue; @endphp
                                <div class="news-card news-card--over news-card--large"><a class="news-card__link" href="{{$record['url']}}"></a>
                                    <div class="news-card__img"><img src="{{env('APP_URL')}}/images/520x600/{{$record['main_img']}}"/>
                                    </div>
                                    <div class="news-card__caption">
                                        <div class="news-card__details">
                                            <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                            </div>
                                            <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </section>

                        <section class="right-side noMr">
                            @foreach($data['global'] as $key=>$record)
                                @if($key == 1 || $key == 2)

                                    <div class="news-card news-card--card"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['main_img']}}"/>
                                            @if(!empty($record['video']))
                                                <span class="icon news-card__video">
                                <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                  <g>
                                    <g>
                                            <!-- video -->
                                            <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                    </g>
                                  </g>
                                </svg>
                            </span>
                                            @elseif(!empty($record['album']))
                                            <!-- album -->
                                                <span class="icon news-card__video">
                                <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                  <g>
                                    <g>
                                            <!-- album -->
                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                    </g>
                                  </g>
                                </svg>
                            </span>
                                            @endif
                                        </div>
                                        <div class="news-card__caption">
                                            <div class="news-card__details">
                                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                                </div>
                                                <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </section>
                    </div>
                    <aside class="left-side">
                        <div class="ads ads--square">
                            <?php include(public_path().'/ad_file/5e1b2e9f29cc0f06996e73c2.php')?>
                        </div>
                        @foreach($data['global'] as $key=>$record)
                            @php if($key < 3) continue; @endphp
                            <div class="news-card news-card--card news-card--switch"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                <div class="news-card__img"><img src="{{env('APP_URL')}}/images/150x150/{{$record['main_img']}}"/>
                                </div>
                                <div class="news-card__caption">
                                    <div class="news-card__details">
                                        <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                        </div>
                                        <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </aside>
                </div>
            @endif
        </section>
        <!-- End World -->


        <!-- bollywood -->
        <section class="mt20">
            @if(count($data['bollywood']) > 0)
                <div class="main-title">
                    <div class="main-title__box">
                        <h1>بوليود</h1>
                    </div>
                </div>
                <div class="layout-wrap">
                    <div class="section-wrapper section-wrapper--reverse">
                        <section class="right-side">
                            @foreach($data['bollywood'] as $key=>$record)
                                @if($key == 1 || $key == 2)

                                    <div class="news-card news-card--card"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['main_img']}}"/>
                                            @if(!empty($record['video']))
                                                <span class="icon news-card__video">
                                <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                  <g>
                                    <g>
                                            <!-- video -->
                                            <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                    </g>
                                  </g>
                                </svg>
                            </span>
                                            @elseif(!empty($record['album']))
                                            <!-- album -->
                                                <span class="icon news-card__video">
                                <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                  <g>
                                    <g>
                                            <!-- album -->
                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                    </g>
                                  </g>
                                </svg>
                            </span>
                                            @endif

                                        </div>
                                        <div class="news-card__caption">
                                            <div class="news-card__details">
                                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                                </div>
                                                <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </section>
                        <section class="main-section">
                            @foreach($data['bollywood'] as $key=>$record)
                                @php if($key > 0) continue; @endphp
                                <div class="news-card news-card--over news-card--large"><a class="news-card__link" href="{{$record['url']}}"></a>
                                    <div class="news-card__img"><img src="{{env('APP_URL')}}/images/520x600/{{$record['main_img']}}"/>
                                    </div>
                                    <div class="news-card__caption">
                                        <div class="news-card__details">
                                            <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                            </div>
                                            <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </section>
                    </div>
                    <aside class="left-side">

                        @foreach($data['bollywood'] as $key=>$record)
                            @php if($key < 3) continue; @endphp
                            <div class="news-card news-card--card news-card--switch"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                <div class="news-card__img"><img src="{{env('APP_URL')}}/images/150x150/{{$record['main_img']}}"/>
                                </div>
                                <div class="news-card__caption">
                                    <div class="news-card__details">
                                        <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                        </div>
                                        <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="ads ads--square">
                            <?php include(public_path().'/ad_file/5e1b2e4029cc0f264e1fcf73.php')?>
                        </div>

                    </aside>
                </div>
            @endif
        </section>
        <!-- End bollywood -->


        <!-- turkey -->
        <section class="mt20">
            @if(count($data['turkey']) > 0)
                <div class="main-title">
                    <div class="main-title__box">
                        <h1>تركي</h1>
                    </div>
                </div>
                <div class="layout-wrap">
                    <div class="section-wrapper section-wrapper--reverse">
                        <section class="main-section ml15">
                            @foreach($data['turkey'] as $key=>$record)
                                @php if($key > 0) continue; @endphp
                                <div class="news-card news-card--over news-card--large"><a class="news-card__link" href="{{$record['url']}}"></a>
                                    <div class="news-card__img"><img src="{{env('APP_URL')}}/images/520x600/{{$record['main_img']}}"/>
                                    </div>
                                    <div class="news-card__caption">
                                        <div class="news-card__details">
                                            <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                            </div>
                                            <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </section>

                        <section class="right-side noMr">
                            @foreach($data['turkey'] as $key=>$record)
                                @if($key == 1 || $key == 2)

                                    <div class="news-card news-card--card"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['main_img']}}"/>
                                            @if(!empty($record['video']))
                                                <span class="icon news-card__video">
                                                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                      <g>
                                                        <g>
                                                                <!-- video -->
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                                        </g>
                                                      </g>
                                                    </svg>
                                                </span>
                                            @elseif(!empty($record['album']))
                                            <!-- album -->
                                                <span class="icon news-card__video">
                                                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                      <g>
                                                        <g>
                                                                <!-- album -->
                                                    <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                                        </g>
                                                      </g>
                                                    </svg>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="news-card__caption">
                                            <div class="news-card__details">
                                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                                </div>
                                                <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </section>
                    </div>
                    <aside class="left-side">
                        @foreach($data['turkey'] as $key=>$record)
                            @php if($key < 3) continue; @endphp
                            <div class="news-card news-card--card news-card--switch"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                <div class="news-card__img"><img src="{{env('APP_URL')}}/images/150x150/{{$record['main_img']}}"/>
                                </div>
                                <div class="news-card__caption">
                                    <div class="news-card__details">
                                        <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                        </div>
                                        <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </aside>
                </div>
            @endif
        </section>
        <!-- End turkey -->


        <!-- nestology -->
        <section class="mt20">
            @if(count($data['nestology']) > 0)
                <div class="main-title">
                    <div class="main-title__box">
                        <h1>زمان</h1>
                    </div>
                </div>
                <div class="layout-wrap">
                    <div class="section-wrapper section-wrapper--reverse">
                        <section class="right-side">
                            @foreach($data['nestology'] as $key=>$record)
                                @if($key == 1 || $key == 2)

                                    <div class="news-card news-card--card"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['main_img']}}"/>
                                            @if(!empty($record['video']))
                                                <span class="icon news-card__video">
                                                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                      <g>
                                                        <g>
                                                                <!-- video -->
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                                        </g>
                                                      </g>
                                                    </svg>
                                                </span>
                                            @elseif(!empty($record['album']))
                                            <!-- album -->
                                                <span class="icon news-card__video">
                                                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                      <g>
                                                        <g>
                                                                <!-- album -->
                                                    <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                                        </g>
                                                      </g>
                                                    </svg>
                                                </span>
                                            @endif

                                        </div>
                                        <div class="news-card__caption">
                                            <div class="news-card__details">
                                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                                </div>
                                                <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </section>
                        <section class="main-section">
                            @foreach($data['nestology'] as $key=>$record)
                                @php if($key > 0) continue; @endphp
                                <div class="news-card news-card--over news-card--large"><a class="news-card__link" href="{{$record['url']}}"></a>
                                    <div class="news-card__img"><img src="{{env('APP_URL')}}/images/520x600/{{$record['main_img']}}"/>
                                    </div>
                                    <div class="news-card__caption">
                                        <div class="news-card__details">
                                            <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                            </div>
                                            <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </section>
                    </div>
                    <aside class="left-side">
                        @foreach($data['nestology'] as $key=>$record)
                            @php if($key < 3) continue; @endphp
                            <div class="news-card news-card--card news-card--switch"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                <div class="news-card__img"><img src="{{env('APP_URL')}}/images/150x150/{{$record['main_img']}}"/>
                                </div>
                                <div class="news-card__caption">
                                    <div class="news-card__details">
                                        <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                        </div>
                                        <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </aside>
                </div>
            @endif
        </section>
        <!-- End nestology -->



        <!-- mix -->
        <section class="mt20">
            @if(count($data['mix']) > 0)
                <div class="main-title">
                    <div class="main-title__box">
                        <h1>منوعات</h1>
                    </div>
                </div>
                <div class="layout-wrap">
                    <div class="section-wrapper section-wrapper--reverse">
                        <section class="main-section ml15">
                            @foreach($data['mix'] as $key=>$record)
                                @php if($key > 0) continue; @endphp
                                <div class="news-card news-card--over news-card--large"><a class="news-card__link" href="{{$record['url']}}"></a>
                                    <div class="news-card__img"><img src="{{env('APP_URL')}}/images/520x600/{{$record['main_img']}}"/>
                                    </div>
                                    <div class="news-card__caption">
                                        <div class="news-card__details">
                                            <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                            </div>
                                            <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </section>

                        <section class="right-side noMr">
                            @foreach($data['mix'] as $key=>$record)
                                @if($key == 1 || $key == 2)

                                    <div class="news-card news-card--card"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['main_img']}}"/>
                                            @if(!empty($record['video']))
                                                <span class="icon news-card__video">
                                                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                      <g>
                                                        <g>
                                                                <!-- video -->
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                                        </g>
                                                      </g>
                                                    </svg>
                                                </span>
                                            @elseif(!empty($record['album']))
                                            <!-- album -->
                                                <span class="icon news-card__video">
                                                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                      <g>
                                                        <g>
                                                                <!-- album -->
                                                    <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                                        </g>
                                                      </g>
                                                    </svg>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="news-card__caption">
                                            <div class="news-card__details">
                                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                                </div>
                                                <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </section>
                    </div>
                    <aside class="left-side">
                        <div class="ads ads--square">
                            <?php include(public_path().'/ad_file/5e1b2db629cc0f264e1fcf72.php')?>
                        </div>
                        @foreach($data['mix'] as $key=>$record)
                            @php if($key < 3) continue; @endphp
                            <div class="news-card news-card--card news-card--switch"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                <div class="news-card__img"><img src="{{env('APP_URL')}}/images/150x150/{{$record['main_img']}}"/>
                                </div>
                                <div class="news-card__caption">
                                    <div class="news-card__details">
                                        <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                        </div>
                                        <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </aside>
                </div>
            @endif
        </section>
        <!-- End mix -->



        <!-- sport -->
        <section class="mt20">
            @if(count($data['sport']) > 0)
                <div class="main-title">
                    <div class="main-title__box">
                        <h1>رياضة</h1>
                    </div>
                </div>
                <div class="layout-wrap">
                    <div class="section-wrapper section-wrapper--reverse">
                        <section class="right-side">
                            @foreach($data['sport'] as $key=>$record)
                                @if($key == 1 || $key == 2)

                                    <div class="news-card news-card--card"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['main_img']}}"/>
                                            @if(!empty($record['video']))
                                                <span class="icon news-card__video">
                                                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                      <g>
                                                        <g>
                                                                <!-- video -->
                                                                <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                                        </g>
                                                      </g>
                                                    </svg>
                                                </span>
                                            @elseif(!empty($record['album']))
                                            <!-- album -->
                                                <span class="icon news-card__video">
                                                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                      <g>
                                                        <g>
                                                                <!-- album -->
                                                    <path d="M256,0C114.833,0,0,114.844,0,256s114.833,256,256,256s256-114.844,256-256S397.167,0,256,0z M357.771,264.969    l-149.333,96c-1.75,1.135-3.771,1.698-5.771,1.698c-1.75,0-3.521-0.438-5.104-1.302C194.125,359.49,192,355.906,192,352V160    c0-3.906,2.125-7.49,5.563-9.365c3.375-1.854,7.604-1.74,10.875,0.396l149.333,96c3.042,1.958,4.896,5.344,4.896,8.969    S360.813,263.01,357.771,264.969z"></path>
                                                        </g>
                                                      </g>
                                                    </svg>
                                                </span>
                                            @endif

                                        </div>
                                        <div class="news-card__caption">
                                            <div class="news-card__details">
                                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                                </div>
                                                <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </section>
                        <section class="main-section">
                            @foreach($data['sport'] as $key=>$record)
                                @php if($key > 0) continue; @endphp
                                <div class="news-card news-card--over news-card--large"><a class="news-card__link" href="{{$record['url']}}"></a>
                                    <div class="news-card__img"><img src="{{env('APP_URL')}}/images/520x600/{{$record['main_img']}}"/>
                                    </div>
                                    <div class="news-card__caption">
                                        <div class="news-card__details">
                                            <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                            </div>
                                            <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </section>
                    </div>
                    <aside class="left-side">
                        @foreach($data['sport'] as $key=>$record)
                            @php if($key < 3) continue; @endphp
                            <div class="news-card news-card--card news-card--switch"><a class="news-card__link" href="/{{$record['url']}}"></a>
                                <div class="news-card__img"><img src="{{env('APP_URL')}}/images/150x150/{{$record['main_img']}}"/>
                                </div>
                                <div class="news-card__caption">
                                    <div class="news-card__details">
                                        <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                        </div>
                                        <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="ads ads--square">
                            <?php include(public_path().'/ad_file/5e1b2dc229cc0f069a3b3872.php')?>
                        </div>

                    </aside>
                </div>
            @endif
        </section>
        <!-- End sport -->




    </div>



@endsection
