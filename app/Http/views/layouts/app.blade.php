<!DOCTYPE html>
<html dir="rtl" lang="ar-EG">
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no,IE=edge,chrome=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <title>@yield('title')</title>
    <link rel="canonical" href="@yield('url')" />
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    @php
        $keywords = $__env->yieldContent('keywords');
        $keyword = explode(",", $keywords);
    foreach ($keyword as $key){
    echo '<meta property="article:tag" content="'.$key.'" />';
    }
    @endphp
    <meta property="article:section" content="@yield('section')" />
    <meta property="article:published_time" content="@yield('published_time')" />
    <meta property="article:modified_time" content="@yield('modified_time')" />
    <meta property="article:author" content="@yield('author')" />
    <meta name="robots" content="@yield('robots')">
    <meta name="googlebot" content="@yield('googlebot')" />
    <meta property="og:url" content="@yield('url')" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('description')" />
    <meta property="og:image" content="@yield('image')" />
    <meta property="fb:app_id" content="538318339942983" />
    <meta name='twitter:app:country' content='EG'>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@@@@" />
    <meta name="twitter:creator" content="@@@@@" />
    <meta name="twitter:title" content="@yield('title')" />
    <meta name="twitter:url" content="@yield('url')" />
    <meta name="twitter:description" id="TwitterDesc" content="@yield('description')" />
    <meta name="twitter:image" id="TwitterImg" content="@yield('image')" />
    <meta property="fb:pages" content="###" />
    <link rel="manifest" href="/manifest.json">
    <link rel="amphtml" href="@yield('amp')" />
    {{--    @if($__env->yieldContent('style') != null)--}}
{{--     @yield('style')--}}
{{--    @else--}}
        <link href="/assets/css/style.css?<?=time()?>" rel="stylesheet" />
{{--    @endif--}}
{{--    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
        var _wavepush = _wavepush || [];
        _wavepush.push(['_setDomain', 'gololy.com']);
        (function () {
            var wavescript = document.createElement('script');
            wavescript.src = '//push.mediapanarab.com/sdk/MediaPan_SDK.js?1587546184';
            wavescript.type = 'text/javascript';
            wavescript.async = 'true';
            var wave_s = document.getElementsByTagName('script')[0];
            wave_s.parentNode.insertBefore(wavescript, wave_s);
        })();
    </script>
    @if ($__env->yieldContent('ads') == 'on')
    <!-- Start GPT Tag -->
        <script async src='https://securepubads.g.doubleclick.net/tag/js/gpt.js'></script>
        <script>
            window.googletag = window.googletag || {cmd: []};
            googletag.cmd.push(function() {
                googletag.defineSlot('/107479270/gololy_leaderboared', [[728,90],[970,90],[970,250]], 'div-gpt-ad-1100139-1')
                    .addService(googletag.pubads());
                googletag.defineSlot('/107479270/gololy_Medium_rec', [[300,250]], 'div-gpt-ad-1100139-2')
                    .addService(googletag.pubads());
                googletag.defineSlot('/107479270/Gololy_Medium_rec2', [[300,250]], 'div-gpt-ad-1100139-3')
                    .addService(googletag.pubads());
                googletag.defineSlot('/107479270/Gololy_Medium_rec3', [[300,250]], 'div-gpt-ad-1100139-4')
                    .addService(googletag.pubads());
                googletag.defineSlot('/107479270/Gololy_sponsor_left', [[120,600]], 'div-gpt-ad-7653119-1')
                    .addService(googletag.pubads());
                googletag.defineSlot('/107479270/Gololy_Sponsor_right', [[120,600]], 'div-gpt-ad-7653119-2')
                    .addService(googletag.pubads());

                <!-- Mobile -->
                googletag.defineSlot('/107479270/Gololy_Large_Mobile_banner', [[320,50],[320,100]], 'div-gpt-ad-9119753-1')
                    .addService(googletag.pubads());
                googletag.defineSlot('/107479270/Gololy_Mobile_Medium_rec', [[336,280],[300,250]], 'div-gpt-ad-9119753-2')
                    .addService(googletag.pubads());
                googletag.defineSlot('/107479270/Gololy_Mobile_Medium_rec2', [[336,280],[300,250]], 'div-gpt-ad-9119753-3')
                    .addService(googletag.pubads());
                googletag.defineSlot('/107479270/Gololy_Mobile_Medium_rec3', [[336,280],[300,250]], 'div-gpt-ad-9119753-4')
                    .addService(googletag.pubads());
                googletag.defineSlot('/107479270/Gololy_Medium_rec4', [[300,250],[336,280]], 'div-gpt-ad-9119753-5')
                    .addService(googletag.pubads());
                <!-- End-Mobile -->


                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
        </script>
        <!-- End GPT Tag -->





    @endif

</head>

<body>
<div class="notify" id="notify">
    <div class="notification-card">
        <div class="notification-card__close hide-notification" onclick="hidereqpermission()">
            <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 455.111 455.111" style="enable-background:new 0 0 455.111 455.111;" xml:space="preserve">
            <circle style="fill:#E24C4B;" cx="227.556" cy="227.556" r="227.556"></circle>
                <path style="fill:#D1403F;" d="M455.111,227.556c0,125.156-102.4,227.556-227.556,227.556c-72.533,0-136.533-32.711-177.778-85.333  c38.4,31.289,88.178,49.778,142.222,49.778c125.156,0,227.556-102.4,227.556-227.556c0-54.044-18.489-103.822-49.778-142.222  C422.4,91.022,455.111,155.022,455.111,227.556z"></path>
                <path style="fill:#FFFFFF;" d="M331.378,331.378c-8.533,8.533-22.756,8.533-31.289,0l-72.533-72.533l-72.533,72.533  c-8.533,8.533-22.756,8.533-31.289,0c-8.533-8.533-8.533-22.756,0-31.289l72.533-72.533l-72.533-72.533  c-8.533-8.533-8.533-22.756,0-31.289c8.533-8.533,22.756-8.533,31.289,0l72.533,72.533l72.533-72.533  c8.533-8.533,22.756-8.533,31.289,0c8.533,8.533,8.533,22.756,0,31.289l-72.533,72.533l72.533,72.533  C339.911,308.622,339.911,322.844,331.378,331.378z"></path>
          </svg>
        </div>
        <div class="notification-card__img-holder"><img src="/assets/img/logo.png" alt=""/></div>
        <div class="notification-card__icon">
            <svg xmlns="http://www.w3.org/2000/svg" height="511pt" viewBox="-21 1 511 511.99998" width="511pt">
                <path d="m311.628906 435.773438c0 35.046874-23.644531 64.554687-55.839844 73.46875-6.488281 1.796874-13.320312 2.757812-20.375 2.757812-42.097656 0-76.226562-34.125-76.226562-76.226562l76.347656-39.234376zm0 0" fill="#e58e13"></path>
                <path d="m307.941406 459.285156c-7.847656 24.21875-27.492187 43.132813-52.152344 49.957032-15.503906-4.289063-29.023437-13.351563-38.875-25.503907-7.941406-9.800781-.777343-24.453125 11.835938-24.453125zm0 0" fill="#f7d360"></path>
                <path d="m432.210938 359.558594-163.761719 35.414062-229.84375-35.414062c18.035156 0 28.234375-9.433594 34.023437-25.078125 28.003906-75.523438-46.582031-295.632813 162.785156-295.632813 209.367188 0 134.769532 220.109375 162.773438 295.632813 5.800781 15.644531 15.996094 25.078125 34.023438 25.078125zm0 0" fill="#f7d360"></path>
                <path d="m470.316406 397.667969c0 21.042969-17.0625 38.105469-38.105468 38.105469h-393.605469c-10.519531 0-20.050781-4.261719-26.945313-11.160157-6.898437-6.894531-11.160156-16.425781-11.160156-26.945312 0-21.046875 17.0625-38.109375 38.105469-38.109375h393.605469c10.519531 0 20.050781 4.265625 26.945312 11.160156 6.898438 6.898438 11.160156 16.425781 11.160156 26.949219zm0 0" fill="#e58e13"></path>
                <path d="m398.1875 334.480469h-204.574219c-22.054687 0-39.691406-18.253907-39.007812-40.300781 2.882812-93.265626-11.992188-253.558594 79.277343-255.320313-250.535156 1.296875-90.382812 320.699219-195.269531 320.699219h393.597657c-18.027344 0-28.222657-9.433594-34.023438-25.078125zm0 0" fill="#e58e13"></path>
                <path d="m470.316406 397.667969c0 21.042969-17.0625 38.105469-38.105468 38.105469h-283.480469c-10.523438 0-20.054688-4.261719-26.949219-11.160157-6.894531-6.894531-11.160156-16.425781-11.160156-26.945312 0-21.046875 17.0625-38.109375 38.109375-38.109375h283.480469c10.519531 0 20.050781 4.265625 26.945312 11.160156 6.898438 6.898438 11.160156 16.425781 11.160156 26.949219zm0 0" fill="#f7d360"></path>
                <path d="m274.148438 41.765625c.082031-.960937.113281-1.933594.113281-2.917969 0-21.449218-17.394531-38.847656-38.847657-38.847656-21.460937 0-38.847656 17.398438-38.847656 38.847656 0 .984375.03125 1.957032.113282 2.917969" fill="#e58e13"></path>
                <g fill="#e6e6e6">
                    <path d="m424.613281 167.71875c-4.328125 0-7.835937-3.511719-7.835937-7.835938 0-36.269531-9.324219-67.222656-27.710938-92-13.726562-18.496093-27.683594-26.457031-27.820312-26.539062-3.765625-2.113281-5.121094-6.878906-3.019532-10.652344 2.101563-3.769531 6.84375-5.136718 10.621094-3.050781.667969.371094 16.535156 9.277344 32.25 30.160156 14.304688 19.007813 31.355469 52.148438 31.355469 102.082031 0 4.324219-3.511719 7.835938-7.839844 7.835938zm0 0"></path>
                    <path d="m459.09375 122.789062c-4.328125 0-7.835938-3.507812-7.835938-7.835937 0-50.023437-29.625-69.910156-30.886718-70.730469-3.613282-2.355468-4.660156-7.195312-2.328125-10.820312 2.335937-3.625 7.140625-4.695313 10.777343-2.378906 1.558594.988281 38.109376 24.929687 38.109376 83.929687 0 4.328125-3.507813 7.835937-7.835938 7.835937zm0 0"></path>
                    <path d="m46.203125 167.71875c-4.328125 0-7.835937-3.511719-7.835937-7.835938 0-49.933593 17.050781-83.074218 31.351562-102.082031 15.71875-20.882812 31.582031-29.792969 32.25-30.160156 3.789062-2.09375 8.558594-.71875 10.652344 3.070313 2.089844 3.78125.722656 8.539062-3.050782 10.636718-.308593.175782-14.171874 8.15625-27.816406 26.535156-18.390625 24.777344-27.710937 55.730469-27.710937 92-.003907 4.324219-3.511719 7.835938-7.839844 7.835938zm0 0"></path>
                    <path d="m11.722656 122.789062c-4.328125 0-7.835937-3.507812-7.835937-7.835937 0-59 36.554687-82.941406 38.109375-83.929687 3.652344-2.324219 8.496094-1.246094 10.820312 2.402343 2.316406 3.640625 1.253906 8.46875-2.375 10.796875-1.300781.851563-30.882812 20.746094-30.882812 70.730469 0 4.328125-3.507813 7.835937-7.835938 7.835937zm0 0"></path>
                </g>
            </svg>
        </div>
        <div class="notification-card__text">
            <p>اشترك في خدمة الاشعارات لمتابعة آخر الاخبار المحلية و العالمية فور وقوعها.</p>
        </div>
        <div class="notification-card__subscribe">
            <button class="notification-card__btn1" onclick="allownotify()">اشترك الأن</button>
            <button class="notification-card__btn2 hide-notification" onclick="hidereqpermission()">لاحقاً </button>
        </div>
    </div>
</div>
@include('composer.hits')
<div class="wrapper" id="app">
    <header class="header">
        <div class="search-full-section" id="search-full-section">
            <div class="container">
                <div class="search-full-section__box">
                    <form class="search-full-section__search" action="{{env('APP_URL')}}/search" method="get">
                        <input name="keyword" class="search-full-section__searchInput" type="search" placeholder="إبحث هنا.."/>
                        <button type="submit" id="start-search"><span>
                    <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 515.558 515.558" height="512" viewBox="0 0 515.558 515.558" width="512">
                      <path d="m378.344 332.78c25.37-34.645 40.545-77.2 40.545-123.333 0-115.484-93.961-209.445-209.445-209.445s-209.444 93.961-209.444 209.445 93.961 209.445 209.445 209.445c46.133 0 88.692-15.177 123.337-40.547l137.212 137.212 45.564-45.564c0-.001-137.214-137.213-137.214-137.213zm-168.899 21.667c-79.958 0-145-65.042-145-145s65.042-145 145-145 145 65.042 145 145-65.043 145-145 145z"></path>
                    </svg></span></button>
                    </form>
{{--                    <div class="search-full-section__tags">--}}
{{--                        <h4>أكثر الكلمات إنتشارا</h4>--}}
{{--                        <div class="search-full-section__tagsBox"><a href="#">حمادة هلال </a><a href="#">حمادة هلال </a><a href="#">حمادة هلال </a><a href="#">حمادة هلال </a><a href="#">حمادة هلال </a><a href="#">حمادة هلال </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <div class="header__body">
            <div class="container-fluid flex">
                <div class="header__burger"><span class="one"></span><span class="two"></span><span class="three"></span></div>
                <div class="header__logo"><a href="{{env('APP_URL')}}" title="{{env('APP_NAME')}}"><img src="/assets/images/logo.png" alt="{{env('APP_NAME')}}"/></a></div>
                <nav class="header__nav">
                    <ul class="header__main-list">
                        @include('composer.menu')
                    </ul>
                </nav>
                <div class="header__corner">
                    <a class="header__cornerBtn instagram" href="#">
                        <svg class="u-icon undefined">
                            <use xlink:href="/assets/img/icons.svg#icon-Instagram"></use>
                        </svg>
                    </a>
                    <a class="header__cornerBtn twitter" href="https://twitter.com/gololygololy1">
                        <svg class="u-icon undefined">
                            <use xlink:href="/assets/img/icons.svg#icon-twitter"></use>
                        </svg>
                    </a>
                    <a class="header__cornerBtn facebook" href="https://www.facebook.com/gololy">
                        <svg class="u-icon undefined">
                            <use xlink:href="/assets/img/icons.svg#icon-facebook"></use>
                        </svg>
                    </a>
                    <a class="header__cornerBtn notification" href="#" onclick="allownotify()">
                        <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 512 512" height="512" viewBox="0 0 512 512" width="512">
                            <g>
                                <path d="m256 512c30.692 0 57.122-18.539 68.719-45h-137.438c11.597 26.461 38.028 45 68.719 45z"></path>
                                <path d="m411 247.862v-32.862c0-69.822-46.411-129.001-110-148.33v-21.67c0-24.813-20.187-45-45-45s-45 20.187-45 45v21.67c-63.59 19.329-110 78.507-110 148.33v32.862c0 61.332-23.378 119.488-65.827 163.756-4.16 4.338-5.329 10.739-2.971 16.267s7.788 9.115 13.798 9.115h420c6.01 0 11.439-3.587 13.797-9.115s1.189-11.929-2.97-16.267c-42.449-44.268-65.827-102.425-65.827-163.756zm-140-187.134c-4.937-.476-9.94-.728-15-.728s-10.063.252-15 .728v-15.728c0-8.271 6.729-15 15-15s15 6.729 15 15z"></path>
                                <path d="m451 215c0 8.284 6.716 15 15 15s15-6.716 15-15c0-60.1-23.404-116.603-65.901-159.1-5.857-5.857-15.355-5.858-21.213 0s-5.858 15.355 0 21.213c36.831 36.831 57.114 85.8 57.114 137.887z"></path>
                                <path d="m46 230c8.284 0 15-6.716 15-15 0-52.086 20.284-101.055 57.114-137.886 5.858-5.858 5.858-15.355 0-21.213-5.857-5.858-15.355-5.858-21.213 0-42.497 42.497-65.901 98.999-65.901 159.099 0 8.284 6.716 15 15 15z"></path>
                            </g>
                        </svg>
                    </a>
                    <span class="header__cornerBtn search"><span class="open active">
                  <svg class="u-icon undefined">
                    <use xlink:href="/assets/img/icons.svg#icon-search"></use>
                  </svg></span><span class="close">
                  <svg class="u-icon undefined">
                    <use xlink:href="/assets/img/icons.svg#icon-close"></use>
                  </svg></span></span></div>
            </div>
        </div>
    </header>
    <div class="side-banner">
        <div class="side-banner__left">
            <!-- GPT AdSlot 1 for Ad unit 'Gololy_sponsor_left' ### Size: [[120,600]] -->
            <div id='div-gpt-ad-7653119-1'>
                <script>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-7653119-1'); });
                </script>
            </div>
            <!-- End AdSlot 1 -->
        </div>
        <div class="side-banner__right">
            <!-- GPT AdSlot 2 for Ad unit 'Gololy_Sponsor_right' ### Size: [[120,600]] -->
            <div id='div-gpt-ad-7653119-2'>
                <script>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-7653119-2'); });
                </script>
            </div>
            <!-- End AdSlot 2 -->
        </div>
    </div>





{{--<body class="home">--}}
{{--<header class='site-header navigation-hidden'>--}}
{{--    <div class="header-wrap headerbg-center" style='background-image:none'>--}}
{{--        <div class="overlay"></div>--}}
{{--        <div class="part first">--}}
{{--            <div class="container-fluid full-w">--}}
{{--                <div class="logo-div">--}}
{{--                    <div class="website-logo">--}}
{{--                        <a href="{{env('APP_URL')}}" title="{{env('APP_NAME')}}">--}}
{{--                            <img src="/assets/images/logo.png" alt="{{env('APP_NAME')}}" style="width: 70%">--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <h1 class="hidden">{{env('APP_NAME')}}</h1>--}}
{{--                </div>--}}
{{--                <div id="menu">--}}
{{--                    @include('composer.menu')--}}
{{--                </div>--}}
{{--                <div class="top-sm">--}}
{{--                    <ul>--}}
{{--                        <li class="top-sm-h">--}}
{{--                            <a href="https://www.facebook.com/####/" target="_blank" rel="nofollow" title="facebook">--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-1" class="at-icon at-icon-facebook" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;">--}}
{{--                                    <g>--}}
{{--                                        <path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path>--}}
{{--                                    </g>--}}
{{--                                </svg>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="top-sm-h">--}}
{{--                            <a href="https://twitter.com/####" target="_blank" rel="nofollow" title="twitter">--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-2" class="at-icon at-icon-twitter" style="fill: rgb(255, 255, 255); width: 32px; height: 32px;">--}}
{{--                                    <g>--}}
{{--                                        <path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path>--}}
{{--                                    </g>--}}
{{--                                </svg>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="top-sm-h">--}}
{{--                            <a href="https://www.youtube.com/channel/#####/videos" target="_blank" rel="nofollow" title="youtube">--}}
{{--                                <svg style="fill:rgb(255, 255, 255);width: 22px;margin: 7px;" viewBox="-21 -117 682.66672 682"  xmlns="http://www.w3.org/2000/svg">--}}
{{--                                    <path d="m626.8125 64.035156c-7.375-27.417968-28.992188-49.03125-56.40625-56.414062-50.082031-13.703125-250.414062-13.703125-250.414062-13.703125s-200.324219 0-250.40625 13.183593c-26.886719 7.375-49.03125 29.519532-56.40625 56.933594-13.179688 50.078125-13.179688 153.933594-13.179688 153.933594s0 104.378906 13.179688 153.933594c7.382812 27.414062 28.992187 49.027344 56.410156 56.410156 50.605468 13.707031 250.410156 13.707031 250.410156 13.707031s200.324219 0 250.40625-13.183593c27.417969-7.378907 49.03125-28.992188 56.414062-56.40625 13.175782-50.082032 13.175782-153.933594 13.175782-153.933594s.527344-104.382813-13.183594-154.460938zm-370.601562 249.878906v-191.890624l166.585937 95.945312zm0 0"/>--}}
{{--                                </svg>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="top-sm-h">--}}
{{--                            <a href="https://www.instagram.com/#####" target="_blank" rel="nofollow" title="Instagram">--}}
{{--                                <svg viewBox="0 0 512.00096 512.00096" xmlns="http://www.w3.org/2000/svg"  style="fill:rgb(255, 255, 255);width: 22px;margin: 7px;">--}}
{{--                                    <path d="m373.40625 0h-234.8125c-76.421875 0-138.59375 62.171875-138.59375 138.59375v234.816406c0 76.417969 62.171875 138.589844 138.59375 138.589844h234.816406c76.417969 0 138.589844-62.171875 138.589844-138.589844v-234.816406c0-76.421875-62.171875-138.59375-138.59375-138.59375zm108.578125 373.410156c0 59.867188-48.707031 108.574219-108.578125 108.574219h-234.8125c-59.871094 0-108.578125-48.707031-108.578125-108.574219v-234.816406c0-59.871094 48.707031-108.578125 108.578125-108.578125h234.816406c59.867188 0 108.574219 48.707031 108.574219 108.578125zm0 0"/>--}}
{{--                                    <path d="m256 116.003906c-77.195312 0-139.996094 62.800782-139.996094 139.996094s62.800782 139.996094 139.996094 139.996094 139.996094-62.800782 139.996094-139.996094-62.800782-139.996094-139.996094-139.996094zm0 249.976563c-60.640625 0-109.980469-49.335938-109.980469-109.980469 0-60.640625 49.339844-109.980469 109.980469-109.980469 60.644531 0 109.980469 49.339844 109.980469 109.980469 0 60.644531-49.335938 109.980469-109.980469 109.980469zm0 0"/>--}}
{{--                                    <path d="m399.34375 66.285156c-22.8125 0-41.367188 18.558594-41.367188 41.367188 0 22.8125 18.554688 41.371094 41.367188 41.371094s41.371094-18.558594 41.371094-41.371094-18.558594-41.367188-41.371094-41.367188zm0 52.71875c-6.257812 0-11.351562-5.09375-11.351562-11.351562 0-6.261719 5.09375-11.351563 11.351562-11.351563 6.261719 0 11.355469 5.089844 11.355469 11.351563 0 6.257812-5.09375 11.351562-11.355469 11.351562zm0 0"/>--}}
{{--                                </svg>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="top-sm-h search-">--}}
{{--                            <a href="#" title="search">--}}
{{--                                    <svg version="1.1" id="Capa_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"--}}
{{--                                         viewBox="0 0 511.999 511.999" style="fill:rgb(255, 255, 255);width: 22px;margin: 7px;;" xml:space="preserve">--}}
{{--                                    <g>--}}
{{--                                        <path d="M508.874,478.708L360.142,329.976c28.21-34.827,45.191-79.103,45.191-127.309c0-111.75-90.917-202.667-202.667-202.667--}}
{{--			S0,90.917,0,202.667s90.917,202.667,202.667,202.667c48.206,0,92.482-16.982,127.309-45.191l148.732,148.732--}}
{{--			c4.167,4.165,10.919,4.165,15.086,0l15.081-15.082C513.04,489.627,513.04,482.873,508.874,478.708z M202.667,362.667--}}
{{--			c-88.229,0-160-71.771-160-160s71.771-160,160-160s160,71.771,160,160S290.896,362.667,202.667,362.667z"/>--}}
{{--                                    </g>--}}
{{--                                </svg>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}


{{--        <div class="second">--}}
{{--            <div class="wrap container-fluid row">--}}
{{--                <div class="col-xs-12 col-sm-12"></div>--}}
{{--                <div class="col-sm-6 col-lg-9 col-md-9 ">--}}
{{--                    <ul class="wrap-nav" style="padding-right: 0px;">--}}
{{--                        @include('composer.navigation')--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-lg-3 col-md-3 ">--}}
{{--                    <ul class="wrap-nav" style="padding-right: 0px;">--}}
{{--                            <li style="width: 100%"><a href="{{env('APP_URL')}}/#">من نحن</a></li>--}}
{{--                            <li style="width: 100%"><a href="{{env('APP_URL')}}/#">اتصل بنا</a></li>--}}
{{--                            <li style="width: 100%"><a href="{{env('APP_URL')}}/#">السياسة التحريرية</a></li>--}}
{{--                    </ul>--}}
{{--                    <div class="header-social" style="margin-right: 30%;">--}}
{{--                        <ul>--}}
{{--                            <li >--}}
{{--                                <a href="https://www.facebook.com/ahlmasrnewsofficial/" target="_blank" rel="nofollow" title="facebook" >--}}
{{--                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-1" class="at-icon at-icon-facebook" style="fill:rgb(255, 255, 255);width: 22px;margin: 7px;">--}}
{{--                                        <g>--}}
{{--                                            <path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path>--}}
{{--                                        </g>--}}
{{--                                    </svg>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="https://twitter.com/Ahlmasr2020" target="_blank" rel="nofollow" title="twitter">--}}
{{--                                    <svg style="fill:rgb(255, 255, 255);width: 22px;margin: 7px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-2" class="at-icon at-icon-twitter">--}}
{{--                                        <g>--}}
{{--                                            <path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path>--}}
{{--                                        </g>--}}
{{--                                    </svg>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="https://www.youtube.com/channel/UC6BWSIG6NJhQjc8kLg1c3Jg/videos" target="_blank" rel="nofollow" title="youtube">--}}
{{--                                    <svg style="fill:rgb(255, 255, 255);width: 22px;margin: 7px;" viewBox="-21 -117 682.66672 682"  xmlns="http://www.w3.org/2000/svg">--}}
{{--                                        <path d="m626.8125 64.035156c-7.375-27.417968-28.992188-49.03125-56.40625-56.414062-50.082031-13.703125-250.414062-13.703125-250.414062-13.703125s-200.324219 0-250.40625 13.183593c-26.886719 7.375-49.03125 29.519532-56.40625 56.933594-13.179688 50.078125-13.179688 153.933594-13.179688 153.933594s0 104.378906 13.179688 153.933594c7.382812 27.414062 28.992187 49.027344 56.410156 56.410156 50.605468 13.707031 250.410156 13.707031 250.410156 13.707031s200.324219 0 250.40625-13.183593c27.417969-7.378907 49.03125-28.992188 56.414062-56.40625 13.175782-50.082032 13.175782-153.933594 13.175782-153.933594s.527344-104.382813-13.183594-154.460938zm-370.601562 249.878906v-191.890624l166.585937 95.945312zm0 0"/>--}}
{{--                                    </svg>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="https://www.instagram.com/#" target="_blank" rel="nofollow" title="Instagram">--}}
{{--                                    <svg viewBox="0 0 512.00096 512.00096" xmlns="http://www.w3.org/2000/svg"  style="fill:rgb(255, 255, 255);width: 22px;margin: 7px;">--}}
{{--                                        <path d="m373.40625 0h-234.8125c-76.421875 0-138.59375 62.171875-138.59375 138.59375v234.816406c0 76.417969 62.171875 138.589844 138.59375 138.589844h234.816406c76.417969 0 138.589844-62.171875 138.589844-138.589844v-234.816406c0-76.421875-62.171875-138.59375-138.59375-138.59375zm108.578125 373.410156c0 59.867188-48.707031 108.574219-108.578125 108.574219h-234.8125c-59.871094 0-108.578125-48.707031-108.578125-108.574219v-234.816406c0-59.871094 48.707031-108.578125 108.578125-108.578125h234.816406c59.867188 0 108.574219 48.707031 108.574219 108.578125zm0 0"/>--}}
{{--                                        <path d="m256 116.003906c-77.195312 0-139.996094 62.800782-139.996094 139.996094s62.800782 139.996094 139.996094 139.996094 139.996094-62.800782 139.996094-139.996094-62.800782-139.996094-139.996094-139.996094zm0 249.976563c-60.640625 0-109.980469-49.335938-109.980469-109.980469 0-60.640625 49.339844-109.980469 109.980469-109.980469 60.644531 0 109.980469 49.339844 109.980469 109.980469 0 60.644531-49.335938 109.980469-109.980469 109.980469zm0 0"/>--}}
{{--                                        <path d="m399.34375 66.285156c-22.8125 0-41.367188 18.558594-41.367188 41.367188 0 22.8125 18.554688 41.371094 41.367188 41.371094s41.371094-18.558594 41.371094-41.371094-18.558594-41.367188-41.371094-41.367188zm0 52.71875c-6.257812 0-11.351562-5.09375-11.351562-11.351562 0-6.261719 5.09375-11.351563 11.351562-11.351563 6.261719 0 11.355469 5.089844 11.355469 11.351563 0 6.257812-5.09375 11.351562-11.355469 11.351562zm0 0"/>--}}
{{--                                    </svg>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="searchWrap" style="display: none">--}}
{{--            <div class="search-close">--}}
{{--                <a href="#" class="search-close-btn">--}}
{{--                    <svg style="fill:rgb(255, 255, 255);width: 22px;" id="Capa_3" enable-background="new 0 0 413.348 413.348" viewBox="0 0 413.348 413.348" xmlns="http://www.w3.org/2000/svg"><path d="m413.348 24.354-24.354-24.354-182.32 182.32-182.32-182.32-24.354 24.354 182.32 182.32-182.32 182.32 24.354 24.354 182.32-182.32 182.32 182.32 24.354-24.354-182.32-182.32z"/></svg>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="header-search" itemscope="" itemtype="http://schema.org/WebSite">--}}
{{--                <meta itemprop="url" content="{{env('APP_URL')}}">--}}
{{--                <form action="{{env('APP_URL')}}/search/" method="get" itemprop="potentialAction" itemscope="" itemtype="http://schema.org/SearchAction" style="width: 100%;direction: rtl;">--}}
{{--                    <meta itemprop="target" content="{{env('APP_URL')}}/search/search?keyword={keyword}">--}}
{{--                    <div style="width: 100%;height: 50px">--}}
{{--                        <input type="text" class="searchTerm" name="keyword" placeholder="ابحث فى الموقع" itemprop="query-input" />--}}
{{--                        <button type="submit" class="searchButton">--}}
{{--                            <svg version="1.1" id="Capa_4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.999 511.999" style="fill:rgb(255, 255, 255);width: 22px;margin: 7px;;" xml:space="preserve">--}}
{{--<g>--}}
{{--    <path d="M508.874,478.708L360.142,329.976c28.21-34.827,45.191-79.103,45.191-127.309c0-111.75-90.917-202.667-202.667-202.667--}}
{{--			S0,90.917,0,202.667s90.917,202.667,202.667,202.667c48.206,0,92.482-16.982,127.309-45.191l148.732,148.732--}}
{{--			c4.167,4.165,10.919,4.165,15.086,0l15.081-15.082C513.04,489.627,513.04,482.873,508.874,478.708z M202.667,362.667--}}
{{--			c-88.229,0-160-71.771-160-160s71.771-160,160-160s160,71.771,160,160S290.896,362.667,202.667,362.667z"></path> </g>--}}
{{--</svg>--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--            @include('composer.search')--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</header>--}}
{{--<div id="right_ads">--}}

{{--    <div id='div-gpt-ad-1531159669082-4' style='height:600px; width:120px;'>--}}
{{--        <script>--}}
{{--            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1531159669082-4'); });--}}
{{--        </script>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<div id="left_ads">--}}

{{--    <div id='div-gpt-ad-1531159669082-3' style='height:600px; width:120px;'>--}}
{{--        <script>--}}
{{--            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1531159669082-3'); });--}}
{{--        </script>--}}
{{--    </div>--}}
{{--</div>--}}
@yield('content')

    <footer class="footer">
        <div class="container">
            <div class="footer__goup"><span class="icon">
              <svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 492 492" style="enable-background:new 0 0 492 492;" xml:space="preserve">
                <g>
                  <g></g>
                  <path d="M442.627,185.388L265.083,7.844C260.019,2.78,253.263,0,245.915,0c-7.204,0-13.956,2.78-19.02,7.844L49.347,185.388                        c-10.488,10.492-10.488,27.568,0,38.052l16.12,16.128c5.064,5.06,11.82,7.844,19.028,7.844c7.204,0,14.192-2.784,19.252-7.844                        l103.808-103.584v329.084c0,14.832,11.616,26.932,26.448,26.932h22.8c14.832,0,27.624-12.1,27.624-26.932V134.816l104.396,104.752                        c5.06,5.06,11.636,7.844,18.844,7.844s13.864-2.784,18.932-7.844l16.072-16.128C453.163,212.952,453.123,195.88,442.627,185.388z"></path>
                </g>
              </svg></span></div>
            <div class="footer__menu"><a href="about-us.html">من نحن</a></div>
            <div class="footer__copy">
                <p>© جميع الحقوق محفوظة - 2020</p>
            </div>
            <div class="footer__socials">
                <li>
                    <a href="https://www.youtube.com/user/gololygololy">
                        <span class="icon">
                  <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="90.677px" height="90.677px" viewBox="0 0 90.677 90.677" style="enable-background:new 0 0 90.677 90.677;" xml:space="preserve">
                    <g>
                      <g>
                        <path d="M82.287,45.907c-0.937-4.071-4.267-7.074-8.275-7.521c-9.489-1.06-19.098-1.065-28.66-1.06    c-9.566-0.005-19.173,0-28.665,1.06c-4.006,0.448-7.334,3.451-8.27,7.521c-1.334,5.797-1.35,12.125-1.35,18.094    c0,5.969,0,12.296,1.334,18.093c0.936,4.07,4.264,7.073,8.272,7.521c9.49,1.061,19.097,1.065,28.662,1.061    c9.566,0.005,19.171,0,28.664-1.061c4.006-0.448,7.336-3.451,8.272-7.521c1.333-5.797,1.34-12.124,1.34-18.093    C83.61,58.031,83.62,51.704,82.287,45.907z M28.9,50.4h-5.54v29.438h-5.146V50.4h-5.439v-4.822H28.9V50.4z M42.877,79.839h-4.629    v-2.785c-1.839,2.108-3.585,3.136-5.286,3.136c-1.491,0-2.517-0.604-2.98-1.897c-0.252-0.772-0.408-1.994-0.408-3.796V54.311    h4.625v18.795c0,1.084,0,1.647,0.042,1.799c0.111,0.718,0.462,1.082,1.082,1.082c0.928,0,1.898-0.715,2.924-2.166v-19.51h4.629    L42.877,79.839L42.877,79.839z M60.45,72.177c0,2.361-0.159,4.062-0.468,5.144c-0.618,1.899-1.855,2.869-3.695,2.869    c-1.646,0-3.234-0.914-4.781-2.824v2.474h-4.625V45.578h4.625v11.189c1.494-1.839,3.08-2.769,4.781-2.769    c1.84,0,3.078,0.969,3.695,2.88c0.311,1.027,0.468,2.715,0.468,5.132V72.177z M77.907,67.918h-9.251v4.525    c0,2.363,0.773,3.543,2.363,3.543c1.139,0,1.802-0.619,2.066-1.855c0.043-0.251,0.104-1.279,0.104-3.134h4.719v0.675    c0,1.491-0.057,2.518-0.099,2.98c-0.155,1.024-0.519,1.953-1.08,2.771c-1.281,1.854-3.179,2.768-5.595,2.768    c-2.42,0-4.262-0.871-5.599-2.614c-0.981-1.278-1.485-3.29-1.485-6.003v-8.941c0-2.729,0.447-4.725,1.43-6.015    c1.336-1.747,3.177-2.617,5.54-2.617c2.321,0,4.161,0.87,5.457,2.617c0.969,1.29,1.432,3.286,1.432,6.015v5.285H77.907z"></path>
                        <path d="M70.978,58.163c-1.546,0-2.321,1.181-2.321,3.541v2.362h4.625v-2.362C73.281,59.344,72.508,58.163,70.978,58.163z"></path>
                        <path d="M53.812,58.163c-0.762,0-1.534,0.36-2.307,1.125v15.559c0.772,0.774,1.545,1.14,2.307,1.14    c1.334,0,2.012-1.14,2.012-3.445V61.646C55.824,59.344,55.146,58.163,53.812,58.163z"></path>
                        <path d="M56.396,34.973c1.705,0,3.479-1.036,5.34-3.168v2.814h4.675V8.82h-4.675v19.718c-1.036,1.464-2.018,2.188-2.953,2.188    c-0.626,0-0.994-0.37-1.096-1.095c-0.057-0.153-0.057-0.722-0.057-1.817V8.82h-4.66v20.4c0,1.822,0.156,3.055,0.414,3.836    C53.854,34.363,54.891,34.973,56.396,34.973z"></path>
                        <path d="M23.851,20.598v14.021h5.184V20.598L35.271,0h-5.242l-3.537,13.595L22.812,0h-5.455c1.093,3.209,2.23,6.434,3.323,9.646    C22.343,14.474,23.381,18.114,23.851,20.598z"></path>
                        <path d="M42.219,34.973c2.342,0,4.162-0.881,5.453-2.641c0.981-1.291,1.451-3.325,1.451-6.067v-9.034    c0-2.758-0.469-4.774-1.451-6.077c-1.291-1.765-3.11-2.646-5.453-2.646c-2.33,0-4.149,0.881-5.443,2.646    c-0.993,1.303-1.463,3.319-1.463,6.077v9.034c0,2.742,0.47,4.776,1.463,6.067C38.069,34.092,39.889,34.973,42.219,34.973z     M39.988,16.294c0-2.387,0.724-3.577,2.231-3.577c1.507,0,2.229,1.189,2.229,3.577v10.852c0,2.387-0.722,3.581-2.229,3.581    c-1.507,0-2.231-1.194-2.231-3.581V16.294z"></path>
                      </g>
                    </g>
                  </svg>
                        </span>
                    </a>
                </li>
                <li><a href=""><span class="icon">
                  <svg id="Bold" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="512" viewBox="0 0 24 24" width="512">
                    <path d="m20.779 19.685c.912-.143 2.799-.473 3.16-1.304.182-.428-.078-.776-.41-.829v-.001c-3.208-.528-4.654-3.814-4.713-3.954-.889-1.594 1.524-1.326 2.473-2.298.926-.953-.559-1.985-1.488-1.547-.381.179-.952.387-1.332.213.09-1.509.339-4.011-.272-5.377-2.055-4.603-8.115-4.769-10.931-2.097-2.219 2.104-1.947 4.267-1.739 7.474-.69.301-1.372-.297-1.869-.297-.522 0-1.129.339-1.227.85-.124.671.605 1.103 1.236 1.352 1.075.422 2.097.547 1.53 1.699-.005.009-.009.018-.012.027-.803 1.834-2.556 3.597-4.714 3.953-.348.057-.586.424-.412.829v.002c.357.832 2.262 1.165 3.161 1.305.278.52.065 1.547.926 1.547.335 0 .983-.219 1.81-.219 1.205 0 1.616.263 2.582.945.992.701 2.082 1.386 3.437 1.287 1.411.088 2.459-.561 3.487-1.285l.001-.002c1.002-.705 1.718-1.226 3.84-.809 1.57.304 1.112-.759 1.476-1.464z"></path>
                  </svg></span></a>
                </li>
                <li><a href=""><span class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" height="512pt" viewBox="-32 0 512 512" width="512pt">
                    <path d="m432.734375 112.464844c-53.742187 0-97.464844-43.722656-97.464844-97.464844 0-8.285156-6.714843-15-15-15h-80.335937c-8.28125 0-15 6.714844-15 15v329.367188c0 31.59375-25.707032 57.296874-57.300782 57.296874s-57.296874-25.703124-57.296874-57.296874c0-31.597657 25.703124-57.300782 57.296874-57.300782 8.285157 0 15-6.714844 15-15v-80.335937c0-8.28125-6.714843-15-15-15-92.433593 0-167.632812 75.203125-167.632812 167.636719 0 92.433593 75.199219 167.632812 167.632812 167.632812 92.433594 0 167.636719-75.199219 167.636719-167.632812v-145.792969c29.851563 15.917969 63.074219 24.226562 97.464844 24.226562 8.285156 0 15-6.714843 15-15v-80.335937c0-8.28125-6.714844-15-15-15zm0 0"></path>
                  </svg></span></a>
                </li>
                <li><a href="https://www.facebook.com/gololy"><span class="icon">
                  <svg aria-hidden="true" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512" data-fa-i2svg="">
                    <path d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"></path>
                  </svg></span></a>
                </li>
                <li><a href="https://twitter.com/gololygololy1"><span class="icon">
                  <svg aria-hidden="true" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                    <path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path>
                  </svg></span></a>
                </li>
                <li><a href=""><span class="icon">
                  <svg aria-hidden="true" data-prefix="fab" data-icon="instagram" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                    <path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path>
                  </svg></span></a>
                </li>
            </div>
        </div>
    </footer>
    <script src="/assets/js/bundle.js?<?=time()?>"></script>

</div>
</body>
</html>
