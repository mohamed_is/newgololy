<!DOCTYPE html>
<html ⚡>
<head>
    <meta charset="utf-8" />
    <title>{{ $content->title }}</title>
    <link rel="canonical" href="./regular-html-version.html" />
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1"/>
    <style amp-custom>
        h1 {
            color: red;
        }
    </style>
    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "NewsArticle",
        "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "{{ env('APP_URL').'/'.$content->url }}"
        },

        "headline": "{{ $content->title }}",
        "image": ["thumbnail1.jpg"],
        "datePublished": "{{ $content->publication_date}}",
        "dateModified": "{{ $content->publication_date}}",
        "author": {
        "@type": "Person",
        "name": "{{ $content->written }}"
        },
        "publisher": {
        "@type": "Organization",
        "name": "Gololy",
            "logo": {
             "@type": "ImageObject",
             "url": "{{ env('APP_URL'). '/assets/images/logo.png' }}"
            }
        }
      }
    </script>
    <script
            async
            custom-element="amp-carousel"
            src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"
    ></script>
    <script
            async
            custom-element="amp-ad"
            src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"
    ></script>
    <style amp-boilerplate>
        body {
            -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
            animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        }
        @-webkit-keyframes -amp-start {
            from {
                visibility: hidden;
            }
            to {
                visibility: visible;
            }
        }
        @-moz-keyframes -amp-start {
            from {
                visibility: hidden;
            }
            to {
                visibility: visible;
            }
        }
        @-ms-keyframes -amp-start {
            from {
                visibility: hidden;
            }
            to {
                visibility: visible;
            }
        }
        @-o-keyframes -amp-start {
            from {
                visibility: hidden;
            }
            to {
                visibility: visible;
            }
        }
        @keyframes -amp-start {
            from {
                visibility: hidden;
            }
            to {
                visibility: visible;
            }
        }
    </style>
    <noscript
    ><style amp-boilerplate>
            body {
                -webkit-animation: none;
                -moz-animation: none;
                -ms-animation: none;
                animation: none;
            }
        </style></noscript
    >
    <script async src="https://cdn.ampproject.org/v0.js"></script>
</head>
<body>
<h1>{{html_entity_decode($content->title)}}</h1>
@foreach($content->content as $key=>$record)
    <p><?=htmlspecialchars_decode($record)?></p>
@endforeach
<amp-img src="{{ env('APP_URL') . '/images/800x500' . $content->main_img }}" width="800" height="500"></amp-img>



{{--<p>--}}
{{--    Some text--}}
{{--    <amp-img src="sample.jpg" width="300" height="300"></amp-img>--}}
{{--</p>--}}
{{--<amp-ad--}}
{{--        width="300"--}}
{{--        height="250"--}}
{{--        type="a9"--}}
{{--        data-aax_size="300x250"--}}
{{--        data-aax_pubname="test123"--}}
{{--        data-aax_src="302"--}}
{{-->--}}
{{--</amp-ad>--}}
</body>
</html>
