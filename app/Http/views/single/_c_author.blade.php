@if($content->type != 'opinion')
    <div itemprop="author" itemscope itemtype="https://schema.org/Person">
    <span itemprop="name">
        @if(isset($content->written))
                @foreach($content->written as $key=>$record)
                    <a href="https://www.baladnaelyoum.com/search?q=@if(isset($record['written_title'])) {{$record['written_title']}} @endif : @if(isset($record['written_by'])) {{$record['written_by']}} @endif" title="@if(isset($record['written_by'])) {{$record['written_by']}} @endif " rel="author">@if(isset($record['written_title'])) {{$record['written_title']}} @endif : @if(isset($record['written_by'])) {{$record['written_by']}} @endif </a>
                @endforeach
            @else
                <a href="#" title="" rel="author">كتب : بلدنا اليوم </a>
            @endif
    </span>
</div>
@endif