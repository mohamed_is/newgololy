@if($content->provider == 'youtube')
    <iframe class="content-media__object" id="featured-video" width="100%" height="100%" src="https://www.youtube.com/embed/{{$content->code}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
@elseif($content->provider == 'vimeo')
    <iframe class="content-media__object" id="featured-video" src="https://player.vimeo.com/video/{{$content->code}}" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
@elseif($content->provider == 'dailymotion')
    <iframe class="content-media__object" id="featured-video" frameborder="0" width="100%" height="500" src="//www.dailymotion.com/embed/video/{{$content->code}}" allowfullscreen allow="autoplay"></iframe>
@elseif($content->provider == 'facebook')
    <?=html_entity_decode($content->code)?>
@elseif($content->provider == 'twitter')
    <?=html_entity_decode($content->code)?>
@elseif($content->provider == 'unknown')
    <?=html_entity_decode($content->code)?>
@endif