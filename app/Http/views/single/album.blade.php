@php
    $img_count = count($images);
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $url = substr($actual_link, strpos($actual_link, "?url=") + 5);
    $next_img = $images_id + 1;
    $prev_img = $images_id - 1;
    if($next_img > $img_count - 1){
    $next_img = 0;
    }
    if($prev_img < 0){
    $prev_img = $img_count - 1;
    }
    $next_url = '/album/'.$content->_id.'/'.$next_img.'?url='.$url;
    $prev_url = '/album/'.$content->_id.'/'.$prev_img.'?url='.$url;
@endphp
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <script type="text/javascript">
        $("link[rel='canonical']").attr("href", window.location.href);
    </script>
    <title>{{$content->title}}</title>


    <meta http-equiv="refresh" content="30;url={{$next_url}}" />


    <meta name="description" content="{{$content->description}}" />
    <meta name="keywords" content="" />
    <meta property="og:url" content="" />
    <link rel="canonical" href="" />
    <meta property="og:title" content="{{$content->title}}" />
    <meta property="og:description" content="{{$content->description}}" />
    <meta property="og:image" content="/images/large/{{$content['main_img']}}" />
    <style>
        body {
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
            border: 0;
            background: #000;
            overflow: hidden
        }

        div {
            border: 0;
            margin: 0;
            padding: 0
        }

        img {
            max-width: 100%;
            max-height: 100%;
            vertical-align: middle
        }

        #container {
            width: 100%;
            height: 100%;
            text-align: center;
            position: relative
        }
        #container img {
            max-height: 100%;
            max-width: 100%;
            vertical-align: middle
        }
        #container:hover #next_div {
            display: block
        }
        #container:hover #prev_div {
            display: block
        }
        #container:hover #close_btn {
            display: block
        }
        #container:hover #play_btn {
            display: block
        }
        #container:hover #full_btn {
            display: block
        }
        #close_btn {
            position: absolute;
            top: 15px;
            right: 15px;
            padding: 5px;
            display: none;
            z-index: 50
        }
        #play_btn {
            position: absolute;
            top: 15px;
            right: 55px;
            padding: 5px;
            display: none;
            z-index: 50
        }
        #full_btn {
            position: absolute;
            top: 15px;
            right: 95px;
            padding: 5px;
            display: none;
            z-index: 50
        }


        #gallery_title {
            color: #fff;
            font-family: droid arabic kufi, serif;
            line-height: 25px;
            margin-top: 20px;
            font-size: 14px;
            text-align: center
        }




        #loading_img {
            position: absolute;
            top: 41%;
            width: 100%;
            z-index: 5
        }

        #thumbs {
            margin-top: 15px
        }
        .thumbs_item {
            float: right;
            width: 60px;
            height: 60px;
            border: 1px solid #767676;
            margin: 3px;
            padding: 1px;
            border-radius: 1px;
            opacity: .6
        }
        .thumbs_item:hover {
            border: 1px solid #f93;
            opacity: 1
        }
        .thumbs_item img {
            width: 100%;
            height: 100%;
            border-radius: 3px
        }
        .thumb_active {
            border: 1px solid #24a8fe;
            opacity: 1
        }

        #next_div {
            position: absolute;
            top: 48%;
            right: 2.5%;
            width: 10%;
            height: 11%;
            display: none;
            opacity: .8;
            z-index: 50;
            transition: all .2s ease-in-out
        }
        #prev_div {
            position: absolute;
            top: 48%;
            left: 2.5%;
            width: 10%;
            height: 11%;
            display: none;
            z-index: 50;
            transition: all .2s ease-in-out
        }
        #next_div:hover,
        #prev_div:hover {
            display: block;
            transform: scale(1.1)
        }

        .active_image {
            cursor: pointer;
            display: none;
            position: relative;
            z-index: 2
        }
        .image_number {
            position: absolute;
            top: 5px;
            left: 5px;
            background: #000;
            padding: 5px;
            color: #a29da3;
            font-size: 25px;
            text-align: center;
        }


        @media(min-width:10px) and (max-width:800px) {

        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            var body_height = ($(window).height());
            //alert(body_height);
            var current_image_height = parseInt($("#active_height").val());
            //if (body_height > current_image_height) {
            var top = Math.round(body_height - current_image_height) / 2 - 100;
            if (body_height > (top + current_image_height )) {
                $(".active_image").css("padding-top", top);
            }
            // }

            $("body").keydown(function(e) {

                if (e.keyCode == 37) { // left
                    var prev = $(".prev_href").attr("href");
                    window.location = prev;
                } else if (e.keyCode == 39) { // right
                    var next = $(".next_href").attr("href");
                    window.location = next;
                }
            });

            $(".active_image").click(function() {
                var next = $(".next_href").attr("href");
                window.location = next;
            });

            $("#loading_img").hide();
            $(".active_image").fadeIn('slow');
            $(window).resize(function() {
                $(".active_image").css("padding-top", 0);
            });
        });
        $.fullscreen = IS_NATIVELY_SUPPORTED
            ? new FullScreenNative()
            : new FullScreenFallback();

        $.fn.fullscreen = function(options) {
            var elem = this[0];

            options = $.extend({
                toggleClass: null,
                overflow: 'hidden'
            }, options);
            options.styles = {
                overflow: options.overflow
            };
            delete options.overflow;

            if (elem) {
                $.fullscreen.open(elem, options);
            }

            return this;
        };

    </script>

    <script type="text/javascript">
        $(function() {
            $('.full_btn').click(function() {
                $('body').fullscreen();
                return false;
            });
            $('.full_btn').click(function() {
                $.fullscreen.exit();
                return false;
            });
        });
    </script>
</head>
<body>

<div id="container">
    <div id="loading_img">
        <img src="/assets/images/loading.gif" />
    </div>
    <div class="active_image">
        @if($content->news_id < 1000000)
            <img class="active_image" id="full_497030" alt="" src="{{$images[$images_id]['images_url']}}" />
        @else
            <img class="active_image" id="full_497030" alt="" src="/images/large/{{$images[$images_id]['images_url']}}" />
        @endif
    </div>
    <div id="thumbs">
        @foreach($images as $key=>$record)
            <div class="thumbs_item @if($key == $images_id)thumb_active @endif">
                <a href="/album/{{$content->_id}}/{{$key}}?url={{$url}}">
                    @if($content->news_id < 1000000)
                    <img alt="" src="{{$record['images_url']}}" />
                    @else
                        <img alt="" src="/images/100x60/{{$record['images_url']}}" />
                    @endif
                </a>
            </div>
        @endforeach
    </div>
    <div id="next_div">
        <a class="next_href" href="{{$next_url}}" title="التالي ">
            <img class="diss" src="/assets/images/right.png" />
        </a>
    </div>
    <div id="prev_div">
        <a class="prev_href" href="{{$prev_url}}" title="السابق">
            <img class="diss" src="/assets/images/left.png" />
        </a>
    </div>
    <div id="close_btn">
        <a href="/{{$url}}" rel="bookmark" title="">
            <img src="/assets/images/close.png" />
        </a>
    </div>
    {{--<div id="play_btn">--}}
        {{--<a href="/{{$url}}" rel="bookmark" title="">--}}
            {{--<img src="/assets/images/close.png" />--}}
        {{--</a>--}}
    {{--</div>--}}
    {{--<div id="full_btn">--}}
        {{--<a href="#" rel="bookmark" title="">--}}
            {{--<img src="/assets/images/close.png" />--}}
        {{--</a>--}}
    {{--</div>--}}
    <div class="image_number">
        {{$img_count}} : {{$images_id}}
    </div>

</div>
</body>
</html>