<div class="row">
    <div class="col-sm-12">
        <figure class="news-article-hero">
            <span class="news-article-hero-image-wrap">
                <a href="/album/{{$content->_id}}/0?url={{$content->url}}">
                    <img src="/images/750x450/{{$content['main_img']}}" title="{{$content->img_description}}">
                </a>
            </span>
        </figure>
        <div style="text-align: center;padding: 10px 10px 5px 10px;font-size: 10pt;">
            {{$content->img_description}}
        </div>
    </div>
</div>