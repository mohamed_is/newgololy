@if(isset($content['related_data']))
    @if(count($content['related_data'])>0)
    <div style="padding: 5px 20px 0px 20px;background-color:#e5e5e5;margin-bottom: 15px;">
        <div style="margin: 5px 5px 10px 5px;width: 100%;">إقرأ أيضاً </div>
        <section class="container-fluid">
                <div class="row">
                    @foreach($content['related_data'] as $key=>$record)
                        @php
                            if($key > 2)
                            continue;
                        @endphp
                        <div class="col-sm-12 col-lg-4 col-md-4 article-container sm-card">
                            <a href="/{{$record['url']}}" class="article">
                                <div class="image-container">
                                    <img class="lazyload"
                                         src="/assets/images/lazyload.jpg"
                                         data-srcset="{{env('APP_URL')}}/images/150x150/{{$record['main_img']}} 480w, {{env('APP_URL')}}/images/450x250/{{$record['main_img']}} 1024w"

                                         alt="{{$record['img_description']}}"
                                         title="{{$record['img_description']}}"/>
                                </div>
                                <div class="article-panel">
                                    <div class="news-detail-wrap">
                                        <h3>{{$record['title']}}</h3>
                                    </div>
                                </div>
                            </a>
                        </div>

                    @endforeach
                </div>
        </section>
    </div>
    @endif
@endif