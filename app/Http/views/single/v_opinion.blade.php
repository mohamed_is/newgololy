<div class="clearfix">
    <div class="pp-image">
        <img src="{{env('APP_URL')}}/images/455x256/{{$content->main_img}}" alt="{{$content->main_img_description}}" class="img-fluid">
    </div>
    <div class="pp-desc">

        <div itemprop="author" itemscope itemtype="https://schema.org/Person">
            <span itemprop="name">
                <a href="/author/{!! str_replace(" ","-",htmlentities($content->written)) !!}" title="" rel="author" style="font-size: 16pt;color: #d31820;border-bottom: 2px solid #d31820;">بقلم {{$content->written}} </a>
            </span>
        </div>
        <time itemprop="datePublished" datetime="{{$content->created_at}}">
            {{ (new App\Http\Helpers\Helpers)->date($content->created_at)}}
        </time>
        <h1 itemprop="headline">{{$content->title}}</h1>

    </div>
</div>

