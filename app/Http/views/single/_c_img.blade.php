<div class="col-sm-12 col-md-12">
    <div class="thumbnail">
        <img src="{{env('APP_URL')}}/images/large/{{$record['content']}}" alt="{{$record['title']}}" style="width: 100%; display: block;">
        <span class="dasht-feat-caption" style="margin-top: 0px;">{{$record['title']}}</span>
    </div>
</div>