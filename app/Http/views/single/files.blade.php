@section('title', $content->title)
@section('description', $content->description)
@section('keywords', $content->tags)
@section('robots', 'index, follow')
@section('googlebot', 'index, follow')
@section('url', url()->current())
@section('image', 'https://www.elrsala.com/images/1000x600/'.$content->main_img)
@extends('layouts.app')
@section('content')
    <style>
        .dasht-feat1-pop-img {
            max-height: inherit;
        }
        @media screen and (max-width: 600px) {
            .dasht-side-wrap {
                display: none;
            }
        }
    </style>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/ar_AR/sdk.js#xfbml=1&version=v3.1&appId=538318339942983&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="dasht-main-box">
        <section id="dasht-feat1-wrap" class="left relative margin-top-20">
            <div class="dasht-feat1-right-out left relative">
                @include('ads.home_responsive_1')
            </div>
        </section>
    </div>
    <div id="contentsWrapper">
        <div class="content" id="dasht-article-wrap">
            <div data-scrolly class="dasht-main-box">
                <section id="dasht-feat1-wrap" class="left relative">
                    <div class="dasht-feat1-right-out left relative">
                        <article itemscope itemtype="http://schema.org/NewsArticle">
                            <meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="{{url()->current()}}" itempublisher="الرسالة"/>
                            <div id="dasht-article-cont" class="left relative">
                                <div class="dasht-main-box">
                                    <div id="dasht-post-main" class="left relative">
                                        <div class="dasht-post-main-out left relative">
                                            <div class="dasht-post-main-in">
                                                <div id="dasht-post-content" class="left relative">
                                                    <header id="dasht-post-head" class="left relative">
                                                        <nav aria-label="breadcrumb" >
                                                            <ol class="breadcrumb">
                                                                <li class="breadcrumb-item"><a href="/">الرئيسية</a></li>
                                                                <li class="breadcrumb-item active" aria-current="page"><a href="{{ (new App\Http\Helpers\Helpers)->generate_url('section',$content['section_name']['mydasht_id'],$content['section_name']['name'],'0')}}" title="{{$content['section_name']['name']}}">{{$content['section_name']['name']}}</a></li>
                                                            </ol>
                                                        </nav>
                                                        @if($content->type != 'opinion')
                                                            <H1 class="dasht-post-title left entry-title" itemprop="headline">{{$content->title}}</H1>
                                                            <div class="dasht-author-info-wrap left relative">
                                                                <div class="dasht-author-info-text left relative">
                                                                    <div class="dasht-author-info-date left relative">
                                                                <span class="dasht-post-date updated">
                                                                    <time class="post-date updated" itemprop="datePublished" datetime="{{$content->created_at}}">
                                                                        {{ (new App\Http\Helpers\Helpers)->date($content->created_at)}}
                                                                    </time>
                                                                </span>
                                                                        <span class="dasht-post-date">{{ (new App\Http\Helpers\Helpers)->datetime($content->created_at)}}</span>
                                                                        <meta itemprop="dateModified" content="{{$content->created_at}}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif

                                                    </header>
                                                    @if($content->type == 'video')
                                                        <div id="dasht-video-embed-wrap" class="left relative">
                                                            <div id="dasht-video-embed-cont" class="left relative">
                                                                <span class="dasht-video-close fa fa-times" aria-hidden="true"></span>
                                                                <div id="dasht-video-embed" class="left relative">

                                                                    @if($content->provider == 'youtube')
                                                                        <iframe class="content-media__object" id="featured-video" width="100%" height="100%" src="https://www.youtube.com/embed/{{$content->code}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                                    @elseif($content->provider == 'vimeo')
                                                                        <iframe class="content-media__object" id="featured-video" src="https://player.vimeo.com/video/{{$content->code}}" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                                    @elseif($content->provider == 'dailymotion')
                                                                        <iframe class="content-media__object" id="featured-video" frameborder="0" width="100%" height="500" src="//www.dailymotion.com/embed/video/{{$content->code}}" allowfullscreen allow="autoplay"></iframe>
                                                                    @elseif($content->provider == 'facebook')
                                                                        <?=html_entity_decode($content->code)?>
                                                                    @elseif($content->provider == 'twitter')
                                                                        <?=html_entity_decode($content->code)?>
                                                                    @elseif($content->provider == 'unknown')
                                                                        <style>
                                                                            .instagram-media{
                                                                                min-width: 100% !important;
                                                                            }
                                                                        </style>
                                                                        <?=html_entity_decode($content->code)?>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>

                                                    @elseif($content->type == 'album')
                                                        album
                                                    @elseif($content->type == 'article')
                                                        <div id="dasht-post-feat-img" class="left relative dasht-post-feat-img-wide2" itemprop="image" itemscope itemtype="https://schema.org/ImageObject" itemid="{{env('APP_URL')}}/images/1000x600/{{$content->main_img}}">
                                                            <picture>
                                                                <source media="(max-width: 600px)" srcset="{{env('APP_URL')}}/images/560x600/{{$content->main_img}}">
                                                                <source media="(max-width: 1099px)" srcset="{{env('APP_URL')}}/images/400x240/{{$content->main_img}}">
                                                                <img src="{{env('APP_URL')}}/images/1000x600/{{$content->main_img}}" class="attachment- size- " alt="{{$content->main_img_description}}" title="{{$content->title}}" itemprop="image"/>
                                                            </picture>
                                                        </div>
                                                    @endif
                                                    @if($content->type != 'opinion')
                                                        <span class="dasht-feat-caption">{{$content->main_img_description}}</span>
                                                    @endif
                                                    @if($content->type == 'opinion')

                                                        <style>
                                                            .entry-title{
                                                                font-size: 22pt !important;
                                                                padding-top: 15px;
                                                            }


                                                            @media screen and (max-width: 600px) {
                                                                #dasht-author-box-img {
                                                                    line-height: 0;
                                                                    margin-right: 5px;
                                                                    width: 80px;
                                                                }
                                                                .dasht-author-box-in {
                                                                    margin-right: 105px;
                                                                }
                                                                .entry-title{
                                                                    font-size: 12pt !important;
                                                                    padding-top: 15px;
                                                                }
                                                                .dasht-author-box-in {
                                                                    margin-left: 5px;
                                                                }

                                                            }
                                                            @media screen and (max-width: 380px) {
                                                                #dasht-author-box-img {
                                                                    line-height: 0;
                                                                    margin-right: 5px;
                                                                    width: 80px;
                                                                }
                                                                .dasht-author-box-in {
                                                                    margin-right: 105px;
                                                                }
                                                                .entry-title{
                                                                    font-size: 12pt !important;
                                                                    padding-top: 15px;
                                                                }
                                                                .dasht-author-box-in {
                                                                    margin-left: 5px;
                                                                }

                                                            }

                                                        </style>

                                                        <div id="dasht-author-box-wrap" class="left relative">
                                                            <div class="dasht-author-box-out right relative">
                                                                <div id="dasht-author-box-img" class="left relative">
                                                                    <img src="{{env('APP_URL')}}/images/150x150/{{$content->main_img}}" class="attachment- size- " alt="{{$content->main_img_description}}" class="avatar avatar-60 photo" width="150" height="150" />
                                                                </div>
                                                                <div class="dasht-author-box-in">
                                                                    <div id="dasht-author-box-head" class="left relative">
                                                                        <span class="dasht-author-box-name left relative" style="border-bottom: 3px solid #E30613;padding-bottom: 15px;">
                                                                            <a href="#" title="{{$content->written}}" rel="author">{{$content->written}}</a>
                                                                        </span>
                                                                        <H1 class="dasht-post-title left entry-title" itemprop="headline">{{$content->title}}</H1>
                                                                        <div id="dasht-author-box-soc-wrap" class="left relative hide">
                                                                            <a href="http://www.facebook.com/" alt="Facebook" target="_blank"><span class="dasht-author-box-soc fa fa-facebook-square fa-2"></span></a>
                                                                            <a href="https://www.twitter.com/" alt="Twitter" target="_blank"><span class="dasht-author-box-soc fa fa-twitter-square fa-2"></span></a>
                                                                            <a href="http://www.pinterest.com/" alt="Pinterest" target="_blank"><span class="dasht-author-box-soc fa fa-pinterest-square fa-2"></span></a>
                                                                            <a href="https://plus.google.com/" alt="Google Plus" target="_blank"><span class="dasht-author-box-soc fa fa-google-plus-square fa-2"></span></a>
                                                                            <a href="http://www.instagram.com/" alt="Instagram" target="_blank"><span class="dasht-author-box-soc fa fa-instagram fa-2"></span></a>
                                                                            <a href="http://www.linkedin.com/" alt="LinkedIn" target="_blank"><span class="dasht-author-box-soc fa fa-linkedin-square fa-2"></span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="dasht-author-box-text" class="left relative hide">
                                                                <span class="dasht-post-date">{{ (new App\Http\Helpers\Helpers)->datetime($content->created_at)}}</span>
                                                                <meta itemprop="dateModified" content="{{$content->created_at}}" />
                                                                <p></p>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div id="dasht-content-wrap" class="left relative">
                                                        <div class="dasht-post-soc-out right relative">
                                                            <div class="dasht-post-soc-wrap left relative">
                                                                <ul class="dasht-post-soc-list left relative">
                                                                    <a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u={{url()->current()}}&amp;t={{$content->title}}', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook">
                                                                        <li class="dasht-post-soc-fb">
                                                                            <i class="fa fa-2 fa-facebook" aria-hidden="true"></i>
                                                                        </li>
                                                                    </a>
                                                                    <a href="#" onclick="window.open('http://twitter.com/share?text={{$content->title}}&amp;url={{url()->current()}}', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post">
                                                                        <li class="dasht-post-soc-twit">
                                                                            <i class="fa fa-2 fa-twitter" aria-hidden="true"></i>
                                                                        </li>
                                                                    </a>
                                                                    <a href="#" onclick="window.open('http://pinterest.com/pin/create/button/?url={{url()->current()}}&amp;media=assets/uploads/baseball2.jpg&amp;description={{$content->title}}', 'pinterestShare', 'width=750,height=350'); return false;" title="Pin This Post">
                                                                        <li class="dasht-post-soc-pin">
                                                                            <i class="fa fa-2 fa-pinterest-p" aria-hidden="true"></i>
                                                                        </li>
                                                                    </a>
                                                                    <a href="mailto:?subject={{$content->title}}&amp;BODY=لقد وجدت هذه المقالة مثيرة للاهتمام وفكرت في مشاركتها معك. تحقق من ذلك: {{url()->current()}}">
                                                                        <li class="dasht-post-soc-email">
                                                                            <i class="fa fa-2 fa-envelope" aria-hidden="true"></i>
                                                                        </li>
                                                                    </a>
                                                                    <a href="#comments">
                                                                        <li class="dasht-post-soc-com dasht-com-click">
                                                                            <i class="fa fa-2 fa-commenting" aria-hidden="true"></i>
                                                                        </li>
                                                                    </a>
                                                                </ul>
                                                            </div>
                                                            <div id="dasht-soc-mob-wrap"  class="dasht-soc-mob-up">
                                                                <div class="dasht-soc-mob-out left relative">
                                                                    <div class="dasht-soc-mob-in">
                                                                        <div class="dasht-soc-mob-left left relative">
                                                                            <ul class="dasht-soc-mob-list left relative">
                                                                                <a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u={{url()->current()}}&amp;t={{$content->title}}', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook">
                                                                                    <li class="dasht-soc-mob-fb">
                                                                                        <i class="fa fa-facebook" aria-hidden="true"></i><span class="dasht-soc-mob-fb">Share</span>
                                                                                    </li>
                                                                                </a>
                                                                                <a href="#" onclick="window.open('http://twitter.com/share?text={{$content->title}};url={{url()->current()}}', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post">
                                                                                    <li class="dasht-soc-mob-twit">
                                                                                        <i class="fa fa-twitter" aria-hidden="true"></i><span class="dasht-soc-mob-fb">Tweet</span>
                                                                                    </li>
                                                                                </a>
                                                                                <a href="whatsapp://send?text={{$content->title}} {{url()->current()}}">
                                                                                    <div class="whatsapp-share">
                                                                                     <span class="whatsapp-but1">
                                                                                         <li class="dasht-soc-mob-what">
                                                                                             <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                                                                         </li>
                                                                                     </span>
                                                                                    </div>
                                                                                </a>
                                                                                <a href="mailto:?subject={{$content->title}}&amp;BODY=لقد وجدت هذه المقالة مثيرة للاهتمام وفكرت في مشاركتها معك. تحقق من ذلك: {{url()->current()}}">
                                                                                    <li class="dasht-soc-mob-email">
                                                                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                                                    </li>
                                                                                </a>
                                                                                <a href="#comments">
                                                                                    <li class="dasht-soc-mob-com dasht-com-click">
                                                                                        <i class="fa fa-comment-o" aria-hidden="true"></i>
                                                                                    </li>
                                                                                </a>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="dasht-soc-mob-right left relative">
                                                                        <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="dasht-post-soc-in">
                                                                <div id="dasht-content-body" class="left relative">
                                                                    <div id="dasht-content-body-top" class="left relative">
                                                                        @if($content->type != 'opinion')
                                                                            <div class="dasht-author-info-name left relative" itemprop="author" itemscope itemtype="https://schema.org/Person">
                                                                            <span class="author-name vcard fn author" itemprop="name">
                                                                                @foreach($content->written as $key=>$record)
                                                                                    <a href="#" title="" rel="author">{{$record['written_title']}} : {{$record['written_by']}} |</a>
                                                                                @endforeach
                                                                            </span>
                                                                            </div>
                                                                        @endif
                                                                        <div id="dasht-content-main" class="left relative read-more">

                                                                            @foreach($content_array as $key=>$record)

                                                                                @if(is_array($record))
                                                                                    @if($record['type'] == 'img')
                                                                                        <div class="col-sm-12 col-md-12">
                                                                                            <div class="thumbnail">
                                                                                                <img src="{{env('APP_URL')}}/images/large/{{$record['content']}}" alt="{{$record['title']}}" style="width: 100%; display: block;">
                                                                                                <span class="dasht-feat-caption" style="margin-top: 0px;">{{$record['title']}}</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    @elseif($record['type'] == 'album')
                                                                                        <section class="dasht-post-gallery-wrap left relative">
                                                                                            <a href="/album/{{$content->_id}}/{{$record['album']['mydasht_id']}}/0">
                                                                                                <div class="dasht-post-gallery-top left relative flexslider">
                                                                                                    <ul class="dasht-post-gallery-top-list slides">
                                                                                                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide">
                                                                                                            <img  src="/images/1000x600/{{$record['album']['main_img']}}" class="attachment-dasht-post-thumb size-dasht-post-thumb" alt="" draggable="false" width="1000" height="600">
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="dasht-post-gallery-bot left relative flexslider">
                                                                                                    <div class="flex-viewport" style="overflow: hidden; position: relative;">
                                                                                                        <ul class="dasht-post-gallery-bot-list slides" style="width: 400%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                                                                                                            @foreach($record['album']['album_img'] as $img)
                                                                                                                <li class="flex-active-slide">
                                                                                                                    <img src="/images/150x150/{{$img['id']}}" class="attachment-dasht-small-thumb size-dasht-small-thumb" draggable="false" width="80" height="80">
                                                                                                                </li>
                                                                                                            @endforeach
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                    <ul class="flex-direction-nav">
                                                                                                        <li class="flex-nav-prev">
                                                                                                            <a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a>
                                                                                                        </li>
                                                                                                        <li class="flex-nav-next">
                                                                                                            <a class="flex-next flex-disabled" href="#" tabindex="-1">Next</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </a>
                                                                                        </section>


                                                                                    @elseif($record['type'] == 'video')
                                                                                        @if($record['video']['provider'] == 'youtube')
                                                                                            <iframe width="100%" height="500" src="https://www.youtube.com/embed/{{$record['video']['code']}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                                                        @elseif($record['video']['provider'] == 'vimeo')
                                                                                            <iframe src="https://player.vimeo.com/video/{{$record['video']['code']}}" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                                                        @elseif($record['video']['provider'] == 'dailymotion')
                                                                                            <iframe frameborder="0" width="100%" height="500" src="//www.dailymotion.com/embed/video/{{$record['video']['code']}}" allowfullscreen allow="autoplay"></iframe>
                                                                                        @elseif($record['video']['provider'] == 'facebook')
                                                                                            {{$record['video']['code']}}
                                                                                        @elseif($record['video']['provider'] == 'twitter')
                                                                                            {{$record['video']['code']}}
                                                                                        @elseif($record['video']['provider'] == 'unknown')
                                                                                            {{$record['video']['code']}}
                                                                                        @endif
                                                                                    @elseif($record['type'] == 'iframe')
                                                                                        <?=htmlspecialchars_decode($record['content'])?>
                                                                                    @elseif($record['type'] == 'text')
                                                                                        <P><?=htmlspecialchars_decode($record['content'])?></P>
                                                                                    @elseif($record['type'] == 'paragraph')
                                                                                        <blockquote class="blockquote"><p><?=htmlspecialchars_decode($record['content'])?></p></blockquote>
                                                                                    @elseif($record['type'] == 'exclamation')
                                                                                        <blockquote class="info"><p class="info_p"><?=htmlspecialchars_decode($record['content'])?></p></blockquote>
                                                                                    @endif
                                                                                @else
                                                                                    <P><?=htmlspecialchars_decode($record)?></P>
                                                                                    @if($key == 2)
                                                                                        <div class="dasht-feat1-right-out left relative">
                                                                                            @include('ads.home_responsive_3')
                                                                                        </div>
                                                                                    @elseif($key == 8)
                                                                                        <div class="dasht-feat1-right-out left relative">
                                                                                            @include('ads.home_responsive_4')
                                                                                        </div>
                                                                                    @elseif($key == 14)
                                                                                        <div class="dasht-feat1-right-out left relative">
                                                                                            @include('ads.home_responsive_4')
                                                                                        </div>
                                                                                    @endif
                                                                                @endif

                                                                            @endforeach

                                                                        </div>
                                                                        <div id="dasht-content-bot" class="left">
                                                                            <div class="dasht-post-tags">
                                                                                <span itemprop="keywords">
                                                                                    <?php $tags = explode(',', $content->tags);?>
                                                                                    @foreach($tags as $tag)
                                                                                        <a href="{{ (new App\Http\Helpers\Helpers)->generate_url('tags',$tag,'0',$tag)}}" rel="tags" title="{{$tag}}" rel="tag">{{$tag}}</a>
                                                                                    @endforeach
                                                                                </span>
                                                                            </div>

                                                                        </div>
                                                                        <div id="dasht-read-more" class="dasht-post-add-link">
                                                                            <a href="https://www.elrsala.com/{{ (new App\Http\Helpers\Helpers)->generate_url('news',$content->_id,$content['section_name']['name'],$content->title)}}#read-more" title="{{$content->title}}" rel="bookmark">
                                                                                <span class="dasht-post-add-link-but">أكمل القراءة</span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="dasht-cont-read-wrap">
                                                                        <div id="dasht-comments-button" class="left relative dasht-com-click hide">
                                                                            <span class="dasht-comment-but-text"> التعليقات</span>
                                                                        </div>
                                                                        <div id="comments">
                                                                            <h4 class="dasht-sec-head">
                                                                                <span class="dasht-sec-head">التعليقات</span>
                                                                                <span class="dasht-widget-head-link">
                                                                                </span>
                                                                            </h4>
                                                                            <div class="fb-comments" data-href="{{url()->current()}}" data-width="100%" data-numposts="5"></div>
                                                                        </div>
                                                                    </div>
                                                                        <div id="dasht-related-posts" class="left relative">
                                                                            <h4 class="dasht-sec-head">
                                                                                <span class="dasht-sec-head">تغطية خاصة</span>
                                                                                <span class="dasht-widget-head-link"></span>
                                                                            </h4>
                                                                            <ul class="dasht-related-posts-list left related">
                                                                                @foreach($content->related_file as $key=>$record)

                                                                                            <li>
                                                                                                <a href="{{ (new App\Http\Helpers\Helpers)->generate_url('news',$record->_id,$record['section_name']['name'],$record->title)}}" rel="bookmark" title="{{$record->title}}">
                                                                                                <div class="dasht-related-img left relative">
                                                                                                    <img src="{{env('APP_URL')}}/images/590x354/{{$record->main_img}}" class="attachment- size- " alt="{{$record->main_img_description}}"  />
                                                                                                    <div class="dasht-related-text left relative">
                                                                                                        <p>{{ str_limit($record->title, 50) }}</p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                </a>
                                                                                            </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="dasht-side-wrap" class="left relative theiaStickySidebar">
                                                <div class="dasht-feat1-list-wrap left relative">
                                                    <h4 class="dasht-sec-head">
                                                        <span class="dasht-sec-head">الأكثر قراءة اليوم</span>
                                                        <span class="dasht-widget-head-link"></span>
                                                    </h4>
                                                    <div class="dasht-feat1-pop-wrap left relative">
                                                        @foreach($hits as $key=>$record)
                                                            <a href="{{ (new App\Http\Helpers\Helpers)->generate_url('news',$record->_id,$record['section_name']['name'],$record->title)}}" rel="bookmark" title="{{$record->title}}">
                                                                <div class="dasht-feat1-pop-cont left relative">
                                                                    <div class="dasht-feat1-pop-img left relative">
                                                                        <img src="{{env('APP_URL')}}/images/590x354/{{$record->main_img}}" class="attachment- size- " alt="{{$record->main_img_description}}"  />


                                                                        @if($record->type == 'video')
                                                                            <div class="dasht-vid-box-wrap dasht-vid-box-mid dasht-vid-marg">
                                                                                <i class="fa fa-2 fa-play" aria-hidden="true"></i>
                                                                            </div>
                                                                        @elseif($record->type == 'album')
                                                                            <div class="dasht-vid-box-wrap dasht-vid-box-mid dasht-vid-marg">
                                                                                <i class="fa fa-2 fa-camera" aria-hidden="true"></i>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="dasht-feat1-pop-text left relative">
                                                                        <div class="dasht-cat-date-wrap left relative">
                                                                            <span class="dasht-cd-cat left relative">{{$record['section_name']['name']}}</span>
                                                                            <span class="dasht-cd-date left relative">
                                                                               {{ (new App\Http\Helpers\Helpers)->datetime($record->created_at)}}
                                                                        </span>
                                                                        </div>
                                                                        <h2>{{$record->title}}</h2>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="dasht-feat1-list-ad left relative margin-top-20">
                                                    @include('ads.home_box_1')
                                                </div>
                                                {{--<div class="dasht-feat1-list-ad left relative margin-top-20">--}}
                                                {{--<iframe width="300" height="250" src="https://www.akhbarak.net/gallerywidgetgen" frameborder="0" scrolling="no"></iframe>--}}
                                                {{--</div>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </section>
            </div>
            <div class="dasht-main-box">
                <section id="dasht-feat1-wrap" class="left relative">
                    <div class="dasht-feat1-right-out left relative">
                        @include('ads.home_responsive_2')
                    </div>
                </section>
            </div>
        </div>

        <a href="{{ (new App\Http\Helpers\Helpers)->generate_url('news',$next->_id,$next['section_name']['name'],$next->title)}}"  id="next" style="display: none">التالى</a>
        @if($content->type == 'video')
            <script>
                $(window).on("scroll.video", function(event){
                    var scrollTop     = $(window).scrollTop();
                    var elementOffset = $("#dasht-content-wrap").offset().top;
                    var distance      = (elementOffset - scrollTop);
                    var aboveHeight = $("#dasht-video-embed-wrap").outerHeight();
                    if ($(window).scrollTop() > distance + aboveHeight + screen.height){
                        $("#dasht-video-embed-cont").addClass("dasht-vid-fixed");
                        $("#dasht-video-embed-wrap").addClass("dasht-vid-height");
                        $(".dasht-video-close").show();
                    } else {
                        $("#dasht-video-embed-cont").removeClass("dasht-vid-fixed");
                        $("#dasht-video-embed-wrap").removeClass("dasht-vid-height");
                        $(".dasht-video-close").hide();
                    }
                });
                $(".dasht-video-close").on("click", function(){
                    $("iframe").attr("src", $("iframe").attr("src"));
                    $("#dasht-video-embed-cont").removeClass("dasht-vid-fixed");
                    $("#dasht-video-embed-wrap").removeClass("dasht-vid-height");
                    $(".dasht-video-close").hide();
                    $(window).off("scroll.video");
                });
            </script>
    @endif
@endsection
