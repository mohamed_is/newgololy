@if($record['video']['provider'] == 'youtube')
    <iframe width="100%" height="500" src="https://www.youtube.com/embed/{{$record['video']['code']}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
@elseif($record['video']['provider'] == 'vimeo')
    <iframe src="https://player.vimeo.com/video/{{$record['video']['code']}}" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
@elseif($record['video']['provider'] == 'dailymotion')
    <iframe frameborder="0" width="100%" height="500" src="//www.dailymotion.com/embed/video/{{$record['video']['code']}}" allowfullscreen allow="autoplay"></iframe>
@elseif($record['video']['provider'] == 'facebook')
    {{$record['video']['code']}}
@elseif($record['video']['provider'] == 'twitter')
    {{$record['video']['code']}}
@elseif($record['video']['provider'] == 'unknown')
    {{$record['video']['code']}}
@endif