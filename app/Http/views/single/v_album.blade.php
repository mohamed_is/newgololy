<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <a href="/album/{{$content->_id}}/{{$record['album']['mydasht_id']}}/0">
            <img  src="/images/925x520/{{$record['album']['main_img']}}"  width="100%">
        </a>
    </div>
    <div id="thumbs">
        @if(isset($content['main_album']['album_img']) != false)
            @foreach($content['main_album']['album_img'] as $img)
                <div class="thumbs_item" >
                    <a href="/album/{{$content->_id}}/{{$record['album']['mydasht_id']}}/{{$key}}">
                        <img src="/images/110x61/{{$img['id']}}" width="100%">
                    </a>
                </div>
            @endforeach
        @endif
    </div>
</div>