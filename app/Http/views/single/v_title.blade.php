<div class="post-list-desc">
    <h1 itemprop="headline">{!! $content->title !!}</h1>
    <label>
        <time itemprop="datePublished" datetime="{{$content->created_at}}">
            {{ (new App\Http\Helpers\Helpers)->date($content->created_at)}}
        </time>
    </label>
</div>