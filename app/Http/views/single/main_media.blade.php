<div itemprop="publisher" itemscope itemtype="https://schema.org/Organization"  itemid="{{env('APP_URL')}}/" style="display: none">
    <div itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
        <img src="{{env('APP_URL')}}/assets/images/logo.png" alt="جولولي">
        <meta itemprop="url" content="{{env('APP_URL')}}">
    </div>
    <meta itemprop="name" content="جولولي">
</div>

        @if($content->type === 'article')
            @if(in_array("inside_img", $content['options']))
{{--                <figure class="news-article-hero">--}}
{{--                                            <span class="news-article-hero-image-wrap" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">--}}
{{--                                                 @if(strpos($content['main_img'], 'photo') !== false)--}}
                                                    <img src="{{env('APP_URL')}}/images/800x500/{{$content->main_img}}" alt="{{$content['img_description']}}">
{{--                                                @else--}}
{{--                                                    <picture>--}}
{{--                                                    <source media="(max-width: 1099px)" srcset="/images/800x500/{{$content->main_img}}">--}}
{{--                                                    <img src="/images/800x500/{{$content->main_img}}" alt="{{$content->img_description}}" title="{{$content->img_description}}" itemprop="contentUrl" />--}}
{{--                                                </picture>--}}
{{--                                                @endif--}}

                                                <meta itemprop="url" content="{{env('APP_URL')}}/images/800x500/{{$content->main_img}}">
                                                <meta itemprop="width" content="750">
                                                <meta itemprop="height" content="450">
{{--                                            </span>--}}
{{--                </figure>--}}
{{--                <p style="text-align: center;padding: 10px 10px 5px 10px;font-size: 8pt;">--}}
{{--                    {{$content->img_description}}--}}
{{--                </p>--}}
            @endif
        @elseif($content->type === 'infographic')
            <figure class="news-article-hero">
                <span class="news-article-hero-image-wrap" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
                    <picture>
                        <source media="(max-width: 1099px)" srcset="{{env('APP_URL')}}/images/large/{{$content->main_img}}">
                        <img src="{{env('APP_URL')}}/images/large/{{$content->main_img}}" alt="{{$content->img_description}}" title="{{$content->img_description}}" itemprop="contentUrl" />
                    </picture>
                    <meta itemprop="url" content="{{env('APP_URL')}}/images/large/{{$content->main_img}}">
                    <meta itemprop="width" content="750">
                    <meta itemprop="height" content="450">
                </span>
            </figure>
            <div style="text-align: center;padding: 10px 10px 5px 10px;font-size: 10pt;">
                {{html_entity_decode($content->img_description)}}
            </div>

        @elseif($content->type === 'album')

            <figure class="news-article-hero">
                <span class="news-article-hero-image-wrap" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
                    <a href="/album/{{$content->_id}}/0?url={{$content->url}}">
                    <picture>
                        <source media="(max-width: 1099px)" srcset="{{env('APP_URL')}}/images/800x500/{{$content->main_img}}">
                        <img src="{{env('APP_URL')}}/images/800x500/{{$content->main_img}}" alt="{{$content->img_description}}" title="{{$content->img_description}}" itemprop="contentUrl" />
                    </picture>
                    </a>
                    <meta itemprop="url" content="{{env('APP_URL')}}/images/800x500/{{$content->main_img}}">
                    <meta itemprop="width" content="750">
                    <meta itemprop="height" content="450">
                </span>
            </figure>
            <div style="text-align: center;padding: 10px 10px 5px 10px;font-size: 10pt;">
                {{html_entity_decode($content->img_description)}}
            </div>
        @elseif($content->type === 'video')
                @if(isset($content['video']['provider']))
                    @if($content['video']['provider'] == 'youtube')
                    <iframe class="content-media__object" id="featured-video" width="100%" height="500px" src="https://www.youtube.com/embed/{{$content['video']['code']}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    @elseif($content['video']['provider'] == 'vimeo')
                        <iframe class="content-media__object" id="featured-video" src="https://player.vimeo.com/video/{{$content['video']['code']}}" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    @elseif($content['video']['provider'] == 'dailymotion')
                        <iframe class="content-media__object" id="featured-video" frameborder="0" width="100%" height="500" src="//www.dailymotion.com/embed/video/{{$content['video']['code']}}" allowfullscreen allow="autoplay"></iframe>
                    @elseif($content['video']['provider'] == 'facebook')
                        <?=html_entity_decode($content['video']['code'])?>
                    @elseif($content['video']['provider'] == 'twitter')
                        <?=html_entity_decode($content['video']['code'])?>
                    @elseif($content['video']['provider'] == 'unknown')
                        <?=html_entity_decode($content['video']['code'])?>
                    @endif
                @else
                    @if(in_array("inside_img", $content['options']))
{{--                        <figure class="news-article-hero">--}}
{{--                            <span class="news-article-hero-image-wrap" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">--}}
{{--                                <picture>--}}
{{--                                    <source media="(max-width: 1099px)" srcset="{{env('APP_URL')}}/images/800x500/{{$content->main_img}}">--}}
                                    <img src="{{env('APP_URL')}}/images/800x500/{{$content->main_img}}" alt="{{$content->img_description}}" title="{{$content->img_description}}" itemprop="contentUrl" />
{{--                                </picture>--}}
                                <meta itemprop="url" content="{{env('APP_URL')}}/images/800x500/{{$content->main_img}}">
                                <meta itemprop="width" content="750">
                                <meta itemprop="height" content="450">
{{--                            </span>--}}
{{--                        </figure>--}}
{{--                        <div style="text-align: center;padding: 10px 10px 5px 10px;font-size: 10pt;">--}}
{{--                            {{$content->img_description}}--}}
{{--                        </div>--}}
                    @endif
            @endif
        @endif



