{{--@php--}}
{{--    $keywords = array();--}}
{{--    if(isset($content['tags_data'])){--}}
{{--        foreach($content->tags_data as $key=>$tag){--}}
{{--            array_push($keywords,$tag['keyword']);--}}
{{--        }--}}
{{--    }--}}
{{--@endphp--}}
{{--@section('keywords', implode(",",$keywords))--}}
{{--@if(in_array("noindex", $content['seo']))--}}
{{--    @section('robots', 'noindex, nofollow')--}}
{{--@section('googlebot', 'noindex, nofollow')--}}
{{--@else--}}
{{--    @section('robots', 'index, follow')--}}
{{--@section('googlebot', 'index, follow')--}}
{{--@endif--}}

{{--@if(in_array("ads", $content['seo']))--}}
{{--    @section('ads', 'on')--}}
{{--@else--}}
{{--    @section('ads', 'off')--}}
{{--@endif--}}
@php
return $content;
@endphp
@section('title', $content->post_title)
@section('description', $content->post_title)
@section('url', env('APP_URL').'/'.$content->post_name)
@section('image', env('APP_URL').'/images/750x450/'.$content->main_img)

{{--@section('section', $section->name)--}}
{{--@section('published_time', $content->publication_date)--}}
{{--@section('modified_time', $content->last_update_date)--}}
{{--@section('author', $content->written)--}}

@extends('layouts.app')
@section('content')

{{--    <script type="application/ld+json">--}}
{{--{--}}
{{--  "@context": "https://schema.org",--}}
{{--  "@type": "Article",--}}
{{--  "mainEntityOfPage": {--}}
{{--    "@type": "WebPage",--}}
{{--    "@id": "{{env('APP_URL')}}/{{$content->url}}"--}}
{{--  },--}}
{{--  "headline": "{{$content->title}}",--}}
{{--  "image": [--}}
{{--    "{{env('APP_URL')}}/{{$content->main_img}}"--}}
{{--   ],--}}
{{--  "datePublished": "{{$content->publication_date}}",--}}
{{--  "dateModified": "{{$content->publication_date}}",--}}
{{--  "author": {--}}
{{--    "@type": "Person",--}}
{{--    "name": "{{$content->written}}"--}}
{{--  },--}}
{{--   "publisher": {--}}
{{--    "@type": "Organization",--}}
{{--    "name": "جولولى",--}}
{{--    "logo": {--}}
{{--      "@type": "ImageObject",--}}
{{--      "url": "{{env('APP_URL')}}/assets/images/logo.png"--}}
{{--    }--}}
{{--  },--}}
{{--  "description": "{{$content->description}}"--}}
{{--}--}}
{{--</script>--}}

    <div class="container" id="app-box" data-src="single">
        @if(in_array("ads", $content['seo']))
            <div class="ads">
                <?php include(public_path().'/ad_file/5e1b2ae229cc0f069520b273.php')?>
            </div>
        @endif

        <div class="layout-wrap">
            <section class="main-section">

{{--                <div class="breadCrumb mt15">--}}
{{--                    <div class="breadCrumb__list"><span class="icon breadCrumb__home">--}}
{{--                  <svg xmlns="http://www.w3.org/2000/svg" height="511pt" viewBox="0 1 511 511.999" width="511pt">--}}
{{--                    <path d="m498.699219 222.695312c-.015625-.011718-.027344-.027343-.039063-.039062l-208.855468-208.847656c-8.902344-8.90625-20.738282-13.808594-33.328126-13.808594-12.589843 0-24.425781 4.902344-33.332031 13.808594l-208.746093 208.742187c-.070313.070313-.144532.144531-.210938.214844-18.28125 18.386719-18.25 48.21875.089844 66.558594 8.378906 8.382812 19.441406 13.234375 31.273437 13.746093.484375.046876.96875.070313 1.457031.070313h8.320313v153.695313c0 30.417968 24.75 55.164062 55.167969 55.164062h81.710937c8.285157 0 15-6.71875 15-15v-120.5c0-13.878906 11.292969-25.167969 25.171875-25.167969h48.195313c13.878906 0 25.167969 11.289063 25.167969 25.167969v120.5c0 8.28125 6.714843 15 15 15h81.710937c30.421875 0 55.167969-24.746094 55.167969-55.164062v-153.695313h7.71875c12.585937 0 24.421875-4.902344 33.332031-13.8125 18.359375-18.367187 18.367187-48.253906.027344-66.632813zm-21.242188 45.421876c-3.238281 3.238281-7.542969 5.023437-12.117187 5.023437h-22.71875c-8.285156 0-15 6.714844-15 15v168.695313c0 13.875-11.289063 25.164062-25.167969 25.164062h-66.710937v-105.5c0-30.417969-24.746094-55.167969-55.167969-55.167969h-48.195313c-30.421875 0-55.171875 24.75-55.171875 55.167969v105.5h-66.710937c-13.875 0-25.167969-11.289062-25.167969-25.164062v-168.695313c0-8.285156-6.714844-15-15-15h-22.328125c-.234375-.015625-.464844-.027344-.703125-.03125-4.46875-.078125-8.660156-1.851563-11.800781-4.996094-6.679688-6.679687-6.679688-17.550781 0-24.234375.003906 0 .003906-.003906.007812-.007812l.011719-.011719 208.847656-208.839844c3.234375-3.238281 7.535157-5.019531 12.113281-5.019531 4.574219 0 8.875 1.78125 12.113282 5.019531l208.800781 208.796875c.03125.03125.066406.0625.097656.09375 6.644531 6.691406 6.632813 17.539063-.03125 24.207032zm0 0"></path>--}}
{{--                  </svg></span>--}}
{{--                        <li class="breadCrumb__item"><a href="{{env('APP_URL')}}/" class="breadCrumb__link">الرئيسية</a></li><span class="icon">--}}
{{--                  <svg id="Layer" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 64 64" height="512" viewBox="0 0 64 64" width="512">--}}
{{--                    <path d="m54 30h-39.899l15.278-14.552c.8-.762.831-2.028.069-2.828-.761-.799-2.027-.831-2.828-.069l-17.448 16.62c-.755.756-1.172 1.76-1.172 2.829 0 1.068.417 2.073 1.207 2.862l17.414 16.586c.387.369.883.552 1.379.552.528 0 1.056-.208 1.449-.621.762-.8.731-2.065-.069-2.827l-15.342-14.552h39.962c1.104 0 2-.896 2-2s-.896-2-2-2z"></path>--}}
{{--                  </svg></span>--}}
{{--                        <li class="breadCrumb__item"><a href="{{env('APP_URL')}}/@if(isset($section->url)){{$section->url}}@endif" class="breadCrumb__link">@if(isset($section->url)){{$section->name}}@endif</a></li><span class="icon">--}}
{{--                  <svg id="Layer" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 64 64" height="512" viewBox="0 0 64 64" width="512">--}}
{{--                    <path d="m54 30h-39.899l15.278-14.552c.8-.762.831-2.028.069-2.828-.761-.799-2.027-.831-2.828-.069l-17.448 16.62c-.755.756-1.172 1.76-1.172 2.829 0 1.068.417 2.073 1.207 2.862l17.414 16.586c.387.369.883.552 1.379.552.528 0 1.056-.208 1.449-.621.762-.8.731-2.065-.069-2.827l-15.342-14.552h39.962c1.104 0 2-.896 2-2s-.896-2-2-2z"></path>--}}
{{--                  </svg></span>--}}
{{--                        <li class="breadCrumb__item"><span class="breadCrumb__link">{{$content->title}}</span></li>--}}
{{--                    </div>--}}
{{--                </div>--}}


                <div class="breadCrumb mt15">
                    <div class="breadCrumb__list"><span class="icon breadCrumb__home">
                  <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                    <g>
                      <g>
                        <polygon points="256,152.96 79.894,288.469 79.894,470.018 221.401,470.018 221.401,336.973 296.576,336.973 296.576,470.018     432.107,470.018 432.107,288.469   "></polygon>
                      </g>
                    </g>
                    <g>
                      <g>
                        <polygon points="439.482,183.132 439.482,90.307 365.316,90.307 365.316,126.077 256,41.982 0,238.919 35.339,284.855     256,115.062 476.662,284.856 512,238.92   "></polygon>
                      </g>
                    </g>
                  </svg></span>
                        <li class="breadCrumb__item"><a class="breadCrumb__link">الرئيسية</a></li><span class="icon">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 492 492" style="enable-background:new 0 0 492 492;" xml:space="preserve">
                    <g>
                      <g>
                        <path d="M198.608,246.104L382.664,62.04c5.068-5.056,7.856-11.816,7.856-19.024c0-7.212-2.788-13.968-7.856-19.032l-16.128-16.12    C361.476,2.792,354.712,0,347.504,0s-13.964,2.792-19.028,7.864L109.328,227.008c-5.084,5.08-7.868,11.868-7.848,19.084    c-0.02,7.248,2.76,14.028,7.848,19.112l218.944,218.932c5.064,5.072,11.82,7.864,19.032,7.864c7.208,0,13.964-2.792,19.032-7.864    l16.124-16.12c10.492-10.492,10.492-27.572,0-38.06L198.608,246.104z"></path>
                      </g>
                    </g>
                  </svg></span>
                        <li class="breadCrumb__item"><a href="#" class="breadCrumb__link">سير ذاتية</a></li><span class="icon">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 492 492" style="enable-background:new 0 0 492 492;" xml:space="preserve">
                    <g>
                      <g>
                        <path d="M198.608,246.104L382.664,62.04c5.068-5.056,7.856-11.816,7.856-19.024c0-7.212-2.788-13.968-7.856-19.032l-16.128-16.12    C361.476,2.792,354.712,0,347.504,0s-13.964,2.792-19.028,7.864L109.328,227.008c-5.084,5.08-7.868,11.868-7.848,19.084    c-0.02,7.248,2.76,14.028,7.848,19.112l218.944,218.932c5.064,5.072,11.82,7.864,19.032,7.864c7.208,0,13.964-2.792,19.032-7.864    l16.124-16.12c10.492-10.492,10.492-27.572,0-38.06L198.608,246.104z"></path>
                      </g>
                    </g>
                  </svg></span>
                        <li class="breadCrumb__item"><span class="breadCrumb__link">{{$content->post_title}}</span></li>
                    </div>
                </div>





                <div id="single-wrapper" class="single-wrapper" article-id="{{ $content->ID }}">
                    <div class="single-wrapper__title">
                        <h1>{{$content->post_title}}</h1>
                    </div>
                    <div class="single-wrapper__status">
                        <div class="status-cell">
                            <div class="status-cell__icon icon">
                                <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                      <path d="M437.02,74.98C388.667,26.629,324.38,0,256,0S123.333,26.629,74.98,74.98C26.629,123.333,0,187.62,0,256    s26.629,132.667,74.98,181.02C123.333,485.371,187.62,512,256,512s132.667-26.629,181.02-74.98    C485.371,388.667,512,324.38,512,256S485.371,123.333,437.02,74.98z M256,472c-119.103,0-216-96.897-216-216S136.897,40,256,40    s216,96.897,216,216S375.103,472,256,472z"></path>
                                    <polygon points="276,236 276,76 236,76 236,276 388,276 388,236   "></polygon>
                    </svg>
                            </div>
                            <div class="status-cell__text">{{ (new App\Http\Helpers\Helpers)->date($content->publication_date)}}</div>
                        </div>
                        <div class="status-cell">
                            <div class="status-cell__icon icon">
                                <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 383.947 383.947" style="enable-background:new 0 0 383.947 383.947;" xml:space="preserve">
                      <polygon points="0,303.947 0,383.947 80,383.947 316.053,147.893 236.053,67.893    "></polygon>
                                    <path d="M377.707,56.053L327.893,6.24c-8.32-8.32-21.867-8.32-30.187,0l-39.04,39.04l80,80l39.04-39.04     C386.027,77.92,386.027,64.373,377.707,56.053z"></path>
                    </svg>
                            </div>
                            <div class="status-cell__text">{{$content->written}}</div>
                        </div>
                        <div class="status-cell">
                            <div class="status-cell__icon icon">
                                <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                      <g>
                          <g>
                              <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035    c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201    C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418    c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418    C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
                          </g>
                      </g>
                                    <g>
                                        <g>
                                            <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275    s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516    s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
                                        </g>
                                    </g>
                    </svg>
                            </div>
                            <div class="status-cell__text">{{$views->hits}}</div>
                        </div>

                    </div>

                    <div class="single-wrapper__img-holder mb25">
                        @include('single.main_media')
                    </div>
                    <div class="single-wrapper__content">

                        <div class="social-box no-touch">
                            <div class="handle-sticky"><a href="#">
                                    <div class="icon fb">
                                        <svg aria-hidden="true" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512" data-fa-i2svg="">
                                            <path d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"></path>
                                        </svg>
                                    </div></a><a href="#"><span class="icon twtr">
                        <svg aria-hidden="true" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                          <path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path>
                        </svg></span></a><a href="#"><span class="icon wts">
                        <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                          <g>
                            <g>
                              <path d="M256.064,0h-0.128C114.784,0,0,114.816,0,256c0,56,18.048,107.904,48.736,150.048l-31.904,95.104l98.4-31.456    C155.712,496.512,204,512,256.064,512C397.216,512,512,397.152,512,256S397.216,0,256.064,0z M405.024,361.504    c-6.176,17.44-30.688,31.904-50.24,36.128c-13.376,2.848-30.848,5.12-89.664-19.264C189.888,347.2,141.44,270.752,137.664,265.792    c-3.616-4.96-30.4-40.48-30.4-77.216s18.656-54.624,26.176-62.304c6.176-6.304,16.384-9.184,26.176-9.184    c3.168,0,6.016,0.16,8.576,0.288c7.52,0.32,11.296,0.768,16.256,12.64c6.176,14.88,21.216,51.616,23.008,55.392    c1.824,3.776,3.648,8.896,1.088,13.856c-2.4,5.12-4.512,7.392-8.288,11.744c-3.776,4.352-7.36,7.68-11.136,12.352    c-3.456,4.064-7.36,8.416-3.008,15.936c4.352,7.36,19.392,31.904,41.536,51.616c28.576,25.44,51.744,33.568,60.032,37.024    c6.176,2.56,13.536,1.952,18.048-2.848c5.728-6.176,12.8-16.416,20-26.496c5.12-7.232,11.584-8.128,18.368-5.568    c6.912,2.4,43.488,20.48,51.008,24.224c7.52,3.776,12.48,5.568,14.304,8.736C411.2,329.152,411.2,344.032,405.024,361.504z"></path>
                            </g>
                          </g>
                        </svg></span></a></div>
                        </div>


                        <div class="single-wrapper__article">
                            @foreach($content->content as $key=>$record)
                                <p><?=htmlspecialchars_decode($record)?></p>
                            @endforeach

                            @if(!empty($content->video))
                                <iframe src="https://www.youtube.com/embed/{{ $content->video }}"></iframe>
                                @endif
                            @if(!empty($content['album']))
                                    @foreach($content->album_data as $key=>$album)
                                        <div class="col-sm-12" style="margin-bottom: 30px">
                                            <img src="https://www.gololy.com/{{$album['images_url']}}" title="{{$album['name']}}" style="width: 100%">
                                        </div>
                                    @endforeach
                                @endif
                        </div>
                    </div>

                    @if(isset($content['tags_data']))
                    <div class="single-wrapper__tags">
                        @foreach($content->tags_data as $key=>$tag)
                            <a href="#">{{$tag['keyword']}}</a>
                        @endforeach
                    </div>
                    @endif
                    <div class="single-wrapper__nav"><a class="single-wrapper__prev" href="{{env('APP_URL')}}/{{ $prearticle->url }}"><span class="icon single-wrapper__arrow">
                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 492.004 492.004" style="enable-background:new 0 0 492.004 492.004;" xml:space="preserve">
                      <g>
                        <g>
                          <path d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12    c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028    c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265    c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z"></path>
                        </g>
                      </g>
                    </svg></span>
                            <div class="single-wrapper__nav-holder"><span class="single-wrapper__smText">السابق</span>
                                <h2>{{ $prearticle->title }}</h2>
                            </div></a><a class="single-wrapper__next" href="{{env('APP_URL')}}/{{ $nextarticle->url }}">
                            <div class="single-wrapper__nav-holder"><span class="single-wrapper__smText">التالي</span>
                                <h2>{{ $nextarticle->title }}</h2>
                            </div><span class="icon single-wrapper__arrow">
                    <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 492 492" style="enable-background:new 0 0 492 492;" xml:space="preserve">
                      <g>
                        <g>
                          <path d="M198.608,246.104L382.664,62.04c5.068-5.056,7.856-11.816,7.856-19.024c0-7.212-2.788-13.968-7.856-19.032l-16.128-16.12    C361.476,2.792,354.712,0,347.504,0s-13.964,2.792-19.028,7.864L109.328,227.008c-5.084,5.08-7.868,11.868-7.848,19.084    c-0.02,7.248,2.76,14.028,7.848,19.112l218.944,218.932c5.064,5.072,11.82,7.864,19.032,7.864c7.208,0,13.964-2.792,19.032-7.864    l16.124-16.12c10.492-10.492,10.492-27.572,0-38.06L198.608,246.104z"></path>
                        </g>
                      </g>
                    </svg></span></a></div>

                    <div class="related-section">
                        <div class="main-title">
                            <div class="main-title__box">
                                <h1>تريند </h1>
                            </div>
                        </div>
                        <div class="section__content">
                            <div class="row">
                                @foreach($hits as $key=>$record)
                                    @if($key <= 2)
                                        <div class="col-lg-4 col-md-6 col-sm-6">
                                            <div class="news-card news-card--card"><a class="news-card__link" href="/{{$record['contentData']['url']}}"></a>
                                                <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['contentData']['main_img']}}"/>
                                                </div>
                                                <div class="news-card__caption">
                                                    <div class="news-card__details">
{{--                                                        <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>--}}
{{--                                                        </div>--}}
                                                        <h4 class="news-card__title">{{$record['contentData']['title']}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="ads">
                                <?php include(public_path().'/ad_file/5e1b2ae229cc0f069520b273.php')?>
                            </div>
                            <div class="row">
                                @foreach($hits as $key=>$record)
                                    @if($key > 2 && $key < 6)
                                        <div class="col-lg-4 col-md-6 col-sm-6">
                                            <div class="news-card news-card--card"><a class="news-card__link" href="/{{$record['contentData']['url']}}"></a>
                                                <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['contentData']['main_img']}}"/>
                                                </div>
                                                <div class="news-card__caption">
                                                    <div class="news-card__details">
{{--                                                        <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>--}}
{{--                                                        </div>--}}
                                                        <h4 class="news-card__title">{{$record['contentData']['title']}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>



                </div>
                <div id="requested"></div>
            </section>
            <aside class="left-side">
                <div>
                    <div class="main-title">
                        <div class="main-title__box">
                            <h1>أقرأ أيضا</h1>
                        </div>
                    </div>

                    @foreach($latestarticles as $record)
                    <div class="news-card news-card--card news-card--switch"><a class="news-card__link" href="/{{$record->url}}"></a>
                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/150x150/{{$record->main_img}}"/>
                        </div>
                        <div class="news-card__caption">
                            <div class="news-card__details">
                                <div class="news-card__caption-row"><a class="cat" href="{{$record->sectionData[0]['url']}}">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record->publication_date)}}</span>
                                </div>
                                <h4 class="news-card__title">{{$record->title}}</h4>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </aside>
        </div>
    </div>


    <div style="bottom: 0px" id="cache"></div>
    <script>
                {{--$( document ).ready(function() {--}}
                {{--$.get("/backend/out/{{$content->_id}}", function(data, status){--}}
                {{--document.getElementById("cache").innerHTML = data;--}}
                {{--});--}}
                {{--});--}}


        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/backend/out/{{$content->_id}}');
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('cache')=xhr.responseText;
            }
        };
        xhr.send();
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/hits/{{$content->news_id}}');
        xhr.send();
    </script>



{{--    <div style="bottom: 0px" id="cache"></div>--}}
{{--    <script>--}}
{{--        $( document ).ready(function() {--}}
{{--            $.get("/cache/out/{{$content->_id}}", function(data, status){--}}
{{--                document.getElementById("cache").innerHTML = data;--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
@endsection
