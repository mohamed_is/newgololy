<?=
'<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL
?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9">
    @foreach($content as $item)
    <url>
        <loc>{{env('APP_URL')}}{{(new App\Http\Helpers\Helpers)->generate_url('news',$item->_id,$item->section_name,$item->title)}}</loc>
        <news:news>
            <news:publication>
                <news:name>بلدنا اليوم</news:name>
                <news:language>ar</news:language>
            </news:publication>
            <news:publication_date>{{ $item->created_at->format(DateTime::ATOM) }}</news:publication_date>
            <news:title><![CDATA[{{ $item->title }}]]></news:title>
        </news:news>
    </url>
    @endforeach

</urlset>