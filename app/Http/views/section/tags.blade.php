@section('title', $tags)
@section('description', $tags)
@section('keywords', $tags)
@section('robots', 'index, follow')
@section('googlebot', 'index, follow')
@section('url', url()->current())
@section('image', 'https://www.ahlmasrnews.com/images/750x450/')
@extends('layouts.app')
@section('content')
    <div class="home-page" id="maincontent">
        <section class="col-sm-12 container-ads">
            <?php include(public_path().'/ad_file/5e1b2ae229cc0f069520b273.php')?>
        </section>
        <section class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="section-title">{{$tags}}</h1>
                </div>
            </div>
            <div id="contentsWrapper">
                <div class="content" data-title="{{$next_url}}" data-text="{{$tags}}">
                    <div class="row article-grid-container">
                        @foreach($tag as $record)
                            @if(isset($record['content']))
                            <div class="col-sm-3 col-xs-12 article-container">
                                <a class="article " href="/{{$record['content']['url']}}">
                                    <div class="image-container">
                                        @if(strpos($record['content']['main_img'], 'photo') !== false)
                                        <img src="{{$record['content']['main_img']}}" title="{{$record['content']['img_description']}}">
                                        @else
                                        <img src="/images/450x250/{{$record['content']['main_img']}}" title="{{$record['content']['img_description']}}">
                                        @endif

                                    </div>
                                    <div class="article-panel match-height">

                                        <div class="news-detail-wrap">
                                            <h3>{{$record['content']['title']}}</h3>
                                            <span class="date detail">{{ (new App\Http\Helpers\Helpers)->date($record['content']['publication_date'])}}</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endif
                        @endforeach
                    </div>
                    <a href="{{$current_url}}" id="current" ></a>
                    @if(!empty($next_url))
                        <a href="{{$next_url}}" class="jscroll-next"></a>
                    @endif
                </div>
            </div>
            <section class="col-sm-12 container-ads">
                <?php include(public_path().'/ad_file/5e1b34e229cc0f23e90e2bf4.php')?>
            </section>
        </section>

    </div>

@endsection
