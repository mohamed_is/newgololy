
<?php
$xmlstr = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/XSL" href="https://www.ahlmasrnews.com/sitemap.xsl"?>
XML;
echo $xmlstr;
?>
<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php for ($record = (int)$count; $record > 0; $record--) { ?>
    <sitemap>
        <loc><?=env('APP_URL').'/sitemap?page='.$record?></loc>
        <lastmod>{{$content['publication_date']}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.8</priority>
    </sitemap>
    <?php } ?>

</sitemapindex>
