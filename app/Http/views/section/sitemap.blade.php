<?php
$xmlstr = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/XSL" href="https://www.ahlmasrnews.com/sitemap.xsl"?>

XML;
echo $xmlstr;
?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($content as $item)
        <url>
            <loc>{{env('APP_URL')}}/{{$item->url}}</loc>
            <lastmod>{{ $item->publication_date->format(DateTime::ATOM) }}</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach

</urlset>
