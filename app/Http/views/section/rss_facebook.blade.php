<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
    <channel>
        <title>الرسالة</title>
        <link>https://www.elrsala.com</link>
        <description>
            جريدة الرسالة تقدم احدث واهم اخبار مصر على مدار اليوم في مجالات الفن والسياسة والرياضة والاقتصاد والحوادث
        </description>
        <language>ar-eg</language>
        <lastBuildDate>{{$content['0']['created_at']}}</lastBuildDate>
        @foreach($content as $record)
            <item>
                <title>{{$record->title}}</title>
                <link>{{env('APP_URL')}}{{(new App\Http\Helpers\Helpers)->generate_url('news',$record->_id,$record['section_name']['name'],$record->title)}}</link>
                <guid>{{env('APP_URL')}}{{(new App\Http\Helpers\Helpers)->generate_url('news',$record->_id,$record['section_name']['name'],$record->title)}}</guid>
                <pubDate>{{$record->created_at}}</pubDate>
                <author>{{$record['written']['0']['written_title']}}</author>
                <content:encoded>
                    <![CDATA[
                    <!doctype html>
                    <html lang="ar" prefix="op: http://media.facebook.com/op#">
                    <head>
                        <meta charset="utf-8">
                        <meta property="fb:op-recirculation-ads" content="enable=true placement_id=1430397247094185_1430397297094180">
                        <meta property="fb:use_automatic_ad_placement" content="enable=true ad_density=default">
                        <link rel="canonical" href="{{env('APP_URL')}}{{(new App\Http\Helpers\Helpers)->generate_url('news',$record->_id,$record['section_name']['name'],$record->title)}}">
                        <meta property="op:markup_version" content="v1.0">
                    </head>
                    <body>
                    <article>
                        <figure class="op-tracker">
                            <iframe>
                                <script>
                                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                                    ga('create', 'UA-126713540-1', 'auto');
                                    ga('require', 'displayfeatures');
                                    ga('set', 'campaignSource', 'Facebook');
                                    ga('set', 'campaignMedium', 'Instant Article');
                                    ga('set', 'title', ia_document.title);
                                    ga('set', 'page', '{{env('APP_URL')}}{{(new App\Http\Helpers\Helpers)->generate_url('news',$record->_id,$record['section_name']['name'],$record->title)}}#facebook_instant_articles');
                                    ga('send', 'pageview');
                                </script>

                            </iframe>
                        </figure>
                        <header>
                            <section class="op-ad-template">
                                <!-- Ads to be automatically placed throughout the article -->
                                <figure class="op-ad">
                                    <iframe src="https://www.adserver.com/ss;adtype=banner300x250&adslot=1" height="300" width="250"></iframe>
                                </figure>
                                <figure class="op-ad op-ad-default">
                                    <iframe src="https://www.adserver.com/ss;adtype=banner300x250&adslot=2" height="300" width="250"></iframe>
                                </figure>
                                <figure class="op-ad">
                                    <iframe src="https://www.adserver.com/ss;adtype=banner300x250&adslot=3" height="300" width="250"></iframe>
                                </figure>
                            </section>
                            <figure data-mode=aspect-fit>
                                <img src="{{env('APP_URL')}}/images/1000x600/{{$record->main_img}}">
                            </figure>
                            <h1>{{$record->title}}</h1>
                        </header>


                        @foreach($record->content_array as $key=>$rec)
                            @if(is_array($rec))
                                @if($rec['type'] == 'img')
                                    <figure>
                                        <img src="{{env('APP_URL')}}/images/large/{{$rec['content']}}">
                                    </figure>
                                @elseif($rec['type'] == 'album')
                                    {{--@foreach($rec['album']['album_img'] as $img)--}}
                                        {{--<figure>--}}
                                            {{--<img src="{{env('APP_URL')}}/images/large/{{$img['id']}}">--}}
                                        {{--</figure>--}}
                                    {{--@endforeach--}}
                                @elseif($rec['type'] == 'video')
                                    @if($rec['video']['provider'] == 'youtube')
                                        <figure class="op-interactive">
                                            <iframe width="300" height="250" src="https://www.youtube.com/embed/{{$rec['video']['code']}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        </figure>
                                    @elseif($rec['video']['provider'] == 'vimeo')
                                        <figure class="op-interactive">
                                            <iframe src="https://player.vimeo.com/video/{{$rec['video']['code']}}" width="300" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                        </figure>
                                    @elseif($rec['video']['provider'] == 'dailymotion')
                                        <figure class="op-interactive">
                                            <iframe frameborder="0" width="300" height="250" src="//www.dailymotion.com/embed/video/{{$rec['video']['code']}}" allowfullscreen allow="autoplay"></iframe>
                                        </figure>
                                    @elseif($rec['video']['provider'] == 'facebook')
                                        <figure class="op-interactive">
                                            {{$rec['video']['code']}}
                                        </figure>
                                    @elseif($rec['video']['provider'] == 'twitter')
                                        <figure class="op-interactive">
                                            {{$rec['video']['code']}}
                                        </figure>
                                    @elseif($rec['video']['provider'] == 'unknown')
                                        <figure class="op-interactive">
                                            {{$rec['video']['code']}}
                                        </figure>
                                    @endif
                                @elseif($rec['type'] == 'iframe')
                                    <figure class="op-interactive">
                                        <?=htmlspecialchars_decode($rec['content'])?>
                                    </figure>
                                @elseif($rec['type'] == 'text')
                                    <P><?=htmlspecialchars_decode($rec['content'])?></P>
                                @elseif($rec['type'] == 'paragraph')
                                    <blockquote><p><?=htmlspecialchars_decode($rec['content'])?></p></blockquote>
                                @elseif($rec['type'] == 'exclamation')
                                    <blockquote><?=htmlspecialchars_decode($rec['content'])?></blockquote>
                                @endif
                            @else
                                <P><?=htmlspecialchars_decode($rec)?></P>
                            @endif

                        @endforeach
                        <footer>
                        </footer>
                    </article>
                    </body>
                    </html>
                    ]]>
                </content:encoded>
            </item>
        @endforeach


    </channel>
</rss>