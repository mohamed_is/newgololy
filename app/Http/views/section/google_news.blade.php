<?=
'<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL
?>
<rss version="2.0"
     xmlns:content="http://purl.org/rss/1.0/modules/content/"
     xmlns:media="http://search.yahoo.com/mrss/">
    <channel>
        <title>{{$info['page_title']}}</title>
        <link>{{$info['page_link']}}</link>
        <description>{{$info['page_description']}}</description>
        <language>ar-eg</language>
        <copyright>{{env('APP_URL')}}</copyright>
        <image><url>{{env('APP_URL')}}/assets/images/logo.png</url></image>
        <lastBuildDate>{{ $content['0']['created_at'] }}</lastBuildDate>
        @foreach($content as $item)
            <item>
                <title>{{ $item->title }}</title>
                <link>{{env('APP_URL')}}/{{$item->url}}</link>
                <description>
                    <?=html_entity_decode($item->description)?>
                </description>
                <content:encoded>
                    @foreach($item->content as $key=>$record)
                        <p><?=html_entity_decode(strip_tags($record), ENT_QUOTES)?></p>
                    @endforeach
                </content:encoded>
                <enclosure url="{{env('APP_URL')}}/images/750x450/{{$item->main_img}}" length="19500" type="image/jpeg"/>
                <image><url>{{env('APP_URL')}}/images/750x450/{{$item->main_img}}</url></image>
                <guid>{{env('APP_URL')}}/{{$item->url}}</guid>
                <pubDate>{{ $item->created_at->format(DateTime::ATOM) }}</pubDate>
                <author>{{ $item->written }}</author>
            </item>
        @endforeach
    </channel>
</rss>