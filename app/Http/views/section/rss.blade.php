<?=
'<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL
?>
<rss version="2.0">
    <channel>
        <title>{{$info['page_title']}}</title>
        <link>{{$info['page_link']}}</link>
        <description>{{$info['page_description']}}</description>
        <language>ar-eg</language>
        <copyright>{{env('APP_URL')}}</copyright>
        <image><url>{{env('APP_URL')}}/assets/images/logo.png</url></image>
        <lastBuildDate>{{ $content['0']['created_at'] }}</lastBuildDate>
        @foreach($content as $item)
            <item>
                <title><![CDATA[{{ $item->title }}]]></title>
                <link>{{env('APP_URL')}}/{{$item->url}}</link>
                <description>
                    <![CDATA[{{$item->description}}]]>
                </description>
                <image><url>{{env('APP_URL')}}/images/750x450/{{$item->main_img}}</url></image>
                <guid>{{env('APP_URL')}}/{{$item->url}}</guid>
                <pubDate>{{ $item->created_at->format(DateTime::ATOM) }}</pubDate>
            </item>
        @endforeach
    </channel>
</rss>