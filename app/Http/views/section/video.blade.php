@section('title', 'فيديو')
@section('description', 'فيديو' )
@section('keywords',  'فيديو')
@section('robots', 'index, follow')
@section('googlebot', 'index, follow')
@section('url', url()->current())
@section('image', 'https://www.elrsala.com/images/1000x600/')
@extends('layouts.app')
@section('content')
<style>
    .line-height{
        line-height: 1.7 !important;
    }
</style>
<div class="dasht-main-box">
    <section id="dasht-feat1-wrap" class="left relative margin-top-20">
        <div class="dasht-feat1-right-out left relative">
            @include('ads.home_responsive_1')
        </div>
    </section>
</div>
    <div class="dasht-main-box">
    <section id="dasht-feat1-wrap" class="left relative">
            <div class="dasht-feat1-right-out left relative" style="background-color: #000 !important;">
                @if(count($home1) > 4)

                <div class="dasht-widget-feat1-wrap left relative">
                    <div class="dasht-vid-wide-top left relative">
                        <div class="dasht-vid-wide-out left relative">
                            <div class="dasht-vid-wide-in">
                                <div class="dasht-vid-wide-left left relative">
                                    <div id="dasht-video-embed-wrap" class="left relative">
                                        <div id="dasht-video-embed-cont" class="left relative">
                                            <span class="dasht-video-close fa fa-times" aria-hidden="true" style="display: none;"></span>
                                            <div id="dasht-video-embed" class="left relative">
                                                <img style="width: 100%;border: 1px solid #616161;" src="{{env('APP_URL')}}/images/590x354/{{$home1['0']['content']['main_img']}}" class="attachment-dasht-port-thumb" alt="{{$home1['0']['content']['main_img_description']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dasht-post-img-hide" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
                                        <meta itemprop="url" content="assets/uploads/walking-dead.jpg">
                                        <meta itemprop="width" content="1000">
                                        <meta itemprop="height" content="600">
                                    </div>
                                </div>
                            </div>
                            <div class="dasht-vid-wide-right left relative">
                                <div class="dasht-vid-wide-text left relative">
                                    <div class="dasht-cat-date-wrap left relative">
                                        <a class="dasht-post-cat-link" href="#">
                                            <span class="dasht-cd-cat left relative">{{$home1['0']['section']['name']}}</span>
                                        </a>
                                        <a></a>
                                    </div>
                                    <a href="{{ (new App\Http\Helpers\Helpers)->generate_url('news',$home1['0']['content']['_id'],$home1['0']['section']['name'],$home1['0']['content']['title'])}}" rel="bookmark" title="{{$home1['0']['content']['title']}}">
                                        <h1 class="dasht-post-title dasht-vid-wide-title left entry-title" itemprop="headline">{{$home1['0']['content']['title']}}</h1>
                                        <span class="dasht-post-excerpt left">
                                        <p>{{$home1['0']['content']['description']}}</p>
                                    </span>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="dasht-vid-wide-bot left relative">
                        <h4 class="dasht-sec-head">
                            <span class="dasht-sec-head" style="color: #fff">شاهد ايضاء</span>
                            <span class="dasht-widget-head-link"></span>
                        </h4>
                        <div class="dasht-vid-wide-more-wrap left relative">
                            <ul class="dasht-vid-wide-more-list left relative">
                                @if(count($home1) > 1)
                                    @foreach ($home1 as $key=>$record)
                                        <?php if ($key < 1) {continue;} ?>
                                        <a href="{{ (new App\Http\Helpers\Helpers)->generate_url('news',$home1[$key]['content']['_id'],$home1[$key]['section']['name'],$home1[$key]['content']['title'])}}" rel="bookmark" title="{{$home1[$key]['content']['title']}}">
                                            <li>
                                                <div class="dasht-vid-wide-more-img left relative">
                                                    <img style="width: 100%;height: 100%;border: 1px solid #616161;" src="{{env('APP_URL')}}/images/400x240/{{$home1[$key]['content']['main_img']}}" class="attachment-dasht-port-thumb" alt="{{$record->main_img_description}}">

                                                    <div class="dasht-vid-box-wrap dasht-vid-marg">
                                                        <i class="fa fa-2 fa-play" aria-hidden="true"></i>
                                                    </div>
                                                </div>

                                                <div class="dasht-vid-wide-more-text left relative">
                                                    <div class="dasht-cat-date-wrap left relative">
                                                        <span class="dasht-cd-date left relative">
                                                            {{ (new App\Http\Helpers\Helpers)->datetime($home1[$key]['content']['created_at'])}}
                                                        </span>
                                                    </div>
                                                    <p>{{$home1[$key]['content']['title']}}</p>
                                                </div>
                                            </li>
                                        </a>
                                    @endforeach

                                @endif
                            </ul>
                        </div>
                    </div>

                </div>
                @endif

            </div>
        </section>
    </div>
    @if(count($home1) > 5)
        <div class="dasht-main-box">
            <section id="dasht-feat1-wrap" class="left relative margin-top-20">
                <div class="dasht-feat1-right-out left relative">
                    @include('ads.home_responsive_1')
                </div>
            </section>
        </div>
    @endif
    <div class="dasht-main-box">
        <section id="dasht-feat1-wrap" class="left relative">
            <div class="dasht-feat1-right-out left relative">

                <div class="dasht-widget-feat1-wrap left relative">
                    <div class="dasht-widget-feat1-cont left relative" id="datax">

                            @foreach($content as $key=> $record)
                            <a href="{{ (new App\Http\Helpers\Helpers)->generate_url('news',$record->_id,$record['section_name']['name'],$record->title)}}" rel="bookmark">
                                <div class="dasht-widget-feat3-bot-story left relative">
                                    <div class="dasht-widget-feat1-bot-img left relative">
                                        <img width="400" height="240" src="{{env('APP_URL')}}/images/400x240/{{$record->main_img}}" >
                                        @if($record->type == 'video')
                                            <div class="dasht-vid-box-wrap dasht-vid-box-mid dasht-vid-marg">
                                                <i class="fa fa-2 fa-play" aria-hidden="true"></i>
                                            </div>
                                        @elseif($record->type == 'album')
                                            <div class="dasht-vid-box-wrap dasht-vid-box-mid dasht-vid-marg">
                                                <i class="fa fa-2 fa-camera" aria-hidden="true"></i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="dasht-widget-feat1-bot-text left relative">
                                        <div class="dasht-cat-date-wrap left relative">
                                            <span class="dasht-cd-date left relative">{{ (new App\Http\Helpers\Helpers)->datetime($record->created_at)}}</span>
                                        </div>
                                        <h3 class="line-height">{{$record->title}}</h3>
                                    </div>
                                </div>
                            </a>
                            @if($key == '3')
                                <div class="dasht-widget-feat3-bot-story left relative" style="text-align: center;">
                                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                    <ins class="adsbygoogle"
                                         style="display:block"
                                         data-ad-format="fluid"
                                         data-ad-layout-key="-6t+ed+2i-1n-4w"
                                         data-ad-client="ca-pub-8191525600868930"
                                         data-ad-slot="9956737800"></ins>
                                    <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                    </script>
                                </div>
                            @endif
                            @endforeach
                        </div>
                        <div id="loading" class="ajax-load text-center hide">
                            <p><img src="/assets/images/loading.gif?5"></p>
                        </div>


                    </div>

                </div>
        </section>
    </div>
<script type="text/javascript">

    $('#dasht-content-main').removeClass('read-more');
    $('#dasht-read-more').addClass('hide');
    $('#dasht-comments-button').removeClass('hide');

    var page = 1;
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height()) {
            page++;

            loadMoreData(page);
        }
    });


    function loadMoreData(page){
        $.ajax(
            {
                url: '?page=' + page,
                type: "get",
                beforeSend: function()
                {
                    $('#loading').removeClass('hide');
                }
            })
            .done(function(data)
            {

                if(data.html == " "){
                    $('#loading').html("لا توجد اخبار");
                    return;
                }
                $('#loading').addClass('hide');

                $('#datax').append(data);
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                alert('حدث خطاء');
            });
    }

</script>
@endsection
