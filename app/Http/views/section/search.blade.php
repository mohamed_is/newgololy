@section('title', $tags)
@section('description', $tags)
@section('keywords', $tags)
@section('robots', 'index, follow')
@section('googlebot', 'index, follow')
@section('ads', 'on')
@section('url', url()->current())
@section('image', 'https://www.gololy.com/images/750x450/')
@extends('layouts.app')
@section('content')


    <div class="container" id="app-box" data-src="section" section-name="search" key-word="{{ $tags }}">
        <div class="ads">
            <?php include(public_path().'/ad_file/5e1b2ae229cc0f069520b273.php')?>
        </div>
        <div class="row cont">
            @foreach($content as $record)
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="news-card news-card--card section-theme"><a class="news-card__link" href="/{{$record['url']}}"></a>
                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['main_img']}}"/>
                            @foreach($record['sectionData'] as $sec)
                            <a class="news-card__category" href="{{env('APP_URL')}}/{{$sec['url']}}">
                                <p>{{$sec['name']}}</p>
                            </a>
                            @endforeach
                            <div class="news-card__views">
                                <p>88</p><span class="icon">
                    <svg id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                      <g>
                        <g>
                          <path d="M508.745,246.041c-4.574-6.257-113.557-153.206-252.748-153.206S7.818,239.784,3.249,246.035    c-4.332,5.936-4.332,13.987,0,19.923c4.569,6.257,113.557,153.206,252.748,153.206s248.174-146.95,252.748-153.201    C513.083,260.028,513.083,251.971,508.745,246.041z M255.997,385.406c-102.529,0-191.33-97.533-217.617-129.418    c26.253-31.913,114.868-129.395,217.617-129.395c102.524,0,191.319,97.516,217.617,129.418    C447.361,287.923,358.746,385.406,255.997,385.406z"></path>
                        </g>
                      </g>
                      <g>
                        <g>
                          <path d="M255.997,154.725c-55.842,0-101.275,45.433-101.275,101.275s45.433,101.275,101.275,101.275    s101.275-45.433,101.275-101.275S311.839,154.725,255.997,154.725z M255.997,323.516c-37.23,0-67.516-30.287-67.516-67.516    s30.287-67.516,67.516-67.516s67.516,30.287,67.516,67.516S293.227,323.516,255.997,323.516z"></path>
                        </g>
                      </g>
                    </svg></span>
                            </div>
                        </div>
                        <div class="news-card__caption">
                            <div class="news-card__details">
                                <div class="news-card__caption-row"><span class="date">{{ (new App\Http\Helpers\Helpers)->date($record['publication_date'])}}</span>
                                </div>
                                <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <a class="btn btn--primary" id="loadmore-cards"> عرض المزيد</a>
    </div>








    {{--    <div class="full-width-header-content">--}}
    {{--        <div class="article-grid-container">--}}
    {{--        <section class="container-fluid">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-sm-12 article-container section-title">--}}
    {{--                    <h1>{{$tags}}</h1>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div id="contentsWrapper">--}}
    {{--                <div class="content" data-title="{{$next_url}}" data-text="{{$tags}}">--}}
    {{--                <div class="row article-grid-container">--}}
    {{--                    @foreach($content as $record)--}}
    {{--                        <div class="col-sm-6 col-lg-3 col-md-3 article-container small-card">--}}
    {{--                            <a href="/{{$record['url']}}" class="article">--}}
    {{--                                <div class="image-container">--}}
    {{--                                    @if(strpos($record['main_img'], 'photo') !== false)--}}
    {{--                                        <img src="{{$record['main_img']}}" title="{{$record['img_description']}}">--}}
    {{--                                    @else--}}
    {{--                                    <img class="lazyload" src="/assets/images/lazyload.jpg" data-src="{{env('APP_URL')}}/images/450x250/{{$record['main_img']}}"  alt="{{$record['img_description']}}" title="{{$record['img_description']}}"  style="width:auto;">--}}
    {{--                                    @endif--}}
    {{--                                </div>--}}
    {{--                                <div class="article-panel">--}}
    {{--                                    <h3>{{$record['title']}}</h3>--}}
    {{--                                    <span class="date detail">{{ (new App\Http\Helpers\Helpers)->date($record['publication_date'])}}</span>--}}
    {{--                                </div>--}}
    {{--                            </a>--}}
    {{--                        </div>--}}



    {{--                    @endforeach--}}
    {{--                </div>--}}
    {{--                    <a href="{{$current_url}}" id="current" ></a>--}}
    {{--                    @if(!empty($next_url))--}}
    {{--                        <a href="{{$next_url}}" class="jscroll-next"></a>--}}
    {{--                    @endif--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <section class="col-sm-12 container-ads">--}}
    {{--                <?php include(public_path().'/ad_file/5e1b34e229cc0f23e90e2bf4.php')?>--}}
    {{--            </section>--}}
    {{--        </section>--}}
    {{--        </div>--}}
    {{--    </div>--}}

@endsection
