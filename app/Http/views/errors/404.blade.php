@section('title', '404')
@section('description', 'أهل مصر صحافة كل يوم احدث الاخبار لحظة بلحظة')
@section('keywords', '')
@section('robots', 'noindex, nofollow')
@section('googlebot', 'noindex, nofollow')
@section('url', 'https://www.ahlmasrnews.com/page/error/404')
@section('image', 'https://www.ahlmasrnews.com/page/error/404')
@extends('layouts.app')
@section('content')
    <div class="full-width-header-content " style="margin-top: 100px;">
        <section class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12 col-md-12">

                <h1 class="m-0" style="text-align: center;font-size: 100pt;">404</h1>
                <h6  style="text-align: center;font-size: 25pt;">هذه الصفحة غير متاحة الان</h6>
                </div>
        </div>
    </section>
    </div>

</body>
</html>
@endsection
