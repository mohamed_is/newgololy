<section class="container-ads">
    <picture>
        <source media="(min-width: 1000px)" srcset="/ads/728-x-90-1.jpg">
        <source media="(max-width: 465px)" srcset="/ads/300-x-250-1.jpg">
        <img src="/ads/728-x-90-1.jpg">
    </picture>
</section>