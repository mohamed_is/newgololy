<!DOCTYPE html>
<html lang="ar-EG">
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>@yield('title')</title>
    <meta name="yandex-verification" content="bd493ff09ff56f73" />
    <link rel="canonical" href="@yield('url')" />
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="robots" content="@yield('robots')">
    <meta name="googlebot" content="@yield('googlebot')" />
    <meta property="og:url" content="@yield('url')" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('description')" />
    <meta property="og:image" content="@yield('image')" />
    <meta property="fb:app_id" content="538318339942983" />
    <meta name='twitter:app:country' content='EG'>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@website" />
    <meta name="twitter:creator" content="@website" />
    <meta name="twitter:title" content="@yield('title')" />
    <meta name="twitter:url" content="@yield('url')" />
    <meta name="twitter:description" id="TwitterDesc" content="@yield('description')" />
    <meta name="twitter:image" id="TwitterImg" content="@yield('image')" />
    <meta property="fb:pages" content="735616803279479" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet" />
    <link href="/assets/css/style.css?<?=time()?>" rel="stylesheet" />
<script type="application/ld+json">
    {
    "@context": "https://schema.org",
    "@type": "WebSite",
    "url": "https://www.baladnaelyoum.com/",
    "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.baladnaelyoum.com/search/{search_term_string}",
        "query-input": "required name=search_term_string"
        }
    }
    {"@context": "https://schema.org",
    "@type": "WebSite",
    "name": "بلدنا اليوم",
    "url": "https://www.baladnaelyoum.com/",
    "sameAs": ["https://www.facebook.com/elyoom","https://twitter.com/BaladnaElyoom","https://www.youtube.com/channel/UCwG4xI_6FEiUk939wVsJnmw"]
    }
    {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "url": "https://www.baladnaelyoum.com/",
        "logo": "https://www.baladnaelyoum.com/assets/images/logo.png"
    }
</script>

</head>
<body class="grey-background home">
<header class='site-header navigation-hidden'>
    <div class="header-wrap headerbg-center" style='background-image:none'>
        <div class="overlay"></div>
        <div class="part first">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6 col-sm-2">
                        <div class="vertical-center first">
                            <button type="button" class="btn navigation-toggle" data-removeclass="search-active" data-removeclass-target=".site-header" data-toggleclass="navigation-active" data-toggleclass-target=".site-header">
                                <span class="on-closed">
                                    <span class="icon-Menu">
                                        <span class="sr-only">Open navigation</span>
                                    </span>
                                </span>
                                <span class="on-open">
                                    <span class="icon-Close">
                                        <span class="sr-only">Close navigation</span>
                                    </span>
                                </span>
                            </button>
                            <div class="logo-newspaper">
                                <a href="/" title="الرئيسية">
                                    <span class="sr-only"> اهل مصر - الرئيسية</span>
                                </a>
                            </div>
                            <h1 class="hidden">اهل مصر</h1>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-10">
                        <div class="partners last top-menu">
                            <div class="vertical-center">
                                <div class="col-xs-3 col-sm-3" id="owner"><span class="title">المشرف العام على التحرير</span> <span class="name">داليا عماد</span></div>
                                <div class="col-xs-9 col-sm-9" style="text-align: center">
                                    <div id="top-sm">
                                        <ul>
                                            <li><a href="#"><span class="icon-Facebook" aria-hidden="true"></span> <span class="sr-only">Facebook</span> </a> </li>
                                            <li><a href="#"><span class="icon-Twitter" aria-hidden="true"></span> <span class="sr-only">Twitter</span></a></li>
                                            <li><a href="#"><span class="icon-Youtube" aria-hidden="true"></span> <span class="sr-only">YouTube</span></a></li>
                                            <li><a href="#"><span class="icon-Instagram" aria-hidden="true"></span> <span class="sr-only">Instagram</span></a></li>
                                            <li><a href="#"><span class="icon-Search" aria-hidden="true"></span> <span class="sr-only">بحث</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="partners last">
                            <div class="vertical-center">
                                <div id="menu">
                                    <ul>
                                        <li><a href="/"><span class="icon-Home"></span></a></li>
                                        <li><a href="/news">اخبار مصر</a></li>
                                        <li><a href="#">كل الاخبار</a></li>
                                        <li><a href="#">رياضة</a></li>
                                        <li><a href="#">حوادث</a></li>
                                        <li><a href="#">محافظات</a></li>
                                        <li><a href="#">فن</a></li>
                                        <li><a href="#">اخبار عالمية</a></li>
                                        <li><a href="#">اقتصاد</a></li>
                                        <li><a href="#">سيارات</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="part second">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-8 col-sm-9">
                        <nav class="first" aria-label="Main navigation">
                            <ul class="loading">
                                <li class="match-height home-mobile-nav-link">
                                    <a href="/" title="Link to Homepage">Home</a>
                                </li>
                                <li class='match-height'>

                                    <a href="/tickets/" data-dropdown-trigger aria-haspopup='true' title="Link to Tickets">Tickets <span class='icon-Down-Arrow'></span></a>

                                    <ul class="dropdown-menu" data-dropdown-menu aria-hidden="true" aria-label="submenu">
                                        <li class='duplicated-link'>
                                            <a href="/tickets/" class="duplicate-link" title="Link to Tickets">Tickets </a>
                                        </li>

                                        <li>

                                            <a href="https://tickets.burnleyfc.com/PagesPublic/Home/Home.aspx" target='_blank' title="Link to Buy Tickets Online">Buy Tickets Online </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/ticket-news/" title="Link to Ticket News">Ticket News </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/season-tickets/" title="Link to Season Tickets">Season Tickets </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/home-tickets/" title="Link to Home Tickets">Home Tickets </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/away-tickets/" title="Link to Away Tickets">Away Tickets </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/matchday-hospitality/" title="Link to Matchday Hospitality">Matchday Hospitality </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/ticket-office-opening-hours/" title="Link to Ticket Office Opening Hours">Ticket Office Opening Hours </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/group-tickets/" title="Link to Group Tickets">Group Tickets </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/loyalty-points/" title="Link to Loyalty Points">Loyalty Points </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/team-card/" title="Link to Your Team Card">Your Team Card </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/first-time-fan/" title="Link to First Time Fan">First Time Fan </a>

                                        </li>
                                        <li>

                                            <a href="/tickets/stadium-seating-plan/" title="Link to Stadium Seating Plan">Stadium Seating Plan </a>

                                        </li>
                                        <li>

                                            <a href="/fans/supporters-guides/" title="Link to Supporters Guides">Supporters Guides </a>

                                        </li>

                                        <li>
                                            <button class="btn back"><span class="icon-Back-Arrow"></span> Back</button>
                                        </li>
                                    </ul>
                                </li>
                                <li class='match-height'>

                                    <a href="https://shop.burnleyfc.com/" target='_blank' data-dropdown-trigger aria-haspopup='true' title="Link to Shop">Shop <span class='icon-Down-Arrow'></span></a>

                                    <ul class="dropdown-menu" data-dropdown-menu aria-hidden="true" aria-label="submenu">
                                        <li class='duplicated-link'>
                                            <a href="https://shop.burnleyfc.com/" target='_blank' class="duplicate-link" title="Link to Shop">Shop </a>
                                        </li>

                                        <li>

                                            <a href="https://shop.burnleyfc.com/" target='_blank' title="Link to Shop Online">Shop Online </a>

                                        </li>
                                        <li>

                                            <a href="/shop/shops-news/" title="Link to Shop News">Shop News </a>

                                        </li>
                                        <li>

                                            <a href="/shop/locations/" title="Link to Locations">Locations </a>

                                        </li>
                                        <li>

                                            <a href="/shop/opening-times/" title="Link to Opening Times">Opening Times </a>

                                        </li>

                                        <li>
                                            <button class="btn back"><span class="icon-Back-Arrow"></span> Back</button>
                                        </li>
                                    </ul>
                                </li>
                                <li class='match-height'>
                                    <a href="/news/" data-dropdown-trigger aria-haspopup='true' title="Link to News">News <span class='icon-Down-Arrow'></span></a>
                                    <ul class="dropdown-menu" data-dropdown-menu aria-hidden="true" aria-label="submenu">
                                        <li class='duplicated-link'>
                                            <a href="/news/" class="duplicate-link" title="Link to News">News </a>
                                        </li>
                                        <li>
                                            <a href="http://login.burnleyfc.com/" title="Link to Newsletter Sign Up">Newsletter Sign Up </a>
                                        </li>
                                        <li>

                                            <a href="/news/" title="Link to Latest News">Latest News </a>

                                        </li>
                                        <li>

                                            <a href="/news/the-gaffer/" title="Link to The Gaffer">The Gaffer </a>

                                        </li>
                                        <li>

                                            <a href="/news/the-boardroom/" title="Link to The Boardroom">The Boardroom </a>

                                        </li>
                                        <li>

                                            <a href="/news/ticketing-news/" title="Link to Ticketing News">Ticketing News </a>

                                        </li>
                                        <li>

                                            <a href="/news/academy-news/" title="Link to Academy News">Academy News </a>

                                        </li>
                                        <li>

                                            <a href="/news/commercial-news/" title="Link to Commercial News">Commercial News </a>

                                        </li>
                                        <li>

                                            <a href="/news/community-news/" title="Link to Community News">Community News </a>

                                        </li>
                                        <li>

                                            <a href="/news/lottery-news/" title="Link to Lottery News">Lottery News </a>

                                        </li>
                                        <li>

                                            <a href="http://claretslotteries.co.uk/" target='_blank' title="Link to Lottery Results">Lottery Results </a>

                                        </li>
                                        <li>

                                            <a href="/news/news-archive/" title="Link to News Archive">News Archive </a>

                                        </li>

                                        <li>
                                            <button class="btn back"><span class="icon-Back-Arrow"></span> Back</button>
                                        </li>
                                    </ul>
                                </li>
                                <li class='match-height'>

                                    <a href="/matches/" data-dropdown-trigger aria-haspopup='true' title="Link to Matches">Matches <span class='icon-Down-Arrow'></span></a>

                                    <ul class="dropdown-menu" data-dropdown-menu aria-hidden="true" aria-label="submenu">
                                        <li class='duplicated-link'>
                                            <a href="/matches/" class="duplicate-link" title="Link to Matches">Matches </a>
                                        </li>

                                        <li>

                                            <a href="/matches/fixtures/" title="Link to Fixtures">Fixtures </a>

                                        </li>
                                        <li>

                                            <a href="/matches/results/" title="Link to Results">Results </a>

                                        </li>
                                        <li>

                                            <a href="/matches/league-table/" title="Link to League Table">League Table </a>

                                        </li>
                                        <li>

                                            <a href="/matches/u23s/" title="Link to U23's Fixtures">U23's Fixtures </a>

                                        </li>
                                        <li>

                                            <a href="/matches/u23-results/" title="Link to U23's Results">U23's Results </a>

                                        </li>
                                        <li>

                                            <a href="/matches/U23s-league-table/" title="Link to U23's League Table">U23's League Table </a>

                                        </li>
                                        <li>

                                            <a href="/matches/u18s-fixtures/" title="Link to U18's Fixtures">U18's Fixtures </a>

                                        </li>
                                        <li>

                                            <a href="/matches/u18s-results/" title="Link to U18's Results">U18's Results </a>

                                        </li>
                                        <li>

                                            <a href="/matches/u18s-league-table/" title="Link to U18's League Table">U18's League Table </a>

                                        </li>

                                        <li>
                                            <button class="btn back"><span class="icon-Back-Arrow"></span> Back</button>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                    </div>
                    <div class="col-xs-4 col-sm-3">
                        <form class="search" action="/search/" role="search" aria-hidden="true" method="get">
                            <div class="container-fluid">
                                <label for="search-text" class="sr-only">Search text</label>
                                <input type="text" id="search-text" placeholder="Search" name="q">
                                <div class="buttons">
                                    <button type="button" class="btn close" data-removeclass="search-active" data-removeclass-target=".site-header" data-togglearia-target=".search">
                                        <span class="icon-Close"><span class="sr-only">Close</span></span>
                                    </button>
                                    <button type="submit" class="btn">
                                        <span class="icon-Search"><span class="sr-only">بحث</span></span>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <nav class="pull-right last" aria-label="Account navigation">
                            <ul>
                                <li>
                                    <button type="button" class="btn search-link" data-addclass="search-active" data-addclass-target=".site-header" data-togglearia-target=".search" aria-expanded="false" aria-haspopup="true" data-setfocus-target="#search-text">
                                        <span class="text">Search</span><span class="icon-Search"><span class="sr-only">بحث</span></span>
                                    </button>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('content')
<footer class="bg-primary bg-image site-footer">
    <div class="footer-links">
        <div class="container-fluid">
            <div class="row">
                <nav class="footer-social col-sm-4" aria-label="Social links">
                    <ul>
                        <li><a href="#"><span class="icon-Facebook" aria-hidden="true"></span> <span class="sr-only">Facebook</span> </a> </li>
                        <li><a href="#"><span class="icon-Twitter" aria-hidden="true"></span> <span class="sr-only">Twitter</span></a></li>
                        <li><a href="#"><span class="icon-Youtube" aria-hidden="true"></span> <span class="sr-only">YouTube</span></a></li>
                        <li><a href="#"><span class="icon-Instagram" aria-hidden="true"></span> <span class="sr-only">Instagram</span></a></li>
                    </ul>
                </nav>
                <nav class="footer-nav col-sm-8" aria-label="Footer navigation">
                    <ul>
                        <li class="match-height"><a href="#">شروط النشر</a></li>
                        <li class="match-height"><a href="#">من نحن</a></li>
                        <li class="match-height"><a href="#">الاعلانات</a></li>
                        <li class="match-height"><a href="#">اتصل بنا</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="/assets/js/vendor.js?<?=time()?>"></script>
<script src="/assets/js/header.js?<?=time()?>"></script>
<script src="/assets/js/NewsArticle.js?<?=time()?>"></script>
</body>
</html>
