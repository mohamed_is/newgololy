<section class="m-top-30">
    <div class="custom-row">
        <div class="custom-content">
            <div class="box">
                <div class="head">
                    <div></div>
                    <h5>تريند</h5>
                </div>
                <div class="row">
                    @foreach($trend as $record)

                        <div class="col-md-3 col-lg-3 col-sm-12">
                            <a href="{{(new App\Http\Helpers\Helpers)->generate_url('tags',$record->title,'0',$record->title)}}" title="{{$record->title}}" rel="tag">

                            <div class="featured-cols small_post">
                                <div class="featured-bg-gradient">&nbsp;</div>

                                <img alt="{{$record->description}}" src="{{env('APP_URL')}}/images/455x256/{{$record->img}}" class="avatar photo" width="50" height="50" title="{{$record->title}}">
                                <div class="featured-desc large">
                                </div>
                            </div>
                            <h2>{{str_limit($record->title, 50)}}</h2>
                            <p style="color: #909090;">{{str_limit($record->description, 30)}}</p>
                            </a>

                        </div>


                    @endforeach
                </div>
            </div>
        </div>
        <div class="custom-aside">
            <div class="box">
                @include('ads.home_box_1')
            </div>
        </div>
    </div>
</section>



