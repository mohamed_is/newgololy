    <li class="header__main-list-item"><a href="/">الرئيسية</a></li>
    <li class="header__main-list-item"><a href="/news">اخر الاخبار</a></li>
    @foreach($menu as $record)
        <li class="header__main-list-item"><a href="/{{$record->url}}">{{$record->name}}</a></li>
    @endforeach
