
<div class="header-cover">
    <div class="header-cover__content">
        <div class="container">
            <div class="main-title">
                <div class="main-title__box">
                    <h1>لا يفوتك</h1>
                </div>
            </div>
            <div class="row">
                @foreach($hits as $key=>$record)
                    @if($key > 4)
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="news-card news-card--card top-cover"><a class="news-card__link" href="/{{$record['url']}}"></a>
                        <div class="news-card__img"><img src="{{env('APP_URL')}}/images/450x250/{{$record['main_img']}}"/>
                        </div>
                        <div class="news-card__caption">
                            <div class="news-card__details">
                                <div class="news-card__caption-row"><a class="cat" href="#">{{$record->sectionData[0]['name']}}</a><span class="or">|</span><span class="date">{{ (new App\Http\Helpers\Helpers)->datetime($record['publication_date'])}}</span>
                                </div>
                                <h4 class="news-card__title">{{html_entity_decode($record['title'])}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
