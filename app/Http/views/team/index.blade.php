@extends('layouts.app')
@section('content')
<style>
    .dasht-authors-list-wrap {
        margin-bottom: 40px;
        border-left: 0px solid #ddd;
    }
</style>
<div class="dasht-feat1-list-ad">
    <section id="dasht-feat1-wrap" class="left relative">
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-5624808021413910"
             data-ad-slot="4487744159"
             data-ad-format="auto"
             data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </section>
</div>
<div class="dasht-main-box">
    <section id="dasht-feat1-wrap" class="left relative">
        <div class="dasht-feat1-right-out left relative">
            <div class="dasht-feat1-right-in">
                <div class="dasht-widget-feat2-main left relative">

                    <h4 class="dasht-sec-head">
                        <span class="dasht-sec-head">فريق التحرير</span>
                        <span class="dasht-widget-head-link"></span>
                    </h4>


                    <div class="dasht-tags-wrap relative row" style="margin-bottom: 50px;">
                        <div class="dasht-authors-wrap left relative">
                            <section class="dasht-authors-list-wrap relative" style="margin: auto !important;">
                                <div class="dasht-authors-list-img left relative">
                                    <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                </div>
                                <span class="dasht-authors-name">حسن سيد على</span><BR>
                                <h2 class="dasht-authors-latest">رئيس مجلس الإدارة</h2>
                            </section>
                        </div>
                    </div>
                    <div class="dasht-tags-wrap relative row" style="margin-bottom: 50px;">
                        <div class="dasht-authors-wrap left relative row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">وائل الجندي</span><BR>
                                    <h2 class="dasht-authors-latest">مدير عام التحرير</h2>
                                </section>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">عبدالرحمن شلبي</span><BR>
                                    <h2 class="dasht-authors-latest">رئيس التحرير التنفيذي</h2>
                                </section>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">وليد الغمري</span><BR>
                                    <h2 class="dasht-authors-latest">رئيس التحرير</h2>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="dasht-tags-wrap relative row" style="margin-bottom: 50px;">
                        <div class="dasht-authors-wrap left relative row">

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name"> محمد الصاوي</span><BR>
                                    <h2 class="dasht-authors-latest">مدير غرفة الأخبار</h2>
                                </section>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">محمد رجب</span><BR>
                                    <h2 class="dasht-authors-latest">مدير التحرير</h2>
                                </section>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">تامر فاروق</span><BR>
                                    <h2 class="dasht-authors-latest">مساعد رئيس التحرير </h2>
                                </section>
                            </div>

                        </div>
                    </div>
                    <div class="dasht-tags-wrap relative row" style="margin-bottom: 50px;">
                        <div class="dasht-authors-wrap left relative row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">وائل عبدالعزيز</span><BR>
                                    <h2 class="dasht-authors-latest">رئيس قسم الأخبار</h2>
                                </section>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">خالد حسين</span><BR>
                                    <h2 class="dasht-authors-latest">رئيس قسم الديسك</h2>
                                </section>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">سعيد نصر</span><BR>
                                    <h2 class="dasht-authors-latest">الديسك المركزي</h2>
                                </section>
                            </div>

                        </div>
                    </div>
                    <div class="dasht-tags-wrap relative row" style="margin-bottom: 50px;">
                        <div class="dasht-authors-wrap left relative row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name"> طارق الجندي</span><BR>
                                    <h2 class="dasht-authors-latest"> رئيس قسم الحوادث</h2>
                                </section>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">أسامة قديس</span><BR>
                                    <h2 class="dasht-authors-latest">رئيس قسم الرياضة</h2>
                                </section>
                            </div>


                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name"> رحاب فوزي</span><BR>
                                    <h2 class="dasht-authors-latest"> رئيس قسم الفن</h2>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="dasht-tags-wrap relative row" style="margin-bottom: 50px;">
                        <div class="dasht-authors-wrap left relative row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">سالي الجندي</span><BR>
                                    <h2 class="dasht-authors-latest"> رئيس قسم لايف ستايل</h2>
                                </section>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">محمود على</span><BR>
                                    <h2 class="dasht-authors-latest">رئيس قسم المحافظات</h2>
                                </section>
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name"> محمد فوزي</span><BR>
                                    <h2 class="dasht-authors-latest">رئيس قسم الخارجي</h2>
                                </section>
                            </div>

                        </div>
                    </div>

                    <div class="dasht-tags-wrap relative row" style="margin-bottom: 50px;">
                        <div class="dasht-authors-wrap left relative row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                                <section class="dasht-authors-list-wrap relative" style="margin: auto !important;width: 100%;">
                                    <div class="dasht-authors-list-img left relative">
                                        <img alt="" src="/assets/images/blankavatar.jpg" class="avatar avatar-150 photo" width="150" height="150">
                                    </div>
                                    <span class="dasht-authors-name">ناصر هيكل</span><BR>
                                    <h2 class="dasht-authors-latest"> مسؤول كتاب الرأي </h2>
                                </section>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div class="dasht-feat1-right-wrap left relative">
                <div class="dasht-feat1-list-ad left relative">
                    <span class="dasht-ad-label">"إعلان</span>
                    <!-- الرسالة مربع -->
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:300px;height:250px"
                         data-ad-client="ca-pub-5624808021413910"
                         data-ad-slot="9783646324"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
                <div class="dasht-feat1-list-wrap left relative" style="margin-top: 20px;">


                </div>
            </div>
        </div>
    </section>
</div>
<script language="javascript">
    $( window ).on( "load", function() {
    var w = $( window ).width();
    alert(w);
    })
</script>
@endsection
