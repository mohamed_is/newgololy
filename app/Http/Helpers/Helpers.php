<?php
/**
 * Created by PhpStorm.
 * User: fathallah
 * Date: 12/24/17
 * Time: 6:24 PM
 */
namespace App\Http\Helpers;
use App\Http\Models\User_role_user;
use Auth;
use Session;
use Carbon\Carbon;
use DateTime;
class Helpers
{
    public static function htmlDiff($old, $new)
    {
        $ret = '';
        $diff = Helpers::diff(preg_split("/[\s]+/", $old), preg_split("/[\s]+/", $new));
        foreach ($diff as $k)
        {
            if (is_array($k))
            {
                $ret .= (!empty($k['d'])?"<del style="."color:#ff0000".">".implode(' ',$k['d'])."</del> ":'').(!empty($k['i'])?"<ins style="."color:#009933".">".implode(' ',$k['i'])."</ins> ":'');
            }
            else
            {
                $ret .= $k . ' ';
            }
        }
        return $ret;
    }
    public static function diff($old, $new)
    {
        $matrix = array();
        $maxlen = 0;
        foreach($old as $oindex => $ovalue)
        {
            $nkeys = array_keys($new, $ovalue);
            foreach ($nkeys as $nindex)
            {
                $matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ? $matrix[$oindex - 1][$nindex - 1] + 1 : 1;
                if ($matrix[$oindex][$nindex] > $maxlen)
                {
                    $maxlen = $matrix[$oindex][$nindex];
                    $omax = $oindex + 1 - $maxlen;
                    $nmax = $nindex + 1 - $maxlen;
                }
            }
        }
        if ($maxlen == 0) return array(array('d'=>$old, 'i'=>$new));
        return array_merge(
            Helpers::diff(array_slice($old, 0, $omax), array_slice($new, 0, $nmax)),
            array_slice($new, $nmax, $maxlen),
            Helpers::diff(array_slice($old, $omax + $maxlen), array_slice($new, $nmax + $maxlen)));
    }
    public static function mobiledetect()
    {
        if(isset($_SERVER['HTTP_USER_AGENT'])){
            $device = $_SERVER['HTTP_USER_AGENT'];
        }else{
            $device = 'Desktop';
        }

        if(strpos($device, 'Mobile') !== false){
            return 'Mobile';
        } else{
            return 'Desktop';
        }



    }
    public static function datetime($datetime, $full = false)
    {
        $now = new DateTime();
        $ago = new DateTime($datetime);
//        return $ago;
        $diff = $now->diff($ago);
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        $string = array(
            'y' => 'سنة',
            'm' => 'شهر',
            'w' => 'اسبوع',
            'd' => 'يوم',
            'h' => 'ساعة',
            'i' => 'دقيقة',
            's' => 'ثانية',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
            } else {
                unset($string[$k]);
            }
        }
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? 'منذ ' . implode(', ', $string) . ' ' : 'الان';
    }
    public static function date($date){
        $new_date = Helpers::arabic_date(date_format($date,"d M Y | H:i"));
        $week =  Helpers::week_date(date_format($date,"D"));
        $date = $week.' '.$new_date;
        return $date;
    }
    public static function arabic_date($date){
        $string = array(
            'jan' => 'يناير',
            'Jan' => 'يناير',
            'feb' => 'فبراير',
            'Feb' => 'فبراير',
            'mar' => 'مارس',
            'Mar' => 'مارس',
            'apr' => 'ابريل',
            'Apr' => 'ابريل',
            'may' => 'مايو',
            'May' => 'مايو',
            'jun' => 'يونية',
            'Jun' => 'يونية',
            'jul' => 'يوليو',
            'Jul' => 'يوليو',
            'aug' => 'اغسطس',
            'Aug' => 'اغسطس',
            'sep' => 'سبتمبر',
            'Sep' => 'سبتمبر',
            'oct' => 'أكتوبر',
            'Oct' => 'أكتوبر',
            'nov' => 'نوفمبر',
            'Nov' => 'نوفمبر',
            'dec' => 'ديسمبر',
            'Dec' => 'ديسمبر',
        );
        return str_replace(array_keys($string), array_values($string), $date);
    }
    public static function week_date($date){
        $string = array(
            'Sun' => 'الاحد',
            'Mon' => 'الاثنين',
            'Tue' => 'الثلاثاء',
            'Wed' => 'الاربعاء',
            'Thu' => 'الخميس',
            'Fri' => 'الجمعة',
            'Sat' => 'السبت',
        );
        return str_replace(array_keys($string), array_values($string), $date);
    }
    public static function search($keyword){
        $new_text = str_replace('أ', 'ا', $keyword);
        $new_text = str_replace('إ', 'ا', $new_text);
        $new_text = str_replace('آ', 'ا', $new_text);
        $new_text = str_replace('ي', 'ى', $new_text);
        return($new_text);
    }
    public static function video($code) {
        if (strpos($code, 'youtube') !== false) {

            ///embed
            if (strpos($code, 'iframe') !== false) {
                return redirect()->route('video.create')
                    ->with('errors','link');
            }elseif (strpos($code, 'com/watch') !== false){
                return redirect()->route('video.create')
                    ->with('errors','link');
            }
        }elseif  (strpos($code, 'youtu.be') !== false) {
            $code = str_replace('https://youtu.be/','',$code);
            $video = array('code'=>$code, 'provider'=>'youtube');
            return $video;
        }elseif  (strpos($code, 'vimeo') !== false) {

            ///embed
            if (strpos($code, 'iframe') !== false) {
                return redirect()->route('video.create')
                    ->with('errors','link');
            }else{
                $code = str_replace('https://vimeo.com/','',$code);
                $video = array('code'=>$code, 'provider'=>'vimeo');
                return $video;
            }
        }elseif  (strpos($code, 'dailymotion') !== false) {

            ///embed
            if (strpos($code, 'iframe') !== false) {
                return redirect()->route('video.create')
                    ->with('errors','link');
            }elseif (strpos($code, 'dailymotion') !== false){
                return redirect()->route('video.create')
                    ->with('errors','link');
            }
        }elseif  (strpos($code, 'dai.ly') !== false) {

            if (strpos($code, 'http://') !== false) {
                $code = str_replace('http://dai.ly/','',$code);
            }elseif (strpos($code, 'https://') !== false) {
                $code = str_replace('https://dai.ly/','',$code);
            }
            $video = array('code'=>$code, 'provider'=>'dailymotion');
            return $video;
        }else{

            if (strpos($code, 'facebook') !== false) {
                $video = array('code'=>$code, 'provider'=>'facebook');
                return $video;
            }elseif(strpos($code, 'twitter') !== false) {
                $video = array('code'=>$code, 'provider'=>'twitter');
                return $video;

            }else{
                $video = array('code'=>$code, 'provider'=>'unknown');
                return $video;
            }
        }
    }
    public static function generate_url($channel,$id,$section,$title){

        $title = Helpers::createSlug($title);
        $section = Helpers::createSlug($section);
        if($channel == 'section'){
            $url = '/section/'.$id.'/'.$section;
        }elseif($channel == 'news'){
            $url = '/news/'.$id.'/'.$section.'/'.$title;
        }elseif($channel == 'tags'){
            $url = '/tags/'.$title;
        }
        return $url;
    }
    public static function createSlug($string, $separator = '-'){

        $string = trim($string);
        $string = mb_strtolower($string, 'UTF-8');
        $vowels = array(":", "'", '"', "?", "<", ">", "(", ")", "/", "\\", "=", "+", "quot", "#", "@", "&", "%", "$", "*", "{", "}", "[", "]");

        $string = str_replace($vowels, "", $string);
//        &%!.,;^

        $string = preg_replace("/[^a-z0-9_\s-ءاأإبتثجحخدذرزسشصضطظعغفقيكلمنيهئؤوة-هي]/u", '', $string);

        // Remove multiple dashes or whitespaces or underscores
        $string = preg_replace("/[\s-_]+/", ' ', $string);

        // Convert whitespaces and underscore to the given separator
        $string = preg_replace("/[\s_]/", $separator, $string);

        return $string;

    }
    public static function now(){
        $time = date("h:i:a");
        $time = str_replace(':am',' صباحاً ',$time);
        $time = str_replace(':pm',' مساء ',$time);

        $d_name = Helpers::week_date(date("D"));
        if(date("d") < 10){
            $d = str_replace('0','',date("d"));
        }else{
            $d = date("d");
        }
        $m = Helpers::arabic_date(date("M"));
        $y = date("Y");

//        $now = $d.' '.$m.' '.$y.' | '.$time;
        $now = $d_name.' '.$d.' '.$m.' '.$y.' | '.$time;

        return $now;

    }
    public static function formatISO($dt) {

        $dt = str_replace('/','-',$dt);
        $dt = new DateTime($dt);
        $time_dt = $dt->format('Y-m-d\TH:i:s.u\Z');
        return $time_dt;
    }
}