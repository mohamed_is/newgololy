<?php

namespace App\Http\ViewComposers;
use Illuminate\Contracts\View\View;
use App\Http\Models\Section;


class FooterComposer
{
    public function compose(View $view)
    {
        $section = Section::where('case','on')
            ->where('section_type','on')
            ->orderBy('sort', 'ASC')
            ->get();
        foreach ($section as $record){
            $record['sub'] = Section::where('case','on')->where('section',$record->mydasht_id)->get();
        }
        $view->with('footer', $section);
    }
}