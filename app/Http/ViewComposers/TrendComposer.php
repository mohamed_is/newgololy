<?php

namespace App\Http\ViewComposers;
use Illuminate\Contracts\View\View;
use App\Http\Models\Keyword;


class TrendComposer
{


    public function compose(View $view)
    {
        $keyword = keyword::orderBy('updated_at', 'desc')->limit(4)->get();


        $view->with('trend', $keyword);
    }
}