<?php

namespace App\Http\ViewComposers;
use Illuminate\Contracts\View\View;
use App\Http\Models\Hits;


class SearchComposer
{
    public function compose(View $view)
    {
        $hits = Hits::hits(8);
        $view->with('search', $hits);
    }
}