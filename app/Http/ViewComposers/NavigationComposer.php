<?php

namespace App\Http\ViewComposers;
use Illuminate\Contracts\View\View;
use App\Http\Models\Section;


class NavigationComposer
{

    public function compose(View $view)
    {
        $section = Section::where('case','on')->orderBy('sort', 'ASC')->get();
        $view->with('navigation', $section);
    }
}