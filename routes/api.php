<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
Route::post('option', 'ApiController@option');
Route::post('keyword/add', 'ApiController@keyword_add');
Route::post('keyword/update', 'ApiController@keyword_update');
Route::post('keyword/delete', 'ApiController@keyword_delete');







Route::post('related/', 'ApiController@related');


Route::get('content/tracking/{id}/{key}', 'ApiController@tracking');
Route::get('home/{id}/{key}', 'ApiController@home');
Route::get('content/get/{type}/{category}/{section}/{start_date}/{end_date}/{key}', 'ApiController@content');


Route::get('content/ajax/in/{user_id}/{content_id}/{key}', 'ApiController@content_ajax_in');
Route::get('content/ajax/api/{type}/{category}/{section}/{date}/{first_id}/{last_id}/{key}', 'ApiController@content_ajax_api');
Route::get('content/ajax/{type}/{category}/{section}/{date}/{key}', 'ApiController@content_ajax');

Route::get('content/get_test/{type}/{category}/{section}/{date}/{key}', 'ApiController@content_test');
Route::post('content/copy/save', 'ApiController@content_copy');
Route::get('content/create/{key}', 'ApiController@content_create');
Route::post('content/create/save', 'ApiController@content_create_save');
Route::post('content/create_test/save', 'ApiController@content_create_save_test');
Route::get('content/update/{id}/{key}', 'ApiController@content_update');
Route::get('content/out/{id}/{key}', 'ApiController@content_out');

Route::post('content/update/save', 'ApiController@content_update_save');
Route::post('content/update_test/save', 'ApiController@content_update_save_test');
Route::get('content/related/{keyword}/{key}', 'ApiController@content_related');
Route::get('content/search/{keyword}/{date}/{key}', 'ApiController@search');



Route::get('images/get/{key}', 'ApiController@images');
Route::get('images/search/{search}/{key}', 'ApiController@images_search');
Route::post('images/save', 'ApiController@images_save');
Route::post('images/crop', 'ApiController@images_crop');
Route::get('images/album/get/{key}', 'ApiController@album_get');



Route::get('main/related/{keyword}/{key}', 'ApiController@main_related');
Route::get('main/{section}/{area}/{key}', 'ApiController@main');

Route::post('main/save', 'ApiController@main_save');



Route::get('section/{key}', 'ApiController@section');
Route::post('section/create/save', 'ApiController@section_create_save');
Route::get('section/update/{id}/{key}', 'ApiController@section_update');
Route::post('section/update/save', 'ApiController@section_update_save');
Route::post('section/sort/save', 'ApiController@section_sort_save');
Route::post('section/delete', 'ApiController@section_delete');


Route::get('opinion/{key}', 'ApiController@opinion');
Route::post('opinion/create/save', 'ApiController@opinion_create_save');
Route::get('opinion/update/{id}/{key}', 'ApiController@opinion_update');
Route::post('opinion/update/save', 'ApiController@opinion_update_save');
Route::post('opinion/sort/save', 'ApiController@opinion_sort_save');
Route::post('opinion/delete', 'ApiController@opinion_delete');


Route::get('ads/{key}', 'ApiController@ads');
Route::post('ads/create/save', 'ApiController@ads_create_save');
Route::get('ads/update/{id}/{key}', 'ApiController@ads_update');
Route::post('ads/update/save', 'ApiController@ads_update_save');
Route::post('ads/sort/save', 'ApiController@ads_sort_save');
Route::post('ads/delete', 'ApiController@ads_delete');

Route::get('scrollarticles/{id}', 'Single_newsController@scrollarticles');
Route::get('loadmore/{page}/{sec_name?}/{offset}/{limit}', 'SectionController@loadmore');
