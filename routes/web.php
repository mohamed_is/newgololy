<?php



Route::get('/', 'HomeController@index');


Route::get('/news', 'SectionController@category');
Route::get('/category/{title}', 'SectionController@section');



Route::get('{y}/{m}/{d}/{id}/{title}', 'Single_newsController@index');
Route::get('/cv/{id}/{title}', 'Single_newsController@cv');
Route::get('amp/{y}/{m}/{d}/{id}/{title}', 'Single_newsController@amp');

Route::get('/section/{id}/{title}', 'SectionController@section_old');


Route::get('/sitemap/sitemaps.xml', 'SectionController@sitemaps');
Route::get('/sitemap', 'SectionController@sitemap');
Route::get('/sitemap/news', 'SectionController@sitemap_news');
Route::get('/tags/{id}/{slug}', 'SectionController@tags');



Route::get('/album/{id}/{slug}', 'Single_newsController@album');


Route::get('/rss/news', 'SectionController@rss_category');
Route::get('/rss/news/{title}', 'SectionController@rss_section');
Route::get('/rss/{id}', 'SectionController@rss_section_old');

Route::get('/rss/google_news/{title}', 'SectionController@rss_google_news');




Route::get('/page/error/404', function () { return view('errors.404');});





Route::get('/search', 'SectionController@search');




//Route::get('/home/hits/test', 'HomeController@hits');
Route::get('/hits/{id}', 'Single_newsController@hits');
Route::get('/home/test', 'HomeController@test');

Route::get('/cache/out/{id}', 'HomeController@cache');
Route::get('/backend/out/{id}', 'HomeController@backend');
Route::get('/backend/out', 'HomeController@backend_out');
//Clear route cache:
Route::get('/cache/route', function() {
    $exitCode = Artisan::call('route:cache');
    return 'Routes cache cleared';
});

//Clear config cache:
Route::get('/cache/config', function() {
    $exitCode = Artisan::call('config:cache');
    return 'Config cache cleared';
});

// Clear application cache:
Route::get('/cache/cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return 'Application cache cleared';
});

// Clear view cache:
Route::get('/cache/view', function() {
    $exitCode = Artisan::call('view:clear');
    return 'View cache cleared';
});






Route::get('/migration', 'MigrationController@index');
Route::get('/migration/images/copy', 'MigrationController@images');
Route::get('/migration/cv', 'MigrationController@cv');
Route::get('/last_update', 'MigrationController@last_update');
