<?php $device = (new App\Http\Helpers\Helpers)->mobiledetect();
if($device === 'Mobile'){?>
    <!-- GPT AdSlot 2 for Ad unit 'Gololy_Mobile_Medium_rec' ### Size: [[336,280],[300,250]] -->
    <div id='div-gpt-ad-9119753-2'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-9119753-2'); });
        </script>
    </div>
    <!-- End AdSlot 2 -->
<?php }else{?>
    <!-- GPT AdSlot 2 for Ad unit 'gololy_Medium_rec' ### Size: [[300,250]] -->
    <div id='div-gpt-ad-1100139-2'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1100139-2'); });
        </script>
    </div>
    <!-- End AdSlot 2 -->
<?php }?>
