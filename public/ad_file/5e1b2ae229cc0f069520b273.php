<?php $device = (new App\Http\Helpers\Helpers)->mobiledetect();
if($device === 'Mobile'){?>
    <!-- GPT AdSlot 1 for Ad unit 'Gololy_Large_Mobile_banner' ### Size: [[320,50],[320,100]] -->
    <div id='div-gpt-ad-9119753-1'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-9119753-1'); });
        </script>
    </div>
    <!-- End AdSlot 1 -->
<?php }else{?>
    <!-- GPT AdSlot 1 for Ad unit 'gololy_leaderboared' ### Size: [[728,90],[970,90],[970,250]] -->
    <div id='div-gpt-ad-1100139-1'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1100139-1'); });
        </script>
    </div>
    <!-- End AdSlot 1 -->
<?php }?>
