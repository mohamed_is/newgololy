<?php $device = (new App\Http\Helpers\Helpers)->mobiledetect();
if($device === 'Mobile'){?>
    <!-- GPT AdSlot 4 for Ad unit 'Gololy_Mobile_Medium_rec3' ### Size: [[336,280],[300,250]] -->
    <div id='div-gpt-ad-9119753-4'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-9119753-4'); });
        </script>
    </div>
    <!-- End AdSlot 4 -->
<?php }else{?>
    <!-- GPT AdSlot 4 for Ad unit 'Gololy_Medium_rec3' ### Size: [[300,250]] -->
    <div id='div-gpt-ad-1100139-4'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1100139-4'); });
        </script>
    </div>
    <!-- End AdSlot 4 -->
<?php }?>
