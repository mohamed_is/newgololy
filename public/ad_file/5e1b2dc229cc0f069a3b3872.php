<?php $device = (new App\Http\Helpers\Helpers)->mobiledetect();
if($device === 'Mobile'){?>
    <!-- GPT AdSlot 3 for Ad unit 'Gololy_Mobile_Medium_rec2' ### Size: [[336,280],[300,250]] -->
    <div id='div-gpt-ad-9119753-3'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-9119753-3'); });
        </script>
    </div>
    <!-- End AdSlot 3 -->
<?php }else{?>
    <!-- GPT AdSlot 3 for Ad unit 'Gololy_Medium_rec2' ### Size: [[300,250]] -->
    <div id='div-gpt-ad-1100139-3'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1100139-3'); });
        </script>
    </div>
    <!-- End AdSlot 3 -->
<?php }?>
