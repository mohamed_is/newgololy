<?php $device = (new App\Http\Helpers\Helpers)->mobiledetect();
if($device === 'Mobile'){?>
    <!-- GPT AdSlot 5 for Ad unit 'Gololy_Medium_rec4' ### Size: [[300,250],[336,280]] -->
    <div id='div-gpt-ad-9119753-5'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-9119753-5'); });
        </script>
    </div>
    <!-- End AdSlot 5 -->
<?php }else{?>
    <!-- GPT AdSlot 5 for Ad unit 'Gololy_300x600' ### Size: [[300,250],[300,600],[160,600]] -->
    <div id='div-gpt-ad-1100139-5'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1100139-5'); });
        </script>
    </div>
    <!-- End AdSlot 5 -->
<?php }?>
