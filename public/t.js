// Change localhost to the name or ip address of the host running the chat server
var trackingSocketUrl = 'ws://tracking.the24.ooo:9000';

function receiveWidgetArticles(data) {
    // data = JSON.parse(data);
    articles = data.articles;

    if (document.getElementById("globalDiv")) {
        document.getElementById("globalDiv").remove();
    }

    var globalDiv = document.createElement('div');
    globalDiv.setAttribute("id", "globalDiv");

    for (var key in articles) {
        var a = document.createElement('a');
        var linkText = document.createTextNode(articles[key]['title']);
        a.appendChild(linkText);
        a.title = articles[key]['title'];
        a.href = articles[key]['url'];
        a.style.cssText = 'display: block;';

        globalDiv.appendChild(a);
    }
    // document.body.appendChild(globalDiv);

    console.log(globalDiv);
}


var conn;

function getMeta(metaName) {
    const metas = document.getElementsByTagName('meta');
    for (let i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute('name') === metaName) {
            return metas[i].getAttribute('content');
        }
    }
}


// function notifyMe(article) {
//     if (Notification.permission !== "granted")
//         Notification.requestPermission();
//     else {
//         if (article) {
//             article = JSON.parse(article);
//             var notification = new Notification(article['title'], {
//                 icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
//                 body: article['description'],
//             });
//
//             notification.onclick = function () {
//                 window.open(article['url']);
//             };
//         }
//     }
//
// }

function init() {


    document.addEventListener('DOMContentLoaded', function () {
        if (!Notification) {
            alert('Desktop notifications not available in your browser. Try Chromium.');
            return;
        }

        if (Notification.permission !== "granted")  Notification.requestPermission();
        else {
            notifyMe();
        }
    });




    conn = new WebSocket(trackingSocketUrl);

    conn.onopen = function() {
        var params = {
            'roomId': window.location.hostname,
            'action': 'connect',
            "url": window.location.href,
            "hostname": window.location.hostname,
            "title": document.title,
            "description": getMeta('description'),
        };
        // console.log(params);
        conn.send(JSON.stringify(params));
    };

    conn.onmessage = function(e) {
        var data = JSON.parse(e.data);

        if (data.type == 'receive-widget-articles') {
            // console.log(data);
            receiveWidgetArticles(data)
            // displayWidget('There are ' + data.clients.length + ' users connected');
        } else if (data.type == 'receive-notification' && data.hasOwnProperty('clients')) {
            // notifyMe(data.articles);
            // displayWidget('There are ' + data.clients.length + ' users connected');
        }else if (data.type == 'user-disconnected' && data.hasOwnProperty('clients')) {
            console.log(data);
            // displayWidget('There are ' + data.clients.length + ' users connected');
        }
    };

    conn.onclose = function(e) {
        console.log('Disconnected!');
    };


    conn.onerror = function(e) {
        console.log(e);
    };

    // window.onbeforeunload = function() {
    //     var params = {
    //         'roomId': window.location.hostname,
    //         'action': 'disconnect',
    //         "url": window.location.href,
    //         "hostname": window.location.hostname,
    //         "title": document.title,
    //         "description": getMeta('description'),
    //     };
    //     conn.send(JSON.stringify(params));
    // };

    return false;
}

init();

// function sendChatMessage() {
//     var d = new Date();
//     var params = {
//         'message': document.getElementsByName("message")[0].value,
//         'action': 'message',
//         'timestamp': d.getTime()/1000
//     };
//     conn.send(JSON.stringify(params));
//
//     document.getElementsByName("message")[0].value = '';
//     return false;
// }
//
// function updateChatTyping() {
//     var params = {};
//
//     if (document.getElementsByName("message")[0].value.length > 0) {
//         params = {'action': 'start-typing'};
//         conn.send(JSON.stringify(params));
//     }
//     else if (document.getElementsByName("message")[0].value.length == 1) {
//         params = {'action': 'stop-typing'};
//         conn.send(JSON.stringify(params));
//     }
// }
